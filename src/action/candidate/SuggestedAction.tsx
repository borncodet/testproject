import { Reducer } from "react"

import React from "react";

import axios from 'axios';
import AuthService from "../../services/AuthService";
import { AppUrls } from "../../environments/environment";

import { ISuggestedJobManagementState, SuggestedJobDispatcherContext, SuggestedJobStateContext } from "../../context/candidate/SuggestedJobContext";
import { BookMarkCandidateViewResponseModel } from "../../models/candidate/BookMarkedJob";
import { suggestedJobRequestModel, SuggestedJobViewModel } from "../../models/candidate/SuggestedJob";


let token=AuthService.accessToken;
let   authorizationToken =token!=null?token.replace(/['"]+/g,''):""; 

export type ISuggestedJobAction = {
    type:"SET_Job_SUGGESTED_LIST",
    suggestedJob:SuggestedJobViewModel 
}



export const suggestedJobReducer: Reducer<ISuggestedJobManagementState, ISuggestedJobAction> = (draft, action): ISuggestedJobManagementState => {
    switch (action.type) {
     
            case "SET_Job_SUGGESTED_LIST":
                draft.suggestedJob=action.suggestedJob;
                return draft;
               
    }
}



export const useSuggestedJobDispatcher = (): React.Dispatch<ISuggestedJobAction> => {
    const suggestedJobDispatcher = React.useContext(SuggestedJobDispatcherContext);
    if (!suggestedJobDispatcher) {
        throw new Error("You have to provide the SuggestedJob dispatcher using theSuggestedJobDispatcherContext.Provider in a parent component.");
    }
    return suggestedJobDispatcher;
}


export const useSuggestedJobContext = (): ISuggestedJobManagementState => {
    const suggestedJobContext = React.useContext(SuggestedJobStateContext);
    if (!suggestedJobContext) throw new Error("You have to provide the suggestedJob context using the SuggestedJobStateContext.Provider in a parent component.");
    return suggestedJobContext;
}



export const getSuggestedJobList = async (dispatcher: React.Dispatch<ISuggestedJobAction>,query:suggestedJobRequestModel,token:string  ) => {
   
    try{

      if (token?.startsWith(`"`)) {
        token = token.substring(1);
        token = token.slice(0, -1);
      }

      let header = {
                      "Content-Type": "application/json",
                      "Accept": "application/json",
                     "Authorization":`Bearer ${token}`,
                  };
  
                   const url=AppUrls.GetSuggestedJob
  
                  axios.post(url, JSON.stringify( 
                     query
                   ),{ headers: header })
                  .then(res => {
                    console.log(7777,res)
                    dispatcher({ type:"SET_Job_SUGGESTED_LIST", suggestedJob: res.data });
                  })

  }
  catch(e){

  }
}






