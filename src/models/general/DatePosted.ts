export interface datePostedRequestModel {
  page: number;
  pageSize: number;
  searchTerm: string;
  sortOrder: string;
}

export interface datePosted {
  rowId: number;
  datePostedId: number;
  day: number;
  month: number;
  year: number;
  isActive: boolean;
}

export interface datePostedViewModel {
  data: datePosted[];
  total: number;
  hasNext: boolean;
  hasPreviousPage: boolean;
  currentPage: number;
  currentPageSize: number;
}