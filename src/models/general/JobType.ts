export interface jobType {
  rowId: number;
  robTypeId: number;
  title: string;
  description: string;
  isActive: boolean;
}

export interface jobTypeViewModel {
  data: jobType[];
  total: number;
  hasNext: boolean;
  hasPreviousPage: boolean;
  currentPage: number;
  currentPageSize: number;
}

export interface jobTypeRequestmodel {
page: number;
pageSize: number;
searchTerm: string;
sortOrder: string;
}

export interface countAllRequestModel {
  candidateId: number;
  page: number;
  pageSize: number;
  searchTerm: string;
  sortOrder: string;
  showInactive: boolean;
}

export interface countAllRequestModel {
  candidateId: number;
  page: number;
  pageSize: number;
  searchTerm: string;
  sortOrder: string;
  showInactive: boolean;
}


  export interface digiDoc {
      documentCount: number;
      documentTypeTitle: string;
  }

  export interface expiryDigiDoc {
      documentCount: number;
      documentTypeTitle: string;
  }

  export interface countAllResultModel {
      jobApplied: number;
      jobSaved: number;
      resumeCreated: number;
      newAlerts: number;
      shortListed: number;
      newMessages: number;
      digiDocs: digiDoc[];
      expiryDigiDocs: expiryDigiDoc[];
  }


