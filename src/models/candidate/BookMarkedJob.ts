export interface jobBookMarkDataWithId {
  rowId: number
}

export interface jobBookMarkDataWithUserId {
  jobId: number,
  candidateId: number
}

export interface jobBookMarkRequestModel {
  candidateId: number;
  page: number;
  pageSize: number;
  searchTerm: string;
  sortOrder: string;
  showInactive: boolean;
}



export interface jobBookMarked {
  rowId: number;
  jobBookmarkedId: number;
  candidateId: number;
  candidatetName: string;
  jobId: number;
  jobTitle: string;
  isActive: boolean;
}

export interface jobBookMarkViewModel {
  data: jobBookMarked[];
  total: number;
  hasNext: boolean;
  hasPreviousPage: boolean;
  currentPage: number;
  currentPageSize: number;
}


export interface jobBookMarkSaveRequestModel {
  rowId: number;
  jobBookmarkedId: number;
  candidateId: number;
  jobId: number;
  isActive: boolean;
}

export interface jobBookMarkSaveRespondModel {
  entityId: number;
  message: string;
  type: number;
  isSuccess: boolean;
  code: number;
}


export interface BookMarkCandidateResponseModel {
  rowId: number;
  jobId: number;
  categoryId: number;
  categoryName: string;
  title: string;
  description: string;
  experienceId: number;
  experience: string;
  numberOfVacancies: number;
  jobTypeId: number;
  jobType: string;
  isPreferred: boolean;
  isRequired: boolean;
  locationId: string;
  regionId: string;
  territoryId: string;
  minAnnualSalary: number;
  MaxAnnualSalary: number;
  CurrencyId: number;
  Currency: string;
  IndustryId: number;
  Industry: string;
  FunctionalAreaId: number;
  FunctionalArea: string;
  profileDescription: string;
  WillingnessToTravelFlag: boolean;
  PreferedLangId: number;
  PreferedLanguage: string;
  AutoScreeningFilterFlag: boolean;
  AutoSkillAssessmentFlag: boolean;
  postedDate: Date;
  JobBookmarkedId: number;
  candidateId: number;
  candidatetName: string;
  daysAgo: string;
  isActive: boolean;
}

export interface BookMarkCandidateViewResponseModel {
  data: jobViewModel[];
  total: number;
  hasNext: boolean;
  hasPreviousPage: boolean;
  currentPage: number;
  currentPageSize: number;
}



export interface jobViewModel {
  rowId: number;
  jobId: number;
  categoryId: number;
  categoryName: string;
  title: string;
  description: string;
  experienceId: number;
  experience: string;
  numberOfVacancies: number;
  jobTypeId: number;
  jobType: string;
  isBookmarked: boolean,
  isApplied: boolean,
  isPreferred: boolean;
  isRequired: boolean;
  locationId: string;
  regionId: string;
  territoryId: string;
  minAnnualSalary: number;
  maxAnnualSalary: number;
  currencyId: number;
  currency: string;
  industryId: number;
  industry: string;
  functionalAreaId: number;
  functionalArea: string;
  profileDescription: string;
  willingnessToTravelFlag: boolean;
  preferedLangId: number;
  preferedLanguage: string;
  autoScreeningFilterFlag: boolean;
  autoSkillAssessmentFlag: boolean;
  postedDate: Date;
  jobAppliedId: number;
  candidateId: number;
  candidatetName: string;
  daysAgo: string;
  isActive: boolean;
  JobBookmarkedId: number;
}