export interface NamePrefix {
  value: string;
  caption: string;
}

export interface Gender {
  value: string;
  caption: string;
}

export interface MartialStatu {
  value: string;
  caption: string;
}

export interface JobCategory {
  value: string;
  caption: string;
}

export interface Designation {
  value: string;
  caption: string;
}

export interface JobType {
  value: string;
  caption: string;
}

export interface Industry {
  value: string;
  caption: string;
}

export interface FunctionalArea {
  value: string;
  caption: string;
}

export interface JobRole {
  value: string;
  caption: string;
}

export interface State {
  value: string;
  caption: string;
}

export interface Country {
  value: string;
  caption: string;
}

export interface MyProfileSelectBoxDataViewModel {
  namePrefix: NamePrefix[];
  genders: Gender[];
  martialStatus: MartialStatu[];
  jobCategories: JobCategory[];
  designations: Designation[];
  jobTypes: JobType[];
  industries: Industry[];
  functionalAreas: FunctionalArea[];
  jobRoles: JobRole[];
  states: State[];
  countries: Country[];
}

export interface BasicInfoViewModel {
  userId: number;
  userName: string;
  fullName:string;
  email: string;
  phoneNumber: string;
  message: string;
  type: number;
  isSuccess: boolean;
  code: number;
}


export interface profileImageRequestModel {
  candidateId: number;
  page: number;
  pageSize: number;
  searchTerm: string;
  sortOrder: string;
  showInactive: boolean;
}

export interface profileImage {
  rowId: number;
  candidateProfileImageId: number;
  candidateId: number;
  imageUrl: string;
  isActive: boolean;
}

export interface profileImageViewModel {
  data: profileImage[];
  total: number;
  hasNext: boolean;
  hasPreviousPage: boolean;
  currentPage: number;
  currentPageSize: number;
}

export interface profileImageViewModel {
  data: profileImage[];
  total: number;
  hasNext: boolean;
  hasPreviousPage: boolean;
  currentPage: number;
  currentPageSize: number;
}

export interface profileBarProgress {
  profileProgress: number;
  resumeProgress: number;
}
