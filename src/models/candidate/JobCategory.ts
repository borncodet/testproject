
export interface categoryWithId {
  categoryId:number
}
  export interface jobCategory {
      rowId: number;
      jobCategoryId: number;
      title: string;
      description: string;
      isActive: boolean;
  }

  export interface jobCategoryViewModel {
      data: jobCategory[];
      total: number;
      hasNext: boolean;
      hasPreviousPage: boolean;
      currentPage: number;
      currentPageSize: number;
  }



