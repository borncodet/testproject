
export interface jobAlert {
  rowId: number;
  jobId: number;
  categoryId: number;
  categoryName: string;
  title: string;
  description: string;
  experienceId: number;
  experience: string;
  numberOfVacancies: number;
  jobTypeId: number;
  jobType: string;
  isPreferred: boolean;
  isRequired: boolean;
  locationId: string;
  regionId: number;
  territoryId: number;
  minAnnualSalary: number;
  maxAnnualSalary: number;
  currencyId: number;
  currency: string;
  industryId: number;
  industry: string;
  functionalAreaId: number;
  functionalArea: string;
  profileDescription: string;
  willingnessToTravelFlag: boolean;
  preferedLangId: number;
  preferedLanguage: string;
  autoScreeningFilterFlag: boolean;
  autoSkillAssessmentFlag: boolean;
  postedDate: Date;
  isActive: boolean;
}

export interface jobAlertViewModel {
  data: jobAlert[];
  total: number;
  hasNext: boolean;
  hasPreviousPage: boolean;
  currentPage: number;
  currentPageSize: number;
}



export interface jobAlertRequestmodel {
    candidateId: number;
    title: string;
    location: string;
    type: string;
    expereince: string;
    lastDays: number;
    pageIndex: number;
    pageSize: number;
    showInactive: boolean;
}

export interface jobAlertSaveRequestModel {
  rowId: number;
  jobAlertId: number;
  keywords: string;
  totalExperience: string;
  locationId: string;
  alertTitle: string;
  salaryFrom: number;
  userId: number;
  isActive: boolean;
}

export interface jobAlertSaveRespondModel {
  entityId: number;
  message: string;
  type: number;
  isSuccess: boolean;
  code: number;
}

export interface jobAlertCategoryMapRequestModel {
  rowId: number;
  jobAlertCategoryMapId: number;
  jobCategoryId: number;
  jobAlertId: number;
  isActive: boolean;
}

export interface jobAlertExperienceMapRequestModel {
  rowId: number;
  jobAlertExperienceMapId: number;
  durationId: number;
  jobAlertId: number;
  isActive: boolean;
}

export interface jobAlertIndustryMapSaveRequestModel {
  rowId: number;
  robAlertIndustryMapId: number;
  industryId: number;
  jobAlertId: number;
  isActive: boolean;
}

export interface JobAlertRoleMapSaveRequestModel

{
  rowId: number;
  jobAlertRoleMapId: number;
  jobRoleId: number;
  jobAlertId: number;
  isActive: boolean;
}

export interface JobAlertTypeMapSaveRequestModel
{
  rowId: number;
  jobAlertTypeMapId: number;
  jobTypeId: number;
  jobAlertId: number;
  isActive: boolean;
}

export interface jobAlertGellAllRequestModel {
  candidateId: number;
  page: number;
  pageSize: number;
  searchTerm: string;
  sortOrder: string;
  showInactive: boolean;
}

export interface jobAlertGetAll {
  rowId: number;
  jobAlertId: number;
  keywords: string;
  totalExperience: string;
  locationId: string;
  alertTitle: string;
  salaryFrom: number;
  userId: number;
  isActive: boolean;
}

export interface jobAlertGellAllRespondModel {
  data: jobAlertGetAll[];
  total: number;
  hasNext: boolean;
  hasPreviousPage: boolean;
  currentPage: number;
  currentPageSize: number;
}

export interface jobAlertTitleDropdownResult {
  value: string;
  caption: string;
}

// export interface jobAlertTitleDropdownResultArray {
//  data:jobAlertTitleDropdownResult[]
// }