import React from "react";
import CandidateLayoutContainer from "../candidate/layout/CandidateLayoutContainer";
import MiscellaneousLayoutContainer from "./../miscellaneous/MiscellaneousLayoutContainer";
import { Switch, Route, Redirect } from "react-router-dom";
import AboutUsContainer from "../about_us/AboutUsContainer";
import JobSearchContainer from "../job_search/JobSearchContainer";
import ContactUsContainer from "../contact_us/ContactUsContainer";
import HelpContainer from "../help/HelpContainer";
import CareerTipsContainer from "../career_tips/CareerTipsContainer";
import CareerTipsCategoryContainer from "../career_tips/CareerTipsCategoryContainer";
import IndexContainer from "../index/IndexContainer";
import { SearchContextProvider } from "../../context/general/SearchContext";
import { NewlyPostedJobContextProvider } from "../../context/general/NewlyPostedJobContext";
import { MatchedJobContextProvider } from "../../context/MatchedJobContext";
import JitTest from "./JitTest_";
import FileViewer from "../FileView";
import FileView from "../FileView";
import VendorLayoutContainer from "../vendor/layout/VendorLayoutContainer";

interface ILayoutComponentProps {}

interface ILayoutComponentState {}

const initialState = {};

const LayoutComponent: React.FC<ILayoutComponentProps> = (props) => {
  const [
    LayoutComponentState,
    setLayoutComponentState,
  ] = React.useState<ILayoutComponentState>(initialState);

  return (
    <Switch>
      <Route exact path="/">
        {/* <SearchContextProvider> */}

        <IndexContainer />

        {/* </SearchContextProvider> */}
      </Route>
      <Route exact path="/jit_test_">
        <JitTest />
      </Route>
      <Route path="/candidate">
        <CandidateLayoutContainer />
      </Route>
      <Route path="/admin">
        <CandidateLayoutContainer />
      </Route>
      <Route path="/vendor">
        <VendorLayoutContainer />
      </Route>
      <Route path="/about_us">
        <AboutUsContainer />
      </Route>
      <Route path="/contact_us">
        <ContactUsContainer />
      </Route>
      <Route path="/help">
        <HelpContainer />
      </Route>
      <Route path="/career_tips">
        <CareerTipsContainer />
      </Route>
      <Route path="/career_tips_category">
        <CareerTipsCategoryContainer />
      </Route>
      <Route exact path="/job_search/:jobId">
        {/* <SearchContextProvider> */}
        <JobSearchContainer />
        {/* </SearchContextProvider> */}
      </Route>
      <Route path="/job_search/:api/:id">
        {/* <SearchContextProvider> */}
        <JobSearchContainer />
        {/* </SearchContextProvider> */}
      </Route>
      <Route path="/file_view/:fileType/:filePath">
        <FileView />
      </Route>
      <Route path="/">
        <MiscellaneousLayoutContainer />
      </Route>
    </Switch>
  );
};
export default LayoutComponent;
