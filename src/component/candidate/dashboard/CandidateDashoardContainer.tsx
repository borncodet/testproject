import React from "react";
import {
  getAllCount,
  useJobTypeDispatcher,
} from "../../../action/general/JobTypeAction";
import {
  getMatchedJobList,
  useMatchedJobContext,
  useMatchedJobDispatcher,
} from "../../../action/MatchedJobAction";
import { useMyProfileContext } from "../../../action/MyProfileAction";
import { matchedJobRequestModel } from "../../../models/candidate/MatchedJob";
import { countAllRequestModel } from "../../../models/general/JobType";
import AuthService from "../../../services/AuthService";

import CandidateDashboardComponent from "./CandidateDashboardComponent";
import CandidateDashboardComponentFour from "./CandidateDashBoardComponentFour";
import CandidateDashboardComponentThree from "./CandidateDashBoardComponentThree";
import CandidateDashboardComponentTwo from "./CandidateDashboardComponentTwo";

interface ICandidateDashboardContainerProps {}

interface ICandidateDashboardContainerState {}

const initialState = {};

const CandidateDashboardContainer: React.FC<ICandidateDashboardContainerProps> = (
  props
) => {
  const [
    CandidateDashboardContainerState,
    setCandidateDashboardContainerState,
  ] = React.useState<ICandidateDashboardContainerState>(initialState);
  const authorizationToken = AuthService.accessToken;
  const matchedJobDispatcher = useMatchedJobDispatcher();
  const matchedJobContext = useMatchedJobContext();
  const { matchedJob } = matchedJobContext;
  const myProfileContext = useMyProfileContext();
  const {
    basicInfo,
    loggedUserId,
    profileImage,
    myProfileProgressBar,
  } = myProfileContext;
  const jobTypeDispatcher = useJobTypeDispatcher();

  React.useEffect(() => {
    if (authorizationToken != null && loggedUserId != 0) {
      (async () => {
        await getAllCount(
          jobTypeDispatcher,
          {
            candidateId: loggedUserId,
            page: 1,
            pageSize: 10,
            searchTerm: "",
            showInactive: false,
            sortOrder: "",
          } as countAllRequestModel,
          authorizationToken
        );
      })();
    }
  }, [loggedUserId]);

  React.useEffect(() => {
    if (authorizationToken != null) {
      console.log(898989898988989898);
      (async () => {
        await getMatchedJobList(
          matchedJobDispatcher,
          {
            candidateId: 1,
            type: "",
            expereince: "",
            location: "",
            title: "",
            pageIndex: 1,
            pageSize: 10,
            showInactive: true,
          } as matchedJobRequestModel,
          authorizationToken
        );
      })();
    }
  }, []);

  return (
    <>
      <CandidateDashboardComponent />
      {/* <CandidateDashboardComponentTwo/> */}
      {/* <CandidateDashboardComponentThree/> */}
      {/* <CandidateDashboardComponentFour /> */}
    </>
  );
};
export default CandidateDashboardContainer;
