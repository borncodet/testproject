import React from "react";
import {
  getJobBookMarkList,
  useJobBookMarkContext,
  useJobBookMarkDispatcher,
} from "../../../action/candidate/JobBookMarkAction";
import { jobBookMarkRequestModel } from "../../../models/candidate/BookMarkedJob";
import AuthService from "../../../services/AuthService";
import CandidateLayoutComponent from "./CandidateLayoutComponent";
import { CandidateContextProvider } from "./../../../context/candidate/CandidateMyProfile";
import {
  getJobAppliedList,
  useJobAppliedContext,
  useJobAppliedDispatcher,
} from "../../../action/JobAppliedAction";
import { jobAppliedRequestModel } from "../../../models/candidate/JobApplied";
import {
  getLoggedUserId,
  useMyProfileDispatcher,
} from "../../../action/MyProfileAction";
import IdleTimeChecker from "../../IdleTimeChecker";

interface ICandidateLayoutContainerProps {}

interface ICandidateLayoutContainerState {}

const initialState = {};

const CandidateLayoutContainer: React.FC<ICandidateLayoutContainerProps> = (
  props
) => {
  const [
    CandidateLayoutContainerState,
    setCandidateLayoutContainerState,
  ] = React.useState<ICandidateLayoutContainerState>(initialState);

  const authorizationToken = AuthService.accessToken;
  const myProfileDispatcher = useMyProfileDispatcher();
  const jobBookMarkDispatcher = useJobBookMarkDispatcher();
  const jobAppliedDispatcher = useJobAppliedDispatcher();
  const jobAppliedContext = useJobAppliedContext();
  const { jobApplied } = jobAppliedContext;

  // React.useEffect(() => {
  //   if(authorizationToken!=null){
  //   (async () => {
  //     await getJobBookMarkList(jobBookMarkDispatcher,{
  //      candidateId:1,page:1,pageSize:10,searchTerm:'',showInactive:true,sortOrder:''
  //   } as jobBookMarkRequestModel,authorizationToken)

  //   })();
  // }
  // }, [authorizationToken])

  let user = AuthService.currentUser;

  React.useEffect(() => {
    if (authorizationToken != null && user?.id != null)
      (async () => {
        await getLoggedUserId(
          myProfileDispatcher,
          parseInt(user.id),
          authorizationToken
        );
      })();
  }, [authorizationToken, user]);

  // React.useEffect(() => {
  //   if (authorizationToken != null) {
  //     (async () => {
  //       await getJobAppliedList(
  //         jobAppliedDispatcher,
  //         {
  //           page: 1,
  //           pageSize: 10,
  //           searchTerm: "",
  //           sortOrder: "",
  //         } as jobAppliedRequestModel,
  //         authorizationToken
  //       );
  //     })();
  //   }
  // }, [authorizationToken]);

  return (
    
    <CandidateContextProvider>
      <CandidateLayoutComponent />
    </CandidateContextProvider>
    
  );
};
export default CandidateLayoutContainer;
