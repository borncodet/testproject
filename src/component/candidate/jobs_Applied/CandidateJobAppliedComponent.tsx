import React, { useState } from "react";
import { Scrollbars } from "react-custom-scrollbars";
import {
  jobAppliedSaveRequestModel,
  jobAppliedViewModel,
} from "../../../models/candidate/JobApplied";
import {
  getJobAppliedCandidateList,
  getJobAppliedList,
  saveJobApplied,
  useJobAppliedContext,
  useJobAppliedDispatcher,
} from "../../../action/JobAppliedAction";
import { jobAppliedRequestModel } from "../../../models/candidate/JobApplied";
import {
  useSuggestedJobContext,
  useSuggestedJobDispatcher,
} from "../../../action/candidate/SuggestedAction";
import { suggestedJobRequestModel } from "../../../models/candidate/SuggestedJob";
import { Controller, useForm } from "react-hook-form";
import SelectOption from "../my_profile/components/SelectOption";
import { useMyProfileContext } from "../../../action/MyProfileAction";
import AuthService from "../../../services/AuthService";
import { searchDataWithOutTokenRequestModel } from "../../../models/general/Search";
import {
  getSearchListWithToken,
  useSearchDispatcher,
} from "../../../action/general/SearchAction";
import { useHistory } from "react-router-dom";
import { Modal } from "react-bootstrap";
import {
  FacebookShareButton,
  LinkedinIcon,
  LinkedinShareButton,
  TwitterShareButton,
} from "react-share";
import {
  deleteJobBookMarkList,
  saveBookMarkList,
  useJobBookMarkContext,
  useJobBookMarkDispatcher,
} from "../../../action/candidate/JobBookMarkAction";
import {
  jobBookMarkDataWithId,
  jobBookMarkDataWithUserId,
  jobBookMarkSaveRequestModel,
} from "../../../models/candidate/BookMarkedJob";

interface ReactSelectOption {
  value: string;
  label: string;
}
interface ICandidateJobAppliedComponentProps { }

interface ICandidateJobAppliedComponentState {
  location: string;
  type: string[];
  jobTypeData: string;
  currentJobId: number;
  filterFlag: boolean;
}

const defaultValues = {
  location: "",
  type: [],
  jobTypeData: "1",
  currentJobId: 0,
  filterFlag: true,
};

const CandidateJobAppliedComponent: React.FC<ICandidateJobAppliedComponentProps> = (
  props
) => {
  const [
    CandidateJobAppliedComponentState,
    setCandidateJobAppliedComponentState,
  ] = React.useState<ICandidateJobAppliedComponentState>(defaultValues);

  const {
    currentJobId,
    filterFlag,
    jobTypeData,
  } = CandidateJobAppliedComponentState;

  let jobCategoryData: ReactSelectOption[] = [];
  let jobTypeDatas: ReactSelectOption[] = [];

  const {
    register,
    handleSubmit,
    watch,
    errors,
    setValue,
    getValues,
    control,
  } = useForm<ICandidateJobAppliedComponentState>({
    defaultValues,
  });

  let history = useHistory();

  const [isShareOpen, setIsShareOpen] = useState<boolean>(false);

  const jobBookMarkDispatcher = useJobBookMarkDispatcher();
  const jobBookMarkContext = useJobBookMarkContext();
  const { jobBookMark, jobBookMarkSaveRespond } = jobBookMarkContext;

  const jobAppliedDispatcher = useJobAppliedDispatcher();
  const jobAppliedContext = useJobAppliedContext();
  const { jobApplied, jobAppliedCandidateResult } = jobAppliedContext;

  const suggestedJobDispatcher = useSuggestedJobDispatcher();
  const suggestedJobContext = useSuggestedJobContext();
  const { suggestedJob } = suggestedJobContext;

  const myProfileContext = useMyProfileContext();
  const { myProfile, loggedUserId } = myProfileContext;

  const searchDispatcher = useSearchDispatcher();

  const authorizationToken = AuthService.accessToken;

  console.log("jobAppliedCandidateResult", jobAppliedCandidateResult);

  React.useEffect(() => {
    if (authorizationToken != null && loggedUserId != 0) {
      (async () => {
        await getJobAppliedCandidateList(
          jobAppliedDispatcher,
          {
            candidateId: loggedUserId,
            jobAppliedId: 0,
            isActive: true,
            jobId: 0,
            rowId: 0,
          } as jobAppliedSaveRequestModel,
          authorizationToken
        );
      })();
    }
  }, [jobBookMarkSaveRespond]);

  const onSubmit = (data: any) => {
    if (authorizationToken != null && loggedUserId != 0) {
      getSearchListWithToken(
        searchDispatcher,
        {
          expereince: [],
          location: [data.location],
          title: [data.jobTitle],
          type: data.types != null ? [data.types] : [],
          lastDays: [],
          candidateId: loggedUserId,
          pageIndex: 1,
          pageSize: 60,
          showInactive: false,
        } as searchDataWithOutTokenRequestModel,
        authorizationToken
      );
      history.push("/job_search/0");
    }
  };

  const handleJobSelect = (id: any) => {
    history.push(`/job_search/a/${id}`);
  };

  const handleShareButtonClick = (id: any, value: any) => {
    setCandidateJobAppliedComponentState({
      ...CandidateJobAppliedComponentState,
      currentJobId: id,
      filterFlag: value,
    });
    setIsShareOpen(!isShareOpen);
  };

  const handleSaveJobBookMarked = (jobId: number) => {
    if (authorizationToken != null) {
      saveBookMarkList(
        jobBookMarkDispatcher,
        {
          candidateId: loggedUserId,
          jobBookmarkedId: 0,
          jobId: jobId,
          isActive: true,
          rowId: 0,
        } as jobBookMarkSaveRequestModel,
        authorizationToken
      );
    }
  };

  const handleDeleteBookMarked = (jobId: number) => {
    if (authorizationToken != null)
      deleteJobBookMarkList(
        jobBookMarkDispatcher,
        {
          jobId: jobId,
          candidateId: loggedUserId,
        } as jobBookMarkDataWithUserId,
        authorizationToken
      );
  };

  return (
    <React.Fragment>
      <div className="content-wrapper">
        <div className="container-fluid">
          <h1 className="heading">Jobs Applied</h1>
          <div className="clearfix" />
          <div className="row ">
            <div className="col-sm-12">
              <div className="carees_search1">
                <div className="row">
                  <form onSubmit={handleSubmit(onSubmit)} noValidate>
                    <div className="search_forms">
                      <div className="cr_serach_br">
                        <input
                          name="jobTitle"
                          ref={register({ required: "Required" })}
                          type="text"
                          className="form-control"
                          placeholder="Job Title"
                        />
                        <div className="search_icons">
                          <img
                            src={require("../../../images/search_icon.png")}
                          //  src="images/search_icon.png"
                          />
                        </div>
                      </div>
                    </div>
                    {/* <ErrorMessage errors={errors} name="jobType" render={({ message }) => <div className="register_validation">{message}</div>} /> */}
                    <div className="search_forms">
                      <div className="cr_serach_br">
                        <span className="select-search">
                          <input
                            name="location"
                            ref={register({ required: false })}
                            type="text"
                            className="form-control"
                            placeholder="location"
                          />
                        </span>

                        <div className="search_icons">
                          <img
                            src={require("../../../images/locattion_icon.png")}
                          // src="images/locattion_icon.png"
                          />
                        </div>
                      </div>
                    </div>
                    <div className="search_forms">
                      <div className="cr_serach_br">
                        <span className="select-search">
                          <Controller
                            control={control}
                            name="jobTypeData"
                            render={({ onChange, onBlur, value, name }) => (
                              <SelectOption
                                values={
                                  myProfile.jobTypes != undefined
                                    ? myProfile.jobTypes.map((e) => {
                                      return {
                                        value: e["value"],
                                        label: e["caption"],
                                      };
                                    })
                                    : jobTypeDatas
                                }
                                disabled={false}
                                onChange={onChange}
                                onBlur={onBlur}
                                value={value}
                                name={name}
                              />
                            )}
                          />
                          {/* <span className="holder">Type</span> */}
                        </span>
                        <div className="search_icons">
                          <img
                            src={require("../../../images/type_icon.png")}
                          // src="images/type_icon.png"
                          />
                        </div>
                      </div>
                    </div>
                    <div className="search_button">
                      <div className="search">
                        <button className="CustomButtonCss" type="submit">
                          Search
                        </button>
                      </div>
                    </div>
                  </form>
                  {/* <div className="search_forms">
                    <div className="cr_serach_br">
                      <input name="" type="text" className="form-control" placeholder="Job Title" />
                      <div className="search_icons"><img
                        src={require("../../../images/search_icon.png")}
                      // src="images/search_icon.png" 
                      />
                      </div>
                    </div>
                  </div>
                  <div className="search_forms">
                    <div className="cr_serach_br">
                      <span className="select-search"><select name="timepass" className="custom-search">
                        <option value=" ">Location</option>
                        <option value=" ">Dummy</option>
                        <option value=" ">Dummy</option>
                      </select><span className="holder">Location</span></span>
                      <div className="search_icons">
                        <img
                          src={require("../../../images/locattion_icon.png")}
                        //  src="images/locattion_icon.png" 
                        /></div>
                    </div>
                  </div>
                  <div className="search_forms">
                    <div className="cr_serach_br">
                      <span className="select-search"><select name="timepass" className="custom-search">
                        <option value=" ">Type</option>
                        <option value=" ">Dummy</option>
                        <option value=" ">Dummy</option>
                      </select><span className="holder">Type</span></span>
                      <div className="search_icons"><img
                        src={require("../../../images/type_icon.png")}
                      //  src="images/type_icon.png"
                      /></div>
                    </div>
                  </div>
                  <div className="search_button"><div className="search"><a href="#">Search</a></div>
                  </div> */}
                </div>
              </div>
            </div>
            <div className="col-sm-8">
              <div className="section_box3">
                <div className="row">
                  <div
                    className="CandidateJobApplied_scroll"
                    style={{ overflow: "hidden", outline: "none" }}
                    tabIndex={0}
                  >
                    <Scrollbars
                      style={{ height: 690 }}
                      autoHide
                      renderThumbVertical={({ style, ...props }) => (
                        <div
                          {...props}
                          style={{
                            ...style,
                            position: "relative",
                            display: "block",
                            width: "5px",
                            cursor: "pointer",
                            borderRadius: "inherit",
                            backgroundColor: "rgb(73, 69, 69)",
                            height: "115px",
                          }}
                        />
                      )}
                    >
                      {jobAppliedCandidateResult.data
                        ? // jobApplied != undefined
                        jobAppliedCandidateResult.data
                          //  jobApplied.data
                          .map((Job, index) => {
                            return (
                              <div
                                // onClick={() => handleJobSelect(Job.rowId)}
                                className="col-sm-4"
                                key={`${index}`}
                              >
                                <div className="matched_jobs">
                                  <div className="matched_jobs_cat_t">
                                    {Job.categoryName}
                                  </div>
                                  <div className="jobs_start1">
                                    {Job.postedDate}
                                  </div>
                                  <div className="matched_star">
                                    {Job.isBookmarked ? (
                                      <i
                                        onClick={() =>
                                          handleDeleteBookMarked(Job.jobId)
                                        }
                                        className="fa fa-star"
                                        aria-hidden="true"
                                      />
                                    ) : (
                                        <i
                                          onClick={() =>
                                            handleSaveJobBookMarked(Job.jobId)
                                          }
                                          className="fa fa-star-o"
                                          aria-hidden="true"
                                        />
                                      )}
                                  </div>
                                  <div className="clearfix" />
                                  <div
                                    onClick={() => handleJobSelect(Job.jobId)}
                                  >
                                    <div className="matched_heading">
                                      {Job.title}
                                    </div>
                                    <div className="matched_jobs_cat">
                                      <i
                                        className="fa fa-map-marker"
                                        aria-hidden="true"
                                      />
                                      {""}
                                        San Fransisco
                                      </div>
                                    <div className="matched_jobs_cat text-right">
                                      <i
                                        style={{ marginRight: 5 }}
                                        className="fa fa-clock-o"
                                        aria-hidden="true"
                                      />
                                      {"  "}
                                      {Job.jobType}
                                    </div>
                                    <div className="clearfix" />
                                    <p>{Job.description}....</p>
                                  </div>
                                  <div className="matched_jobs_share">
                                    <a
                                      onClick={() =>
                                        handleShareButtonClick(
                                          Job.rowId,
                                          true
                                        )
                                      }
                                    >
                                      <i
                                        className="fa fa-share-alt"
                                        aria-hidden="true"
                                      />
                                    </a>
                                  </div>
                                  <div className="matched_jobs_status">
                                    <a href="#">status</a>
                                  </div>
                                </div>
                              </div>
                            );
                          })
                        : null}
                    </Scrollbars>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-4">
              <div className="section_box3">
                <h1>Suggested Jobs</h1>
                <div
                  className="suggested_job_scroll"
                  style={{ overflow: "hidden", outline: "none" }}
                  tabIndex={0}
                >
                  <Scrollbars
                    style={{ height: 300 }}
                    autoHide
                    renderThumbVertical={({ style, ...props }) => (
                      <div
                        {...props}
                        style={{
                          ...style,
                          position: "relative",
                          display: "block",
                          width: "5px",
                          cursor: "pointer",
                          borderRadius: "inherit",
                          backgroundColor: "rgb(73, 69, 69)",
                          height: "115px",
                        }}
                      />
                    )}
                  >
                    {suggestedJob.Data != undefined
                      ? suggestedJob.Data.map((data, i) => {
                        return (
                          <div className="suggested_sec" key={i}>
                            <div className="suggested_sec_cat">
                              <i
                                className="fa fa-briefcase"
                                aria-hidden="true"
                              />{" "}
                              {data.JobType}
                            </div>
                            <div className="suggested_sec_cat">
                              <i
                                className="fa fa-calendar"
                                aria-hidden="true"
                              />{" "}
                              {data.PostedDate}
                            </div>
                            <div className="suggested_sec_cat">
                              <i
                                className="fa fa-clock-o"
                                aria-hidden="true"
                              />
                              {"  "}
                              {data.JobType}
                            </div>
                            <div className="suggested_sec_cat">
                              <i className="fa fa-user" aria-hidden="true" />{" "}
                                Ex {data.Experience} years
                              </div>
                            <div className="suggested_sec_cat">
                              <i
                                className="fa fa-map-marker"
                                aria-hidden="true"
                              />{" "}
                                San Fransisco
                              </div>
                            <div className="suggested_sec_cat">
                              <i
                                className="fa fa-share-alt"
                                aria-hidden="true"
                              />{" "}
                                Share
                              </div>
                          </div>
                        );
                      })
                      : null}
                    {/* <div className="suggested_sec">
                    <div className="suggested_sec_cat"><i className="fa fa-briefcase" aria-hidden="true" />  Ui/Ux designer</div>
                    <div className="suggested_sec_cat"><i className="fa fa-calendar" aria-hidden="true" /> Posted 1d ago</div>
                    <div className="suggested_sec_cat"><i className="fa fa-clock-o" aria-hidden="true" />  Part Time</div>
                    <div className="suggested_sec_cat"><i className="fa fa-user" aria-hidden="true" /> Ex 2-4 years</div>
                    <div className="suggested_sec_cat"><i className="fa fa-map-marker" aria-hidden="true" />  San Fransisco</div>
                    <div className="suggested_sec_cat"><i className="fa fa-share-alt" aria-hidden="true" /> Share</div>
                  </div>
                  <div className="suggested_sec">
                    <div className="suggested_sec_cat"><i className="fa fa-briefcase" aria-hidden="true" />  Ui/Ux designer</div>
                    <div className="suggested_sec_cat"><i className="fa fa-calendar" aria-hidden="true" /> Posted 1d ago</div>
                    <div className="suggested_sec_cat"><i className="fa fa-clock-o" aria-hidden="true" />  Part Time</div>
                    <div className="suggested_sec_cat"><i className="fa fa-user" aria-hidden="true" /> Ex 2-4 years</div>
                    <div className="suggested_sec_cat"><i className="fa fa-map-marker" aria-hidden="true" />  San Fransisco</div>
                    <div className="suggested_sec_cat"><i className="fa fa-share-alt" aria-hidden="true" /> Share</div>
                  </div>
                  <div className="suggested_sec">
                    <div className="suggested_sec_cat"><i className="fa fa-briefcase" aria-hidden="true" />  Ui/Ux designer</div>
                    <div className="suggested_sec_cat"><i className="fa fa-calendar" aria-hidden="true" /> Posted 1d ago</div>
                    <div className="suggested_sec_cat"><i className="fa fa-clock-o" aria-hidden="true" />  Part Time</div>
                    <div className="suggested_sec_cat"><i className="fa fa-user" aria-hidden="true" /> Ex 2-4 years</div>
                    <div className="suggested_sec_cat"><i className="fa fa-map-marker" aria-hidden="true" />  San Fransisco</div>
                    <div className="suggested_sec_cat"><i className="fa fa-share-alt" aria-hidden="true" /> Share</div>
                  </div>
                  <div className="suggested_sec">
                    <div className="suggested_sec_cat"><i className="fa fa-briefcase" aria-hidden="true" />  Ui/Ux designer</div>
                    <div className="suggested_sec_cat"><i className="fa fa-calendar" aria-hidden="true" /> Posted 1d ago</div>
                    <div className="suggested_sec_cat"><i className="fa fa-clock-o" aria-hidden="true" />  Part Time</div>
                    <div className="suggested_sec_cat"><i className="fa fa-user" aria-hidden="true" /> Ex 2-4 years</div>
                    <div className="suggested_sec_cat"><i className="fa fa-map-marker" aria-hidden="true" />  San Fransisco</div>
                    <div className="suggested_sec_cat"><i className="fa fa-share-alt" aria-hidden="true" /> Share</div>
                  </div> */}
                  </Scrollbars>
                </div>
              </div>
              <div className="section_box3">
                <h1>Suggested Job Titles</h1>
                <div
                  className="suggested_title_scroll"
                  style={{ overflow: "hidden", outline: "none" }}
                  tabIndex={0}
                >
                  <Scrollbars
                    style={{ height: 270 }}
                    autoHide
                    renderThumbVertical={({ style, ...props }) => (
                      <div
                        {...props}
                        style={{
                          ...style,
                          position: "relative",
                          display: "block",
                          width: "5px",
                          cursor: "pointer",
                          borderRadius: "inherit",
                          backgroundColor: "rgb(73, 69, 69)",
                          height: "115px",
                        }}
                      />
                    )}
                  >
                    <div className="suggested_titles">
                      <ul>
                        {suggestedJob.Data != undefined
                          ? suggestedJob.Data.map((data, i) => {
                            return (
                              <li key={i}>
                                <a href="#">{data.Title}</a>
                              </li>
                            );
                          })
                          : null}
                        {/* <li><a href="#">Software Engineer</a></li>
                      <li><a href="#">Developer</a></li>
                      <li><a href="#">Test Lead</a></li>
                      <li><a href="#">Dev Lead</a></li>
                      <li><a href="#">UI Developer</a></li>
                      <li><a href="#">UI Developer</a></li> */}
                      </ul>
                    </div>
                  </Scrollbars>
                </div>
              </div>
            </div>
          </div>
          <div className="clearfix" />
        </div>
      </div>

      <Modal
        show={isShareOpen}
        onHide={() => {
          setIsShareOpen(!isShareOpen);
        }}
      >
        <Modal.Header closeButton>
          <Modal.Title>Share this job post on</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="col-sm-12 m_t_30 text-center">
            <div className="social1">
              <FacebookShareButton
                url={`http://careerappui.clubactive.in/#/job_search/${currentJobId}`}
                quote={`${
                  filterFlag
                    ? jobAppliedCandidateResult != undefined &&
                      jobAppliedCandidateResult.data != undefined
                      ? jobAppliedCandidateResult.data.filter(
                        (data) => data.rowId === currentJobId
                      )[0] != undefined
                        ? jobAppliedCandidateResult.data.filter(
                          (data) => data.rowId === currentJobId
                        )[0].description
                        : ""
                      : ""
                    : jobAppliedCandidateResult.data != undefined &&
                      jobAppliedCandidateResult.data != undefined
                      ? jobAppliedCandidateResult.data.filter(
                        (data) => data.rowId === currentJobId
                      )[0] != undefined
                        ? jobAppliedCandidateResult.data.filter(
                          (data) => data.rowId === currentJobId
                        )[0].description
                        : ""
                      : ""
                  }`}
                hashtag={`${
                  filterFlag
                    ? jobAppliedCandidateResult != undefined &&
                      jobAppliedCandidateResult.data != undefined
                      ? jobAppliedCandidateResult.data.filter(
                        (data) => data.rowId === currentJobId
                      )[0] != undefined
                        ? jobAppliedCandidateResult.data.filter(
                          (data) => data.rowId === currentJobId
                        )[0].description
                        : ""
                      : ""
                    : jobAppliedCandidateResult.data != undefined &&
                      jobAppliedCandidateResult.data != undefined
                      ? jobAppliedCandidateResult.data.filter(
                        (data) => data.rowId === currentJobId
                      )[0] != undefined
                        ? jobAppliedCandidateResult.data.filter(
                          (data) => data.rowId === currentJobId
                        )[0].description
                        : ""
                      : ""
                  }`}
                className={""}
              >
                {/* <FacebookIcon size={36} /> */}
                <a href="#" className="social_face">
                  <i className="fa fa-facebook" aria-hidden="true" />
                </a>
              </FacebookShareButton>

              <TwitterShareButton
                url={`http://careerappui.clubactive.in/#/job_search/${currentJobId}`}
                via={`${
                  filterFlag
                    ? jobAppliedCandidateResult != undefined &&
                      jobAppliedCandidateResult.data != undefined
                      ? jobAppliedCandidateResult.data.filter(
                        (data) => data.rowId === currentJobId
                      )[0] != undefined
                        ? jobAppliedCandidateResult.data.filter(
                          (data) => data.rowId === currentJobId
                        )[0].description
                        : ""
                      : ""
                    : jobAppliedCandidateResult.data != undefined &&
                      jobAppliedCandidateResult.data != undefined
                      ? jobAppliedCandidateResult.data.filter(
                        (data) => data.rowId === currentJobId
                      )[0] != undefined
                        ? jobAppliedCandidateResult.data.filter(
                          (data) => data.rowId === currentJobId
                        )[0].description
                        : ""
                      : ""
                  }`}
                hashtags={[
                  `${
                  filterFlag
                    ? jobAppliedCandidateResult != undefined &&
                      jobAppliedCandidateResult.data != undefined
                      ? jobAppliedCandidateResult.data.filter(
                        (data) => data.rowId === currentJobId
                      )[0] != undefined
                        ? jobAppliedCandidateResult.data.filter(
                          (data) => data.rowId === currentJobId
                        )[0].description
                        : ""
                      : ""
                    : jobAppliedCandidateResult.data != undefined &&
                      jobAppliedCandidateResult.data != undefined
                      ? jobAppliedCandidateResult.data.filter(
                        (data) => data.rowId === currentJobId
                      )[0] != undefined
                        ? jobAppliedCandidateResult.data.filter(
                          (data) => data.rowId === currentJobId
                        )[0].description
                        : ""
                      : ""
                  }`,
                ]}
                className={""}
              >
                {/* <FacebookIcon size={36} /> */}
                <a href="#" className="social_twit">
                  <i className="fa fa-twitter" aria-hidden="true" />
                </a>
              </TwitterShareButton>

              <LinkedinShareButton
                url={`http://careerappui.clubactive.in/#/job_search/${currentJobId}`}
                title={`${
                  filterFlag
                    ? jobAppliedCandidateResult != undefined &&
                      jobAppliedCandidateResult.data != undefined
                      ? jobAppliedCandidateResult.data.filter(
                        (data) => data.rowId === currentJobId
                      )[0] != undefined
                        ? jobAppliedCandidateResult.data.filter(
                          (data) => data.rowId === currentJobId
                        )[0].title
                        : ""
                      : ""
                    : jobAppliedCandidateResult.data != undefined &&
                      jobAppliedCandidateResult.data != undefined
                      ? jobAppliedCandidateResult.data.filter(
                        (data) => data.rowId === currentJobId
                      )[0] != undefined
                        ? jobAppliedCandidateResult.data.filter(
                          (data) => data.rowId === currentJobId
                        )[0].title
                        : ""
                      : ""
                  }`}
                summary={`${
                  filterFlag
                    ? jobAppliedCandidateResult != undefined &&
                      jobAppliedCandidateResult.data != undefined
                      ? jobAppliedCandidateResult.data.filter(
                        (data) => data.rowId === currentJobId
                      )[0] != undefined
                        ? jobAppliedCandidateResult.data.filter(
                          (data) => data.rowId === currentJobId
                        )[0].description
                        : ""
                      : ""
                    : jobAppliedCandidateResult.data != undefined &&
                      jobAppliedCandidateResult.data != undefined
                      ? jobAppliedCandidateResult.data.filter(
                        (data) => data.rowId === currentJobId
                      )[0] != undefined
                        ? jobAppliedCandidateResult.data.filter(
                          (data) => data.rowId === currentJobId
                        )[0].description
                        : ""
                      : ""
                  }`}
                source={`http://careerappui.clubactive.in/#/job_search/${currentJobId}`}
              >
                <a href="#" className="social_twit">
                  <i className="fa fa-linkedin" aria-hidden="true" />
                </a>
              </LinkedinShareButton>
            </div>
          </div>
          <div className="modal-footer  m-t-30"></div>
        </Modal.Body>
      </Modal>
    </React.Fragment>
  );
};
export default CandidateJobAppliedComponent;
