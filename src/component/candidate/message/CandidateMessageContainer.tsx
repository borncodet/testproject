import React from "react";
import CandidateMessageComponent from "./CandidateMessageComponent";




interface ICandidateMessageContainerProps { }

interface ICandidateMessageContainerState { }

const initialState = {};

const CandidateMessageContainer: React.FC<ICandidateMessageContainerProps> = (props) => {
  const [CandidateMessageContainerState, setCandidateMessageContainerState] = React.useState<ICandidateMessageContainerState>(
    initialState
  );

  return (
    <>
      <CandidateMessageComponent/>
    </>
  );
};
export default CandidateMessageContainer;
