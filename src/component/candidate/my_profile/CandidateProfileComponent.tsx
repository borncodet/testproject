import React, { useContext, useState } from "react";
import Skills from "./Skills";
import Trainings from "./Trainings";
import Experience from "./Experience";
import Qualification from "./Qualification";
import Document from "./Document";
import Communication from "./Communication";
import PersonelInfo from "./PersonelInfo";
import {
  MyProfileSelectBoxDataViewModel,
  profileImageRequestModel,
} from "../../../models/candidate/MyProfileSelectBoxData";
import { Modal } from "react-bootstrap";
import { useForm, Controller } from "react-hook-form";
import { changePassword } from "./../../../apis/misc";
import { ErrorMessage } from "@hookform/error-message";
import { toast, ToastContainer } from "react-toastify";
import {
  GlobalSpinnerContext,
  useGlobalSpinnerContext,
} from "./../../../context/GlobalSpinner";
import { confirmWrapper, confirm } from "./../../GlobalConfirm";
import {
  updateSocialAccounts,
  saveCandidateProfileImage,
} from "./../../../apis/candidate";
import FileUploads from "../my_profile/components/FileUploads";
import DragAndDrop from "../my_profile/components/DragAndDrop";
import AuthService from "../../../services/AuthService";
import {
  getProfileImage,
  useMyProfileDispatcher,
} from "../../../action/MyProfileAction";

const authorizationToken = AuthService.accessToken;

function AddProfileImage(props: any) {
  const myProfileDispatcher = useMyProfileDispatcher();
  const defaultValues = {
    url: "",
    social: "",
  };
  const {
    register,
    handleSubmit,
    watch,
    errors,
    setValue,
    getValues,
    control,
    reset,
  } = useForm<IAddSocialState>({ defaultValues });
  const globalSpinner = useContext(GlobalSpinnerContext);
  const [obj, setObj] = useState<any>({ file: null });
  const onSubmit = (data: any) => {
    const data1 = {
      RowId:
        props.candidateProfileImage.data.length > 0
          ? Number(props.candidateProfileImage.data[0].rowId)
          : 0,
      CandidateProfileImageID:
        props.candidateProfileImage.data.length > 0
          ? Number(props.candidateProfileImage.data[0].rowId)
          : 0,
      CandidateId: Number(props.candidateId),
      imageUrl: obj.file.name,
      Document: obj.file,
      IsActive: true,
    };

    const formData = new FormData();
    formData.append("RowId", JSON.stringify(data1.RowId));
    formData.append(
      "CandidateProfileImageID",
      JSON.stringify(data1.CandidateProfileImageID)
    );
    formData.append("CandidateId", JSON.stringify(data1.CandidateId));
    formData.append("imageUrl", JSON.stringify(data1.imageUrl));
    formData.append("Document", data1.Document);
    formData.append("IsActive", JSON.stringify(data1.IsActive));

    props.setIsOpen3(false);
    globalSpinner.showSpinner();
    saveCandidateProfileImage(formData).then((res: any) => {
      console.log(res);
      if (res.data.isSuccess) {
        props.getCandidateProfileImage({
          CandidateId: Number(props.candidateId),
          Page: 1,
          PageSize: 10,
          SearchTerm: "string",
          SortOrder: "string",
          ShowInactive: false,
        });
        globalSpinner.hideSpinner();
        toast.success("Profile Image saved successfully.");
        // props.setIsOpen3(false);

        if (authorizationToken != null && Number(props.candidateId) != 0) {
          (async () => {
            await getProfileImage(
              myProfileDispatcher,
              {
                candidateId: Number(props.candidateId),
                page: 1,
                pageSize: 10,
                searchTerm: "",
                showInactive: false,
                sortOrder: "",
              } as profileImageRequestModel,
              authorizationToken
            );
          })();
        }
      } else {
        globalSpinner.hideSpinner();
        toast.error("Error:Profile Image not saved.");
      }
    });

    for (var key of (formData as any).entries()) {
      console.log(key[0] + ", " + key[1]);
    }
  };
  const handleDrop = (files: any) => {
    if (["image/jpeg"].includes(files[0].type) && files[0].size < 1000000 * 5) {
      console.log(files[0]);
      if (files.length > 0) {
        setObj({
          ...obj,
          ["file"]: files[0],
        });
      }
    }
  };
  return (
    <form onSubmit={handleSubmit(onSubmit)} noValidate>
      <Modal.Header closeButton>
        <Modal.Title>Add Profile Image</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div>
          <img
            src={require("../../../images/upload_img1.jpg")}
            className="center-block img-responsive"
          />
        </div>

        {/* <div className="box1">
                    <FileUploads />
                    <label htmlFor="file-1"><svg xmlns="http://www.w3.org/2000/svg" width={20} height={17} viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z" /></svg> <span>Upload Document</span></label>
                  </div> */}

        <FileUploads
          accept=".jpg,.jpeg"
          onChange={(file: any) => {
            handleDrop([file]);
          }}
          name="Choose a file"
          disabled={false}
          ref={register}
        />
        <DragAndDrop handleDrop={handleDrop}>
          {obj.file ? (
            <div className="update_con" style={{ width: 268, height: 105 }}>
              {obj.file.name}
            </div>
          ) : (
              <React.Fragment>
                {/* <div className="update_con">
                  Drag and drop your file here{" "}
                </div> */}
                <div className="update_con">Acceptable file formats: JPEG</div>
                <div className="update_con">Maximum file size: 5 MB. </div>
              </React.Fragment>
            )}
        </DragAndDrop>
      </Modal.Body>
      <div className="modal-footer  m-t-30">
        <button
          className="btn btn-success save-event waves-effect waves-light"
          type="submit"
        >
          {" "}
          Add{" "}
        </button>
        <button
          onClick={() => {
            props.setIsOpen3(false);
          }}
          data-dismiss="modal"
          className="btn btn-default waves-effect _cursor-pointer"
          type="button"
        >
          {" "}
          Cancel
        </button>
      </div>
      <div className="clearfix" />
    </form>
  );
}

interface IAddSocialState {
  url: string;
  social: string;
}

function AddSocial(props: any) {
  const defaultValues = {
    url: "",
    social: "",
  };

  const {
    register,
    handleSubmit,
    watch,
    errors,
    setValue,
    getValues,
    control,
    reset,
  } = useForm<IAddSocialState>({
    defaultValues,
  });

  const globalSpinner = useGlobalSpinnerContext();

  const onSubmit = (data: any) => {
    globalSpinner.showSpinner();

    const item =
      props.socialAccounts.data.length > 0
        ? props.socialAccounts.data[0]
        : {
          RowId: 0,
          SocialAccountId: 0,
          Facebooks: "",
          Google: "",
          Twitter: "",
          LinkedIn: "",
          Pinterest: "",
          Instagram: "",
          Other: "",
          CandidateId: Number(props.candidateId),
          IsActive: true,
        };

    if (data["social"] == "google") {
      item["google"] = data["url"];
    } else if (data["social"] == "facebooks") {
      item["facebooks"] = data["url"];
    } else if (data["social"] == "twitter") {
      item["twitter"] = data["url"];
    } else if (data["social"] == "linkedIn") {
      item["linkedIn"] = data["url"];
    }

    console.log(item);

    updateSocialAccounts(item)
      .then((res) => {
        console.log(res);
        globalSpinner.hideSpinner();
        if (res.data.isSuccess) {
          props.getSocialAccounts({
            CandidateId: Number(props.candidateId),
            Page: 1,
            PageSize: 10,
            SearchTerm: "",
            SortOrder: "",
            ShowInactive: false,
          });
          toast.success("Social Account added.");
          props.setIsOpen2(false);
        } else {
          toast.error("Something went wrong.");
        }
      })
      .catch((err) => {
        console.log(err);
        globalSpinner.hideSpinner();
        toast.error("Something went wrong.");
      });

    console.log(data);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)} noValidate>
      <Modal.Header closeButton>
        <Modal.Title>Add Social media</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="col-sm-12">
          <div className="form-group">
            {/* <label htmlFor="email">Social Media URL</label> */}
            <label className={"required"}> Social Media URL</label>
            <input
              type="text"
              className="form-control "
              placeholder="Add"
              name="url"
              ref={register({
                required: "URL is required",
              })}
            />
            <ErrorMessage
              errors={errors}
              name="url"
              render={({ message }) => (
                <div style={{ paddingTop: "2px" }} className="login_validation">
                  {message}
                </div>
              )}
            />
          </div>

          <div className="form-group">
            <div className="row">
              <div className="col-xs-6">
                {/* <label htmlFor="email">Choose social account</label> */}
                <label className={"required"}> Choose social account</label>
              </div>
            </div>
            <div className="row">
              <div className="col-xs-3">
                <input
                  disabled={
                    props.socialAccounts.data.length > 0 &&
                    props.socialAccounts.data[0].google
                  }
                  id="radio1"
                  type="radio"
                  name="social"
                  value="google"
                  ref={register({
                    required: "Social account is required",
                  })}
                />
                <label htmlFor="radio1">
                  <span>
                    <span />
                  </span>
                  Google
                </label>
              </div>
              <div className="col-xs-3">
                <input
                  disabled={
                    props.socialAccounts.data.length > 0 &&
                    props.socialAccounts.data[0].facebooks
                  }
                  id="radio2"
                  type="radio"
                  name="social"
                  value="facebooks"
                  ref={register({
                    required: "Social account is required",
                  })}
                />
                <label htmlFor="radio2">
                  <span>
                    <span />
                  </span>
                  Facebook
                </label>
              </div>
              <div className="col-xs-3">
                <input
                  disabled={
                    props.socialAccounts.data.length > 0 &&
                    props.socialAccounts.data[0].twitter
                  }
                  id="radio1"
                  type="radio"
                  name="social"
                  value="twitter"
                  ref={register({
                    required: "Social account is required",
                  })}
                />
                <label htmlFor="radio1">
                  <span>
                    <span />
                  </span>
                  Twitter
                </label>
              </div>
              <div className="col-xs-3">
                <input
                  disabled={
                    props.socialAccounts.data.length > 0 &&
                    props.socialAccounts.data[0].linkedIn
                  }
                  id="radio2"
                  type="radio"
                  name="social"
                  value="linkedIn"
                  ref={register({
                    required: "Social account is required",
                  })}
                />
                <label htmlFor="radio2">
                  <span>
                    <span />
                  </span>
                  LinkedIn
                </label>
              </div>
              <div className="row">
                <ErrorMessage
                  errors={errors}
                  name="social"
                  render={({ message }) => (
                    <div
                      className="login_validation"
                      style={{
                        paddingTop: "30px",
                        paddingLeft: "19px",
                      }}
                    >
                      {message}
                    </div>
                  )}
                />
              </div>
            </div>
          </div>
        </div>
      </Modal.Body>
      <div className="modal-footer  m-t-30">
        <button
          className="btn btn-success save-event waves-effect waves-light"
          type="submit"
        >
          {" "}
          Add{" "}
        </button>
        <button
          onClick={() => {
            props.setIsOpen2(false);
          }}
          data-dismiss="modal"
          className="btn btn-default waves-effect _cursor-pointer"
          type="button"
        >
          {" "}
          Cancel
        </button>
      </div>
      <div className="clearfix" />
    </form>
  );
}

interface ICandidateProfileComponentProps {
  myProfile: MyProfileSelectBoxDataViewModel;
  candidateExperienceSelectBoxData: any;
  candidateRelativeSelectBoxData: any;
  candidateExperiences: any;
  getCandidateExperiences: any;
  userId: any;
  candidateId: any;
  candidateState: any;
  getCandidates: any;
  getCandidateRelatives: any;
  candidateRelativesState: any;
  candidateLanguageMaps: any[];
  getCandidateLanguageMaps: any;
  getCandidateSkills: any;
  candidateSkillsState: any;
  getTrainings: any;
  trainingState: any;
  getBankDetails: any;
  bankDetailsState: any;
  educationQualificationState: any;
  getEducationQualifications: any;
  getCandidateAchievements: any;
  candidateAchievementsState: any;
  candidateReferences: any;
  getCandidateReferences: any;
  progressBar: any;
  socialAccounts: any;
  getSocialAccounts: any;
  passportInformation: any;
  getPassportInformation: any;
  getCandidateProfileImage: any;
  candidateProfileImage: any;
  candidateOtherCertificate: any;
  getCandidateOtherCertificates: any;
  seamanBookCdc: any;
  getSeamanBookCdc: any;
  digiDocumentDetails: any;
  getDigiDocumentDetails: any;
  digiLockerType: any;
  digiLockers: any;
}

interface ICandidateProfileComponentState { }

interface IChangePasswordState {
  password: string;
  confirmPassword: string;
  oldPassword: string;
}

const initialState = {};

const CandidateProfileComponent: React.FC<ICandidateProfileComponentProps> = (
  props
) => {
  const [
    CandidateProfileComponentState,
    setCandidateProfileComponentState,
  ] = React.useState<ICandidateProfileComponentState>(initialState);

  const [active, setActive] = useState("Personal Info");

  const titles = [
    "Personal Info",
    "Communication",
    "Document",
    "Qualification",
    "Experience",
    "Trainings",
    "Skills",
  ];

  const _titles = titles.map((e, i) => {
    return (
      <li
        key={i}
        onClick={() => {
          setActive(e);
        }}
        className={
          e == active ? "resp-tab-item resp-tab-active" : "resp-tab-item"
        }
        aria-controls={`tab_item-${i}`}
        role="tab"
      >
        {e}
      </li>
    );
  });

  let tab = null;

  console.log(props.candidateProfileImage);

  if (active == "Personal Info") {
    tab = (
      <PersonelInfo
        myProfile={props.myProfile}
        getCandidates={props.getCandidates}
        candidateState={props.candidateState}
        userId={props.userId}
        candidateId={props.candidateId}
        candidateLanguageMaps={props.candidateLanguageMaps}
        getCandidateLanguageMaps={props.getCandidateLanguageMaps}
      />
    );
  } else if (active == "Communication") {
    tab = (
      <Communication
        myProfile={props.myProfile}
        getCandidates={props.getCandidates}
        candidateState={props.candidateState}
        getCandidateRelatives={props.getCandidateRelatives}
        candidateRelativesState={props.candidateRelativesState}
        userId={props.userId}
        candidateId={props.candidateId}
        candidateReferences={props.candidateReferences}
        getCandidateReferences={props.getCandidateReferences}
        candidateRelativeSelectBoxData={props.candidateRelativeSelectBoxData}
      />
    );
  } else if (active == "Document") {
    tab = (
      <Document
        myProfile={props.myProfile}
        getCandidates={props.getCandidates}
        candidateState={props.candidateState}
        userId={props.userId}
        candidateId={props.candidateId}
        getBankDetails={props.getBankDetails}
        bankDetailsState={props.bankDetailsState}
        getPassportInformation={props.getPassportInformation}
        passportInformation={props.passportInformation}
        seamanBookCdc={props.seamanBookCdc}
        getSeamanBookCdc={props.getSeamanBookCdc}
        digiLockerType={props.digiLockerType}
        digiLockers={props.digiLockers}
      />
    );
  } else if (active == "Qualification") {
    tab = (
      <Qualification
        educationQualificationState={props.educationQualificationState}
        getEducationQualifications={props.getEducationQualifications}
        userId={props.userId}
        candidateId={props.candidateId}
        digiLockerType={props.digiLockerType}
        digiLockers={props.digiLockers}
      />
    );
  } else if (active == "Experience") {
    tab = (
      <Experience
        myProfile={props.myProfile}
        candidateExperienceSelectBoxData={
          props.candidateExperienceSelectBoxData
        }
        getCandidateAchievements={props.getCandidateAchievements}
        candidateAchievementsState={props.candidateAchievementsState}
        candidateExperiences={props.candidateExperiences}
        getCandidateExperiences={props.getCandidateExperiences}
        userId={props.userId}
        candidateId={props.candidateId}
      />
    );
  } else if (active == "Trainings") {
    tab = (
      <Trainings
        getTrainings={props.getTrainings}
        trainingState={props.trainingState}
        userId={props.userId}
        candidateId={props.candidateId}
        candidateOtherCertificate={props.candidateOtherCertificate}
        getCandidateOtherCertificates={props.getCandidateOtherCertificates}
        digiDocumentDetails={props.digiDocumentDetails}
        getDigiDocumentDetails={props.getDigiDocumentDetails}
        digiLockerType={props.digiLockerType}
        digiLockers={props.digiLockers}
      />
    );
  } else if (active == "Skills") {
    tab = (
      <Skills
        getCandidateSkills={props.getCandidateSkills}
        candidateSkillsState={props.candidateSkillsState}
        userId={props.userId}
        candidateId={props.candidateId}
      />
    );
  }

  const defaultValues = {
    password: "",
    confirmPassword: "",
    oldPassword: "",
  };

  const {
    register,
    handleSubmit,
    watch,
    errors,
    setValue,
    getValues,
    control,
    reset,
  } = useForm<IChangePasswordState>({
    defaultValues,
  });

  const globalSpinner = useGlobalSpinnerContext();

  const [isOpen, setIsOpen] = useState<boolean>(false);

  const [isOpen2, setIsOpen2] = useState<boolean>(false);
  const [isOpen3, setIsOpen3] = useState<boolean>(false);

  const [showPassword, setShowPassword] = useState<Boolean>(false);

  const [showPassword2, setShowPassword2] = useState<Boolean>(false);

  const onSubmit = (data: any) => {
    console.log(data);
    globalSpinner.showSpinner();
    changePassword({
      CurrentPassword: data["currentPassword"],
      NewPassword: data["newPassword"],
    })
      .then((res: any) => {
        console.log(res.data);

        const r = res.data || "";

        if (r.toLowerCase().includes("please")) {
          toast.error(r);
        } else {
          toast.success("Password updated.");
          setIsOpen(false);
        }

        globalSpinner.hideSpinner();
      })
      .catch((err: any) => {
        console.log(err);
        globalSpinner.hideSpinner();
        toast.error("Something went wrong.");
      });
  };

  console.log(props.socialAccounts.data);

  let socialAccountList: any[] = [];

  const removeSocialAccount = async (id: any, item: any) => {
    if (
      await confirm({
        confirmation: "Are you sure you want to delete this?",
        options: {
          yes: "Yes",
          no: "No",
          header: "Delete",
        },
      })
    ) {
      console.log("yes");
      globalSpinner.showSpinner();

      if (id == "google") {
        item["google"] = "";
      } else if (id == "facebooks") {
        item["facebooks"] = "";
      } else if (id == "twitter") {
        item["twitter"] = "";
      } else if (id == "linkedIn") {
        item["linkedIn"] = "";
      }

      console.log(item);

      updateSocialAccounts(item)
        .then((res) => {
          console.log(res);
          globalSpinner.hideSpinner();
          if (res.data.isSuccess) {
            props.getSocialAccounts({
              CandidateId: Number(props.candidateId),
              Page: 1,
              PageSize: 10,
              SearchTerm: "",
              SortOrder: "",
              ShowInactive: false,
            });
            toast.success("Social Account removed.");
          } else {
            toast.error(res.data.message);
          }
        })
        .catch((err) => {
          console.log(err);
          globalSpinner.hideSpinner();
          toast.error("Something went wrong.");
        });
    } else {
      console.log("no");
    }
  };

  if (props.socialAccounts.data.length > 0) {
    if (props.socialAccounts.data[0].google) {
      socialAccountList.push(
        <div className="profile_social_icons">
          <div className="add_icons1">
            <a
              onClick={() => {
                removeSocialAccount("google", props.socialAccounts.data[0]);
              }}
              className="_cursor-pointer"
            >
              <img src={require("./../../../images/close_icon_1.png")} />
            </a>{" "}
          </div>
          <div className="profile_social">
            {" "}
            <a>
              <img src={require("./../../../images/google.png")} />{" "}
            </a>{" "}
          </div>
        </div>
      );
    }

    if (props.socialAccounts.data[0].facebooks) {
      socialAccountList.push(
        <div className="profile_social_icons">
          <div className="add_icons1">
            <a
              onClick={() => {
                removeSocialAccount("facebooks", props.socialAccounts.data[0]);
              }}
              className="_cursor-pointer"
            >
              <img src={require("./../../../images/close_icon_1.png")} />
            </a>{" "}
          </div>
          <div className="profile_social">
            <a>
              <img src={require("./../../../images/facebook.png")} />
            </a>
          </div>
        </div>
      );
    }

    if (props.socialAccounts.data[0].twitter) {
      socialAccountList.push(
        <div className="profile_social_icons">
          <div className="add_icons1">
            <a
              onClick={() => {
                removeSocialAccount("twitter", props.socialAccounts.data[0]);
              }}
              className="_cursor-pointer"
            >
              <img src={require("./../../../images/close_icon_1.png")} />
            </a>{" "}
          </div>
          <div className="profile_social">
            <a>
              <img src={require("./../../../images/twitter.png")} />
            </a>{" "}
          </div>
        </div>
      );
    }

    if (props.socialAccounts.data[0].linkedIn) {
      socialAccountList.push(
        <div className="profile_social_icons">
          <div className="add_icons1">
            <a
              onClick={() => {
                removeSocialAccount("linkedIn", props.socialAccounts.data[0]);
              }}
              className="_cursor-pointer"
            >
              <img src={require("./../../../images/close_icon_1.png")} />
            </a>{" "}
          </div>
          <div className="profile_social">
            <a>
              <img src={require("./../../../images/linkedin.png")} />
            </a>
          </div>
        </div>
      );
    }
  }

  return (
    <React.Fragment>
      <div className="content-wrapper">
        <div className="container-fluid">
          <h1 className="heading">Profile</h1>
          <div className="clearfix" />
          <div className="row ">
            <div className="col-sm-12">
              <div className=" section_box_m">
                <div className="profile_sec_photo">
                  {props.candidateProfileImage.data.length > 0 ? (
                    <img
                      width="154"
                      height="187"
                      src={`https://localhost:44361/Upload/ProfileImage/${props.candidateProfileImage.data[0].imageUrl}`}
                      alt=""
                      className="image1"
                    />
                  ) : (
                      //<img width="154" height="187" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMEAAACmCAYAAABjs+t+AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAAAAhdEVYdENyZWF0aW9uIFRpbWUAMjAyMDoxMToxMSAxMjoyNzoxMK7z7acAABEiSURBVHhe7Z33i1RXFMfvbjQWlFiwYW+xgAUs2GLDqLCxGwiRBH/zX/KH5BdFUTRRREGISESjYsEWYtegYsUSDMbEhI2f67vr7Djz5r2ZNzvv3Hs+sMnM7Lr7yvneU+659zX99ddfrSZwOnXqZJqamsyrV6/M9evXzd27d829e/fMRx99ZL9857///rNfffv2NSNGjDDDhg0zAwYMMK2trebff/+NfspfghVBseHfuHHDPHnyxH4vFOMvhRNEt27dzMSJE60g+vfvb7/nqyCCEwHGD4z0Fy5cMA8ePLA3PWTDL4cTRL9+/cyYMWOsKDp37uydh/BaBG60f/nypbly5Yq5dOmS+eeff9TgY8Do4c2bN2bdunXeewHwSgRulMfgf/31V/P06VM1+Bpx3oBrOHXqVDNp0iTTpUsX9QR5A+PnphDXnzp1yrw9J/Pxxx9H31WywnmJsWPHmmnTppkePXp4IQbRIigc+dX4Ow7nHcaPH29mzpxpk2jJYhAtAh35G4sTA16BUEkqzdH/xbF3715z5MgRexNUAI2BPIFrf/78eeuVnWeWhjgRUKJ79uyZrfio8ecDxLBlyxZbdub+SENMOORGmePHj9tSpwogf1B+Hj16tJk1a5aopDnXIsDw//77b3P06FFz8+ZNNXxBIAjaMJYvX266du0afZpPci2CO3futMX9WuuXh0ucN23alGuvkNucAC/w008/2dcqAJm4xHn37t3m9evXuU2ccykCkqvHjx9r+OMJL168MDt27LDzOXlMnHMXDjFaXLx40Zw4cUJF4BkucV64cKG9z3kJkXIjApcEHzp0yJbaVAB+4uZ1SJj79OkTfdpYchEOIYA///zT7Ny507Y2qwD8hTwBj7Bnz57chEYNFwEXgkUt27Zt0ypQILiE+ZdffsmFEBoaDnEBTp8+bc6ePaujf4DgEViws3btWvu+UTlCQ0Xw888/6yRY4OD9e/XqZXOERs0yd6gIXOx/8OBBWzbzOfRxE0VAqzE3mBlUVmyxWssJ3yWHjIr0RPE5r58/f25/luvEl/td7pr5eO0471WrVpmBAwd2qBjqLgJn+Lt27fIm5ncGPmTIEPPpp5/aRSYs4+zotbdcW6CahkcltwIfrjGTa3PnzjVTpkyxg0E9qasInAB+/PFHq3LpN8cZP0sMJ0+ebHr27JmLRedODG/vpRUCayw4TulhJjazYMECM2HChLoKoW4i8EkAGBSwimrcuHG5XmPLdefYfvvtN1tw4NglX3vnERh06nXNMxeBL8bPsRPDz58/325EVW+XXC+owLH2gjyMjQekegfuR73yhbp4gn379tkkT7IAWDI4Y8YML/bYceHSmTNnRJejuS8bNmzIvIqUqQgYdX744QexAnBx9NKlS82gQYPEjv7l4P4wI79//36xAxTHzbxClkLIbMaYC0zdn60MpV5gypVffvmldbm+CQA4J86N83R5jjTwBoR2WXqCTESAANjSkFZZya525cqV9lykhz9xcG6cJ0LgnKXBAMu8CSE39yoLahYB8Sa7ONMHIlkA7KETEmyxyDlLFQIRR1a9RzWJwFWCDhw4kPt1pOVwAqDHPSQIjThnqUJgwGXDhWvXrrUl/tVSkwhwrZRCpeYAhQLwMQeoBOdMDZ4mNok5AkI4fPiwHYhrEUJNIiARxpCkioC2h1AFUAjVFgxKqhAYiGvJ46oWAcqjX0WqAKClpSV4AThcO7NEGIgZkKvND6oSgcsFpCbCILVEWA8YRam7L168WGR+wEDMgHz58uWqwqKqPQG1Wqlwo2nM8rkUmhauBYvgaQ6UKAQGZPaoYnBOS2oR4HLYCpE+FIngAbjZtEAr7SE0nDNnju2ZkggegcE5rTdIJQJ++aNHj0TvBcqF+uyzzzQXiGHevHliwyIGZ3qk0pDaE6A0qQLgxs6ePdu2QiulISyibwpvKRFskybBNN4glQgIg1i4IRXcPPMCmgvEg5fEW0olbcUysQhQluQwCC+Am1eSgbeUWkFDBOximJRUniCtwvICN5OJMdy8eoFkSL9OLDFNSiIR4AVQlmQRsEiGBTJKcnh4t+T5lKSTZ4lEwKjABrlSIRfo6G08fICBQ3JIRHdzkgQ5VgT8AlaJff/992JzAW7i6tWrVQBVQF5Aa4nEcinQ3cwal0pCiBUBe+mwTaLkMIg9gbJafBEaDBzkUmweJhEG7mPHjtndzuMoKwLUw4ZZrEmVKgKOm1ZhnRirDbZIlOoNsIHt27fHhnUlRYAA2NWM2TfJXmDo0KGJYkKlPHiDwm0jJYItnDx5smxEUFIEhEH8I6kCAE58+PDh0TulVhhQ4kbTPIMdkxuw/1IpPhABIydtqZK9gEPnBbKD3ECqCABbpsxfKjL4QAR4AR7TL10AbidopXZcgiwZ7JmtKUslye1EgEroEpW8dxBoKJQ9DCiS8wIHrT/F3qCdCKSXRAvp3bt39ErJAmyDBFl6SIQ3KKadCEgcJJdEHdwoZoiV7KDlhI2JpUMXNNvXF3qDNhHwYZrOu7zTvXt3TYoz5pNPPhHtCYCQjt0S8WyONhFgMLgKH0IhzkGT4uxBBD5A5ZPc13kDKwLe3L59W7zKHTwIrlDpSu0wSOJdfRkk2ZnCYUWAwZA1+3CCCBkRaNt09vjSg4Wdkxe4cqkVgQ9l0UIYsZTsoauUAcYHGCyJfoiCmvnPjRs3vBEAaMNcffApxMTeyYE5J+sJfBOBzhEolcDeiX6YFmh++PCh6B0kisHNSe1/zzvkWVJ3sC4FQqCxrvnWrVteeQFFSQp2//vvv5tm30IhRUkDcwbNPoVCipIWHECzegEldFQEStBYTxC9VpRg8VIEvpTw8gYTS9U8BCPveCcC3BttIEp9oJDiWwjtpSfQzbaUNHgpAu0dqg9c11evXkXv/MFLETAB4lOzV14g15K6E10cXuYEJG+6niB7/vjjDy+LDl56AkYrXV+cLbTc++gFwEsRMFqxpbySLXhYHydXvRQBcMPcQmolG9ik2Ue8FAGjlXqC7Hnx4oV6Ailwo9hPlUfOqjfIhu+++67srs7S8TYcQgj379+P3inVwiDC6kOfW1G8FQEwX+Bjr0tHc+fOHS/DIIfXIgCSOQ2JasP31Ydei4AbxwNHdPa4evCkvq8+9F4E7LKtvUTVgQdlNwafvQB4Hw6R0BHTakiUHjwouzGoCITDDaRcqiFReliXQXHBd4IQARNnPrYA15tQtuPxXgSO4qeTKJW5evWqisAXuJHXrl2L3ilJYMDwtWu0mGA8AX0vBw4cUG+QEEqjPjytMgnBiABvwMSZziBXhjXaZ8+ejd75TzAiAIRw8uRJXYhfAYoI5FChEJwImEH2tRsyCwgXz507F70Lg6BEAOoN4uE5XqFUhRxBikC9QWnwAjzA0ee26VIEJwJw3kBpD41yJMQheQEIVgR4Ay2XvofwkFwgNAFAkCIAbrb2E72H8JCnOaoIAkK9wXvwAkeOHInehUewIoBjx44Fv0kXgwAtJUwkhugFIGgR0Btz6tSpoMullEQZDEJpkShF0CJg5KMkSDwcYljEOR86dCi4kmgxQYsAEMLBgwejd+GA97t48WLQYZBDRfDWAFg9debMmegT/8EDsGrsxIkTQYdBjuBFABgCk0ShhETkAXg/FcA7VAQRIYUE5AGhLJhJgoogAhFQK/e9UsT5aR7QHhVBAeyx4/MEGud19+5dDYOKUBEUgHEcPnzY22cbuFxAaY+KoAQ+Ggph0NGjR6N3SiEqgiKIlVmUT/LoS36AV7t8+bIN9TQX+BAVQQkwFGLnzZs3m9evX4sOjTh2Ev7QWyPiUBHEgBikh0bkNyFsqlsLKoIYMBz6irZs2SLWG+zYsUM9QAVUBAlg2eHjx49FCYFjZTfu0JvjkqAiSAAeged2SYNj1jCoMiqChOAJJMHS0efPn0fvlDhUBB5Dd6x6gsqoCBLie09RyKgIEkByOXTo0OidHLp37x69UuJQESRk8ODBohblt7a2mn79+ml1KAHNepHioe9+2rRppkuXLtEncsB76f2tjIogBgQwevRoM336dHFbs3C8o0aNUm9QAa6NhkNlcAJYsmSJ2L2JOO6WlhbTq1cvFUIZrAh0pGiPW3a4ePFi8/nnn4sVgIMS6YoVK8ygQYN0SWUJuD5Nly5daqXLUPtL3jFp0iQzdepU23YgXQCFUOKlnZodJpR3MPiPGDHCNA8fPjz6KEy4ELRL9+zZ03zzzTc2/gefBABv3rwx48ePN19//bX1COr939178qamtze7df/+/ebBgwdBzS46Ixg7dqyZMGGCGTBggDWUEOA82YadXagh1FllbGDjxo2m6e0FaeUhbaytDSEk4sT5IhciaXSlT99G/koQ7tEdy70/f/68fY0YQhEENsAAuGDBAtP09uRb+XDr1q32m77ijJ+Kz+TJk4Ma+eNw7eFsw3LhwgUbEYDvYiAkXLVqlRk4cOA7EXAhSI4ZFXw7eQyfcxo3bpw1fmJ/ZlNDG/krgQ3Qeeoe38p27ay19lUM2MH69eutHbR5gmfPnpk9e/Z4ExJh/EClh4oPYY8afjKcd2BgvHXrlh01EYMvgsA2Zs6caQfFdiKghLZr1y7xT3V0Iz+Gr8ZfG04MCIG84cmTJ16IARuhEujOo00Eju3bt7eNohLgWPmi/Ddr1iw1+jriQiYGShbv82wHvISk6IHj3bBhg+natWv0SZEIOEn2rJewZbcz/iFDhtgMv0ePHmr8HQi24h78jZeQMHByjMycf/HFF+2KIu16hzAiEkgJ0A+zZs0ae0KoWgXQsXC9CSeIq7/99ls7wuYdRDBlyhRbGCnkg3DIgVLYaqTRHoGL60Z7Mnota8rAeQqe+9DoR8NiQ/SBMTtcarAs20XarVs3m1g20s1x4Th4N9qrAOTgPMWcOXNsf04jPUXfvn3LCgDKioB/wGKSRomAv0ufCwevxi8X7IiBjNl5RNHR9oT4KJjEEbuegErL7NmzO1zF/L0+ffrY1+XUq8iBQYyQltxh4sSJtmGxI0BwtMewwi7OjsrmBIV0VNmUv8EXwnMTGYpfMB/FQwPZ47XeNsVgWlwOLUXFlWUkOPPmzau7N+D3U/HhoFUA/oJX6N+/v/UK9fYIzB1ROq9ERRFgjDSd4VbqBQIgCaeXQ+v9/sP95WvZsmVt3r8ezJ07N5EtVRQBoF68Qda4kydpcgesAggHih5fffWVLcNnLQQGVqKYJCQSAeDCsjxQF/5wEUiatAIUHgx4eH6qgFmvgSZySTqgJhYBvzCriTNOlnht9erVNlHS0T9c3L1nLgibyEoIaSKXxCIAdmCo9SD598w/LFy4MPpEUd6F3HQFYBtZ2BiLppKSqERaTDUlUyoBixYtsut5NfRR4iA6OH36tG25SBt9YGfr1q1rm2dKQipPACQbS5cuTaVWflYFoCQFG2HXj7ThET87Y8aMVF4AUouAGI4kBreVBA6Mn1UBKGnAzgiPkm4Ox8/QI4R40tpZahGAU2qlg+P7zDGgThWAkhaEQPk8aUi0fPnyqoosVYkAkvwxSqDs5akCUKqF/rVKBRm+hwCSzA6XomoRQNzBkaDMnz8/eqco1cFgSwMc+UGpyAP7o9ugUpNcHDWJgFCnVEmLg3UJSrUHpigOIgnaoYsX5mBn5AysWagl2qiqRFoMB7Nz504rBl7jIdjdSwWgZAmVSVY7YmPYGstr6WSo1c5q8gQO4ra1a9dapZLEjBw5UgWg1AX2kSLUZqFOFgKATDwBoFK28uMB0hKf7KLIAAGw3UuWNpaZCMB17akAlHqCnWVpY5mKQFEkkklOoCiSUREowaMiUIJHRaAEj4pACR4VgRI4xvwPmYiAC4AtHyIAAAAASUVORK5CYII="></img>
                      <img
                        width="154"
                        height="187"
                        src={require("./../../../images/profileDefault1.jpg")}
                      ></img>
                    )}
                  {/* 
                  <img src={require("./../../../images/profile_img.jpg")} />
                  <img width="154" height="187" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMEAAACmCAYAAABjs+t+AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAAAAhdEVYdENyZWF0aW9uIFRpbWUAMjAyMDoxMToxMSAxMjoyNzoxMK7z7acAABEiSURBVHhe7Z33i1RXFMfvbjQWlFiwYW+xgAUs2GLDqLCxGwiRBH/zX/KH5BdFUTRRREGISESjYsEWYtegYsUSDMbEhI2f67vr7Djz5r2ZNzvv3Hs+sMnM7Lr7yvneU+659zX99ddfrSZwOnXqZJqamsyrV6/M9evXzd27d829e/fMRx99ZL9857///rNfffv2NSNGjDDDhg0zAwYMMK2trebff/+NfspfghVBseHfuHHDPHnyxH4vFOMvhRNEt27dzMSJE60g+vfvb7/nqyCCEwHGD4z0Fy5cMA8ePLA3PWTDL4cTRL9+/cyYMWOsKDp37uydh/BaBG60f/nypbly5Yq5dOmS+eeff9TgY8Do4c2bN2bdunXeewHwSgRulMfgf/31V/P06VM1+Bpx3oBrOHXqVDNp0iTTpUsX9QR5A+PnphDXnzp1yrw9J/Pxxx9H31WywnmJsWPHmmnTppkePXp4IQbRIigc+dX4Ow7nHcaPH29mzpxpk2jJYhAtAh35G4sTA16BUEkqzdH/xbF3715z5MgRexNUAI2BPIFrf/78eeuVnWeWhjgRUKJ79uyZrfio8ecDxLBlyxZbdub+SENMOORGmePHj9tSpwogf1B+Hj16tJk1a5aopDnXIsDw//77b3P06FFz8+ZNNXxBIAjaMJYvX266du0afZpPci2CO3futMX9WuuXh0ucN23alGuvkNucAC/w008/2dcqAJm4xHn37t3m9evXuU2ccykCkqvHjx9r+OMJL168MDt27LDzOXlMnHMXDjFaXLx40Zw4cUJF4BkucV64cKG9z3kJkXIjApcEHzp0yJbaVAB+4uZ1SJj79OkTfdpYchEOIYA///zT7Ny507Y2qwD8hTwBj7Bnz57chEYNFwEXgkUt27Zt0ypQILiE+ZdffsmFEBoaDnEBTp8+bc6ePaujf4DgEViws3btWvu+UTlCQ0Xw888/6yRY4OD9e/XqZXOERs0yd6gIXOx/8OBBWzbzOfRxE0VAqzE3mBlUVmyxWssJ3yWHjIr0RPE5r58/f25/luvEl/td7pr5eO0471WrVpmBAwd2qBjqLgJn+Lt27fIm5ncGPmTIEPPpp5/aRSYs4+zotbdcW6CahkcltwIfrjGTa3PnzjVTpkyxg0E9qasInAB+/PFHq3LpN8cZP0sMJ0+ebHr27JmLRedODG/vpRUCayw4TulhJjazYMECM2HChLoKoW4i8EkAGBSwimrcuHG5XmPLdefYfvvtN1tw4NglX3vnERh06nXNMxeBL8bPsRPDz58/325EVW+XXC+owLH2gjyMjQekegfuR73yhbp4gn379tkkT7IAWDI4Y8YML/bYceHSmTNnRJejuS8bNmzIvIqUqQgYdX744QexAnBx9NKlS82gQYPEjv7l4P4wI79//36xAxTHzbxClkLIbMaYC0zdn60MpV5gypVffvmldbm+CQA4J86N83R5jjTwBoR2WXqCTESAANjSkFZZya525cqV9lykhz9xcG6cJ0LgnKXBAMu8CSE39yoLahYB8Sa7ONMHIlkA7KETEmyxyDlLFQIRR1a9RzWJwFWCDhw4kPt1pOVwAqDHPSQIjThnqUJgwGXDhWvXrrUl/tVSkwhwrZRCpeYAhQLwMQeoBOdMDZ4mNok5AkI4fPiwHYhrEUJNIiARxpCkioC2h1AFUAjVFgxKqhAYiGvJ46oWAcqjX0WqAKClpSV4AThcO7NEGIgZkKvND6oSgcsFpCbCILVEWA8YRam7L168WGR+wEDMgHz58uWqwqKqPQG1Wqlwo2nM8rkUmhauBYvgaQ6UKAQGZPaoYnBOS2oR4HLYCpE+FIngAbjZtEAr7SE0nDNnju2ZkggegcE5rTdIJQJ++aNHj0TvBcqF+uyzzzQXiGHevHliwyIGZ3qk0pDaE6A0qQLgxs6ePdu2QiulISyibwpvKRFskybBNN4glQgIg1i4IRXcPPMCmgvEg5fEW0olbcUysQhQluQwCC+Am1eSgbeUWkFDBOximJRUniCtwvICN5OJMdy8eoFkSL9OLDFNSiIR4AVQlmQRsEiGBTJKcnh4t+T5lKSTZ4lEwKjABrlSIRfo6G08fICBQ3JIRHdzkgQ5VgT8AlaJff/992JzAW7i6tWrVQBVQF5Aa4nEcinQ3cwal0pCiBUBe+mwTaLkMIg9gbJafBEaDBzkUmweJhEG7mPHjtndzuMoKwLUw4ZZrEmVKgKOm1ZhnRirDbZIlOoNsIHt27fHhnUlRYAA2NWM2TfJXmDo0KGJYkKlPHiDwm0jJYItnDx5smxEUFIEhEH8I6kCAE58+PDh0TulVhhQ4kbTPIMdkxuw/1IpPhABIydtqZK9gEPnBbKD3ECqCABbpsxfKjL4QAR4AR7TL10AbidopXZcgiwZ7JmtKUslye1EgEroEpW8dxBoKJQ9DCiS8wIHrT/F3qCdCKSXRAvp3bt39ErJAmyDBFl6SIQ3KKadCEgcJJdEHdwoZoiV7KDlhI2JpUMXNNvXF3qDNhHwYZrOu7zTvXt3TYoz5pNPPhHtCYCQjt0S8WyONhFgMLgKH0IhzkGT4uxBBD5A5ZPc13kDKwLe3L59W7zKHTwIrlDpSu0wSOJdfRkk2ZnCYUWAwZA1+3CCCBkRaNt09vjSg4Wdkxe4cqkVgQ9l0UIYsZTsoauUAcYHGCyJfoiCmvnPjRs3vBEAaMNcffApxMTeyYE5J+sJfBOBzhEolcDeiX6YFmh++PCh6B0kisHNSe1/zzvkWVJ3sC4FQqCxrvnWrVteeQFFSQp2//vvv5tm30IhRUkDcwbNPoVCipIWHECzegEldFQEStBYTxC9VpRg8VIEvpTw8gYTS9U8BCPveCcC3BttIEp9oJDiWwjtpSfQzbaUNHgpAu0dqg9c11evXkXv/MFLETAB4lOzV14g15K6E10cXuYEJG+6niB7/vjjDy+LDl56AkYrXV+cLbTc++gFwEsRMFqxpbySLXhYHydXvRQBcMPcQmolG9ik2Ue8FAGjlXqC7Hnx4oV6Ailwo9hPlUfOqjfIhu+++67srs7S8TYcQgj379+P3inVwiDC6kOfW1G8FQEwX+Bjr0tHc+fOHS/DIIfXIgCSOQ2JasP31Ydei4AbxwNHdPa4evCkvq8+9F4E7LKtvUTVgQdlNwafvQB4Hw6R0BHTakiUHjwouzGoCITDDaRcqiFReliXQXHBd4IQARNnPrYA15tQtuPxXgSO4qeTKJW5evWqisAXuJHXrl2L3ilJYMDwtWu0mGA8AX0vBw4cUG+QEEqjPjytMgnBiABvwMSZziBXhjXaZ8+ejd75TzAiAIRw8uRJXYhfAYoI5FChEJwImEH2tRsyCwgXz507F70Lg6BEAOoN4uE5XqFUhRxBikC9QWnwAjzA0ee26VIEJwJw3kBpD41yJMQheQEIVgR4Ay2XvofwkFwgNAFAkCIAbrb2E72H8JCnOaoIAkK9wXvwAkeOHInehUewIoBjx44Fv0kXgwAtJUwkhugFIGgR0Btz6tSpoMullEQZDEJpkShF0CJg5KMkSDwcYljEOR86dCi4kmgxQYsAEMLBgwejd+GA97t48WLQYZBDRfDWAFg9debMmegT/8EDsGrsxIkTQYdBjuBFABgCk0ShhETkAXg/FcA7VAQRIYUE5AGhLJhJgoogAhFQK/e9UsT5aR7QHhVBAeyx4/MEGud19+5dDYOKUBEUgHEcPnzY22cbuFxAaY+KoAQ+Ggph0NGjR6N3SiEqgiKIlVmUT/LoS36AV7t8+bIN9TQX+BAVQQkwFGLnzZs3m9evX4sOjTh2Ev7QWyPiUBHEgBikh0bkNyFsqlsLKoIYMBz6irZs2SLWG+zYsUM9QAVUBAlg2eHjx49FCYFjZTfu0JvjkqAiSAAeged2SYNj1jCoMiqChOAJJMHS0efPn0fvlDhUBB5Dd6x6gsqoCBLie09RyKgIEkByOXTo0OidHLp37x69UuJQESRk8ODBohblt7a2mn79+ml1KAHNepHioe9+2rRppkuXLtEncsB76f2tjIogBgQwevRoM336dHFbs3C8o0aNUm9QAa6NhkNlcAJYsmSJ2L2JOO6WlhbTq1cvFUIZrAh0pGiPW3a4ePFi8/nnn4sVgIMS6YoVK8ygQYN0SWUJuD5Nly5daqXLUPtL3jFp0iQzdepU23YgXQCFUOKlnZodJpR3MPiPGDHCNA8fPjz6KEy4ELRL9+zZ03zzzTc2/gefBABv3rwx48ePN19//bX1COr939178qamtze7df/+/ebBgwdBzS46Ixg7dqyZMGGCGTBggDWUEOA82YadXagh1FllbGDjxo2m6e0FaeUhbaytDSEk4sT5IhciaXSlT99G/koQ7tEdy70/f/68fY0YQhEENsAAuGDBAtP09uRb+XDr1q32m77ijJ+Kz+TJk4Ma+eNw7eFsw3LhwgUbEYDvYiAkXLVqlRk4cOA7EXAhSI4ZFXw7eQyfcxo3bpw1fmJ/ZlNDG/krgQ3Qeeoe38p27ay19lUM2MH69eutHbR5gmfPnpk9e/Z4ExJh/EClh4oPYY8afjKcd2BgvHXrlh01EYMvgsA2Zs6caQfFdiKghLZr1y7xT3V0Iz+Gr8ZfG04MCIG84cmTJ16IARuhEujOo00Eju3bt7eNohLgWPmi/Ddr1iw1+jriQiYGShbv82wHvISk6IHj3bBhg+natWv0SZEIOEn2rJewZbcz/iFDhtgMv0ePHmr8HQi24h78jZeQMHByjMycf/HFF+2KIu16hzAiEkgJ0A+zZs0ae0KoWgXQsXC9CSeIq7/99ls7wuYdRDBlyhRbGCnkg3DIgVLYaqTRHoGL60Z7Mnota8rAeQqe+9DoR8NiQ/SBMTtcarAs20XarVs3m1g20s1x4Th4N9qrAOTgPMWcOXNsf04jPUXfvn3LCgDKioB/wGKSRomAv0ufCwevxi8X7IiBjNl5RNHR9oT4KJjEEbuegErL7NmzO1zF/L0+ffrY1+XUq8iBQYyQltxh4sSJtmGxI0BwtMewwi7OjsrmBIV0VNmUv8EXwnMTGYpfMB/FQwPZ47XeNsVgWlwOLUXFlWUkOPPmzau7N+D3U/HhoFUA/oJX6N+/v/UK9fYIzB1ROq9ERRFgjDSd4VbqBQIgCaeXQ+v9/sP95WvZsmVt3r8ezJ07N5EtVRQBoF68Qda4kydpcgesAggHih5fffWVLcNnLQQGVqKYJCQSAeDCsjxQF/5wEUiatAIUHgx4eH6qgFmvgSZySTqgJhYBvzCriTNOlnht9erVNlHS0T9c3L1nLgibyEoIaSKXxCIAdmCo9SD598w/LFy4MPpEUd6F3HQFYBtZ2BiLppKSqERaTDUlUyoBixYtsut5NfRR4iA6OH36tG25SBt9YGfr1q1rm2dKQipPACQbS5cuTaVWflYFoCQFG2HXj7ThET87Y8aMVF4AUouAGI4kBreVBA6Mn1UBKGnAzgiPkm4Ox8/QI4R40tpZahGAU2qlg+P7zDGgThWAkhaEQPk8aUi0fPnyqoosVYkAkvwxSqDs5akCUKqF/rVKBRm+hwCSzA6XomoRQNzBkaDMnz8/eqco1cFgSwMc+UGpyAP7o9ugUpNcHDWJgFCnVEmLg3UJSrUHpigOIgnaoYsX5mBn5AysWagl2qiqRFoMB7Nz504rBl7jIdjdSwWgZAmVSVY7YmPYGstr6WSo1c5q8gQO4ra1a9dapZLEjBw5UgWg1AX2kSLUZqFOFgKATDwBoFK28uMB0hKf7KLIAAGw3UuWNpaZCMB17akAlHqCnWVpY5mKQFEkkklOoCiSUREowaMiUIJHRaAEj4pACR4VgRI4xvwPmYiAC4AtHyIAAAAASUVORK5CYII="></img> */}
                  <div className="overlay">
                    <div className="upload_photo">
                      <a
                        onClick={() => {
                          setIsOpen3(!isOpen3);
                        }}
                      >
                        <i className="fa fa-picture-o" aria-hidden="true" />
                      </a>
                    </div>
                  </div>
                </div>
                <div className="col-sm-3">
                  <div className="profile_head">
                    {props.candidateState.data.length > 0
                      ? props.candidateState.data[0]["firstName"]
                      : ""}
                  </div>
                  <div className="progress_bar_bg1">
                    <div
                      className="progress_bar1"
                      style={{
                        width: props.progressBar
                          ? `${props.progressBar["profileProgress"]}%`
                          : 0,
                      }}
                    />
                  </div>
                  <div className="profile_cons1">
                    {/* Please complete your profile to minimum 60% to access all the features of JIT Career. */}
                    Your profile is{" "}
                    {props.progressBar
                      ? props.progressBar["profileProgress"]
                      : 0}
                    % complete.
                  </div>
                </div>

                <div className="col-sm-3">
                  <div
                    className="profile_social_add"
                    style={{ textAlign: "center", paddingLeft: "6%" }}
                  >
                    <a
                      onClick={() => {
                        if (socialAccountList.length == 4) {
                          toast.info(
                            "You have added maximum number of social accounts."
                          );
                        } else {
                          setIsOpen2(!isOpen2);
                        }
                      }}
                      className="_cursor-pointer"
                    >
                      Add Social Networks
                    </a>
                  </div>

                  <div style={{ width: "100%", textAlign: "center" }}>
                    <div style={{ display: "inline-block", margin: "0 auto" }}>
                      {socialAccountList}
                    </div>
                  </div>
                </div>

                <div className="col-sm-3">
                  <div className="update_password _cursor-pointer">
                    <a
                      onClick={() => {
                        setIsOpen(!isOpen);
                      }}
                    >
                      <i
                        className="fa fa-pencil-square-o"
                        aria-hidden="true"
                        style={{ paddingRight: "10px" }}
                      />
                      Update Password
                    </a>
                  </div>
                  <div className="clearfix" />
                  <div className="make_video">
                    <a href="#" data-target="#makevideo" data-toggle="modal">
                      <i className="fa fa-video-camera" aria-hidden="true" />{" "}
                      Make Video
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-12">
              <div className="pfofile_tab_sec">
                <div
                  id="horizontalTab"
                  style={{ display: "block", width: "100%", margin: "0px" }}
                >
                  <ul className="resp-tabs-list profile-tabs">{_titles}</ul>
                  <div className="resp-tabs-container resp-tab-contents">
                    {tab}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="clearfix" />

        <Modal
          show={isOpen}
          onHide={() => {
            setIsOpen(!isOpen);
          }}
        >
          <form onSubmit={handleSubmit(onSubmit)} noValidate>
            <Modal.Header closeButton>
              <Modal.Title>Update your account password</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <div className="col-sm-12">
                <div className="form-group">
                  {/* <label htmlFor="email">Current Password</label> */}
                  <label className={"required"}> Current Password</label>
                  <input
                    type={showPassword ? "text" : "password"}
                    className="form-control"
                    placeholder="Current password"
                    name="currentPassword"
                    ref={register({
                      required: "Password is required",
                      minLength: {
                        value: 9,
                        message:
                          "Password should be 9 digits long with one uppercase and one lowercase letter, a number and a special character",
                      },
                      maxLength: {
                        value: 20,
                        message: "Should not be greater than 20 characters",
                      },
                      validate: function (value) {
                        let re = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{9,}$/;
                        return re.test(value)
                          ? undefined
                          : "Password should be 9 digit long which inludes capital letter, small letter and special characters";
                      },
                    })}
                  />
                  <span
                    id="currentPassword"
                    onClick={(event: any) => {
                      setShowPassword(!showPassword);
                    }}
                    className={
                      showPassword
                        ? "fa fa-fw fa-eye field-icon toggle-password"
                        : "fa fa-fw fa-eye-slash field-icon toggle-password"
                    }
                    style={{ top: "18%" }}
                  />
                  <ErrorMessage
                    errors={errors}
                    name="currentPassword"
                    render={({ message }) => (
                      <div className="register_validation">{message}</div>
                    )}
                  />
                </div>
                <div className="form-group">
                  {/* <label htmlFor="email">New password</label> */}
                  <label className={"required"}> New password</label>
                  <input
                    type={showPassword2 ? "text" : "password"}
                    className="form-control"
                    placeholder="New password"
                    name="newPassword"
                    ref={register({
                      required: "Password is required",
                      minLength: {
                        value: 9,
                        message:
                          "Password should be 9 digits long with one uppercase and one lowercase letter, a number and a special character",
                      },
                      maxLength: {
                        value: 20,
                        message: "Should not be greater than 20 characters",
                      },
                      validate: function (value) {
                        let re = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{9,}$/;
                        return re.test(value)
                          ? undefined
                          : "Password should be 9 digit long which inludes capital letter, small letter and special characters";
                      },
                    })}
                  />
                  <span
                    id="newPassword"
                    onClick={(event: any) => {
                      setShowPassword2(!showPassword2);
                    }}
                    className={
                      showPassword2
                        ? "fa fa-fw fa-eye field-icon toggle-password"
                        : "fa fa-fw fa-eye-slash field-icon toggle-password"
                    }
                    style={{ top: "56%" }}
                  />
                  <ErrorMessage
                    errors={errors}
                    name="newPassword"
                    render={({ message }) => (
                      <div className="register_validation">{message}</div>
                    )}
                  />
                </div>
                <div className="profile_cons1">
                  Note: Your Password should be 9 digits long with one uppercase
                  and one lowercase letter, a number and a special character.
                </div>
              </div>
            </Modal.Body>
            <div className="modal-footer  m-t-30">
              <button
                className="btn btn-success save-event waves-effect waves-light"
                type="submit"
              >
                Save
              </button>
              <button
                onClick={() => {
                  setIsOpen(!isOpen);
                }}
                data-dismiss="modal"
                className="btn btn-default waves-effect"
                type="button"
              >
                Cancel
              </button>
            </div>
            <div className="clearfix" />
          </form>
        </Modal>

        <Modal
          show={isOpen2}
          onHide={() => {
            setIsOpen2(!isOpen2);
          }}
        >
          <AddSocial
            setIsOpen2={setIsOpen2}
            isOpen2={isOpen2}
            socialAccounts={props.socialAccounts}
            getSocialAccounts={props.getSocialAccounts}
            candidateId={props.candidateId}
          />
        </Modal>

        <Modal
          show={isOpen3}
          onHide={() => {
            setIsOpen3(!isOpen3);
          }}
        >
          <AddProfileImage
            setIsOpen3={setIsOpen3}
            isOpen3={isOpen3}
            candidateProfileImage={props.candidateProfileImage}
            getCandidateProfileImage={props.getCandidateProfileImage}
            candidateId={props.candidateId}
          />
        </Modal>
      </div>
    </React.Fragment>
  );
};
export default CandidateProfileComponent;
