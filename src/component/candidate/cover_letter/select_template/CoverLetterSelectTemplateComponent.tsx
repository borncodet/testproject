import React from "react";

interface ICoverLetterSelectTemplateComponentProps { }

interface ICoverLetterSelectTemplateComponentState { }

const initialState = {};

const CoverLetterSelectTemplateComponent: React.FC<ICoverLetterSelectTemplateComponentProps> = (props) => {
  const [coverLetterSelectTemplateComponentState, setCoverLetterSelectTemplateComponentState] = React.useState<ICoverLetterSelectTemplateComponentState>(
    initialState
  );

  return (
    <>
       <div className="content-wrapper">
          <div className="container-fluid">    
            <h1 className="heading">Select your Cover Letter</h1>
            <div className="clearfix" />
            <div className="row">
              <div className="col-sm-3">
                <div><img src={require("./../../../../images/cover_letter.jpg")} className="img-responsive" /></div>
                <div><img src={require("./../../../../images/shadow.jpg")} className="img-responsive" /></div>
              </div>
              <div className="col-sm-9">
                <div className="section_box4">
                  <div className="select_templates">
                    <div className="row">
                      <div className="col-sm-3"><a href="#"><img src={require("./../../../../images/cover_letter.jpg")} className=" img-responsive" /></a></div>
                      <div className="col-sm-3"><a href="#"><img src={require("./../../../../images/cover_letter.jpg")} className=" img-responsive" /></a></div>
                      <div className="col-sm-3"><a href="#"><img src={require("./../../../../images/cover_letter.jpg")} className=" img-responsive" /></a></div>
                      <div className="col-sm-3"><a href="#"><img src={require("./../../../../images/cover_letter.jpg")} className=" img-responsive" /></a></div>
                      <div className="clearfix" />
                      <div className="col-sm-3"><a href="#"><img src={require("./../../../../images/cover_letter.jpg")} className=" img-responsive" /></a></div>
                      <div className="col-sm-3"><a href="#"><img src={require("./../../../../images/cover_letter.jpg")} className=" img-responsive" /></a></div> 
                      <div className="col-sm-3"><a href="#"><img src={require("./../../../../images/cover_letter.jpg")} className=" img-responsive" /></a></div>
                      <div className="col-sm-3"><a href="#"><img src={require("./../../../../images/cover_letter.jpg")} className=" img-responsive" /></a></div> 
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-sm-12"><a href="resume_builder_preview.html" className="btn continue_but">Preview</a></div>
            </div>  
          </div>
          <div className="clearfix" />                      
        </div> 
    </>
  );
};
export default CoverLetterSelectTemplateComponent;
