import React from "react";
import CoverLetterEditTemplateComponent from "./CoverLetterEditTemplateComponent";

interface ICoverLetterEditTemplateContainerProps { }

interface ICoverLetterEditTemplateContainerState { }

const initialState = {};

const CoverLetterEditTemplateContainer: React.FC<ICoverLetterEditTemplateContainerProps> = (props) => {
  const [coverLetterEditTemplateContainerState, setCoverLetterEditTemplateContainerState] = React.useState<ICoverLetterEditTemplateContainerState>(
    initialState
  );

  return (
    <>
      <CoverLetterEditTemplateComponent/>
    </>
  );
};
export default CoverLetterEditTemplateContainer;
