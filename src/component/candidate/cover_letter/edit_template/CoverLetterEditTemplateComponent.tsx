import React from "react";

import { BrowserRouter } from "react-router-dom";

interface ICoverLetterEditTemplateComponentProps { }

interface ICoverLetterEditTemplateComponentState { }

const initialState = {};

const CoverLetterEditTemplateComponent: React.FC<ICoverLetterEditTemplateComponentProps> = (props) => {
  const [CoverLetterEditTemplateComponentState, setCoverLetterEditTemplateComponentState] = React.useState<ICoverLetterEditTemplateComponentState>(
    initialState
  );

  return (
    <>
       <div className="content-wrapper">
        <div className="container-fluid">    
          <h1 className="heading">Cover Letter</h1>
          <div className="profile_edit"><a href="#"><i className="fa fa-pencil" aria-hidden="true" /></a></div>
          <div className="clearfix" /> 
          <div className="section_box4">
            <form className="personal_details">
              <div className="row">
                <div className="col-sm-4">
                  <div className="form-group">
                    <label> Name</label> 
                    <input type="email" className="form-control" placeholder="Type here" /> 
                  </div>
                </div>
                <div className="col-sm-4">
                  <div className="form-group">
                    <label> Date</label> 
                    <input name="" type="text" className="text-wrapper select-calander" id="datepicker" /> 
                  </div>
                </div>
                <div className="col-sm-4">
                  <div className="form-group">
                    <label> mobile</label> 
                    <input type="email" className="form-control" placeholder="Type here" /> 
                  </div>
                </div>
                <div className="col-sm-12">
                  <div className="form-group">
                    <label> Description</label> 
                    <textarea 
                    // name cols rows 
                    className="form-control form-contro11" placeholder="Type here" defaultValue={""} /> 
                  </div>
                </div>
              </div>
              <div className="clearfix" />
              <a href="#" className="btn submit_btn">Save</a>
            </form> 
          </div>
        </div> 
        <div className="clearfix" />                      
      </div>
    </>
  );
};
export default CoverLetterEditTemplateComponent;
