import React from "react";
import CoverLetterPreviewComponent from "./CoverLetterPreviewComponent";


interface ICoverLetterPreviewContainerProps { }

interface ICoverLetterPreviewContainerState { }

const initialState = {};

const CoverLetterPreviewContainer: React.FC<ICoverLetterPreviewContainerProps> = (props) => {
  const [coverLetterPreviewContainerState, setCoverLetterPreviewContainerState] = React.useState<ICoverLetterPreviewContainerState>(
    initialState
  );

  return (
    <>
      <CoverLetterPreviewComponent/>
    </>
  );
};
export default CoverLetterPreviewContainer;
