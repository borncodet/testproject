import React from "react";


interface ICoverLetterPreviewComponentProps { }

interface ICoverLetterPreviewComponentState { }

const initialState = {};

const CoverLetterPreviewComponent: React.FC<ICoverLetterPreviewComponentProps> = (props) => {
  const [coverLetterPreviewComponentState, setCoverLetterPreviewComponentState] = React.useState<ICoverLetterPreviewComponentState>(
    initialState
  );

  return (
    <>
      <div className="content-wrapper">
        <div className="container-fluid">    
          <a href="#" className="btn cutomise_but"> Save</a>
          <a href="#" className="btn cutomise_but">Preview Cover Letter</a>
          <div className="clearfix" />
          <div style={{padding: '0px 30px 0px 30px'}}>
            <table width="100%" 
            // border={0}
             cellSpacing={0} cellPadding={0} style={{background: '#fff'}}>
              <tbody><tr>
                  <td valign="top" style={{background: '#4b56c0', height: '10px'}}>
                  </td>
                </tr>
                <tr>
                  <td valign="top" style={{fontSize: '14px', textAlign: 'left', color: '#222222', padding: '30px', fontWeight: 500}}>
                    <table width="100%"
                    //  border={0} 
                     cellSpacing={0} cellPadding={0}>
                      <tbody><tr>
                          <td style={{padding: '0px 0px 30px 0px'}}>
                            <table width="100%" 
                            // border={0} 
                            cellSpacing={0} cellPadding={0}>
                              <tbody><tr>
                                  <td width="30%" align="center" valign="bottom" style={{fontSize: '20px', textAlign: 'center', background: '#f9f9f9', color: '#222222', padding: '25px 0px 25px 0px', fontWeight: 500}}>
                                    <img 
                                     src={require("./../../../../images/cover_letter_photo.png")}
                                    // src="images/cover_letter_photo.png"
                                     width={166} height={166} />
                                    </td>
                                  <td width="70%" valign="top" style={{background: '#f9f9f9', padding: '50px 15px 0px 0px'}}>
                                    <table width="100%" 
                                    // border={0} 
                                    cellSpacing={0} cellPadding={0}>
                                      <tbody><tr>
                                          <td style={{fontSize: '30px', textAlign: 'left', color: '#222222', padding: '0px 15px 0px 0px', fontWeight: 600}}>John Doe</td>
                                        </tr>
                                        <tr>
                                          <td valign="top" style={{fontSize: '20px', textAlign: 'left', color: '#222222', padding: '7px 15px 7px 0px', fontWeight: 400}}>UI Designer</td>
                                        </tr>
                                      </tbody></table>
                                  </td>
                                </tr>
                                <tr>
                                </tr>
                              </tbody></table>
                          </td>
                        </tr>
                        <tr>
                          <td style={{padding: '0px 0px 0px 0px'}}>
                            <table width="100%" 
                            // border={0}
                             cellSpacing={0} cellPadding={0}>
                              <tbody><tr>
                                  <td valign="top" style={{fontSize: '25px', textAlign: 'left', background: '#f9f9f9', color: '#222222', padding: '7px 15px 7px 15px', fontWeight: 500}}>Cover Letter</td>
                                </tr>
                                <tr>
                                  <td style={{fontSize: '14px', textAlign: 'left', color: '#222222', padding: '25px 0px 15px 0px', fontWeight: 500, lineHeight: '20px'}}>Date : 2020-09-30 </td>
                                </tr>
                                <tr>
                                  <td style={{fontSize: '14px', textAlign: 'left', color: '#222222', padding: '10px 0px 15px 0px', fontWeight: 500, lineHeight: '20px'}}>Address</td>
                                </tr>
                                <tr>
                                  <td style={{fontSize: '14px', textAlign: 'left', color: '#222222', padding: '10px 0px 15px 0px', fontWeight: 600, lineHeight: '20px'}}>Dear mr./Ms./Mrs, Manager</td>
                                </tr>
                                <tr>
                                  <td style={{fontSize: '14px', textAlign: 'left', color: '#222222', padding: '0px 0px 00px 0px', fontWeight: 500, lineHeight: '26px'}}>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like). It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                                  </td>
                                </tr>
                                <tr>
                                  <td style={{fontSize: '14px', textAlign: 'left', color: '#222222', padding: '30px 0px 15px 0px', fontWeight: 500, lineHeight: '20px'}}>Sincerly,</td>
                                </tr>
                                <tr>
                                  <td style={{fontSize: '14px', textAlign: 'left', color: '#222222', padding: '10px 0px 0px 0px', fontWeight: 600, lineHeight: '20px'}}>Name</td>
                                </tr>
                                <tr>
                                  <td style={{fontSize: '14px', textAlign: 'left', color: '#222222', padding: '10px 0px 0px 0px', fontWeight: 600, lineHeight: '20px'}}>Address</td>
                                </tr>
                                <tr>
                                  <td style={{fontSize: '14px', textAlign: 'left', color: '#222222', padding: '10px 0px 0px 0px', fontWeight: 600, lineHeight: '20px'}}>9744572977</td>
                                </tr>
                                <tr>
                                  <td style={{fontSize: '14px', textAlign: 'left', color: '#222222', padding: '10px 0px 15px 0px', fontWeight: 600, lineHeight: '20px'}}>Info@johndoe@gmail.com</td>
                                </tr>
                              </tbody></table> 
                          </td>
                        </tr>
                      </tbody></table>
                  </td> 
                </tr>
              </tbody></table>
          </div>  
        </div>
        <div className="clearfix" />                      
      </div>
    </>
  );
};
export default CoverLetterPreviewComponent;
