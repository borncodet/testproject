import React, { useState } from "react";
import { Link, Redirect, Route, Switch, useHistory, useRouteMatch } from "react-router-dom";
import CoverLetterEditTemplateContainer from "../edit_template/CoverLetterEditTemplateContainer";
import MyCoverLetterContainer from "../my_cover_letter/MyCoverLetterContainer";
import CoverLetterPreviewContainer from "../preview/CoverLetterPreviewContainer";
import CoverLetterSelectTemplateContainer from "../select_template/CoverLetterSelectTemplateContainer";

interface ICoverLetterLayoutComponentProps { }

interface ICoverLetterLayoutComponentState { }

const initialState = {};

const CoverLetterLayoutComponent: React.FC<ICoverLetterLayoutComponentProps> = (props) => {
  const [coverLetterLayoutComponentState, setCoverLetterLayoutComponentState] = React.useState<ICoverLetterLayoutComponentState>(
    initialState
  );

  const [active, setActive] = useState("Select template");
  const { path, url } = useRouteMatch();
  let history = useHistory();
 

  const titles = [
    
    "Select template",
    "Edit template",
    "Preview Cover Letter",
    // "resume",
   "My Cover letter"
    
  ];

  const _titles = titles.map((e, i) => {
    var p=e.split(" ");
    return (
      <>
      <li key={i} onClick={() => { setActive(e); }}>
          <Link to={`${url}/${ p[1]==undefined?p[0]:p[0]+"_"+p[1]}`}
          className={e == active ? "active" : ""}
           > <i className={`fa fa-file-text${i==1?'':'-o'}`} aria-hidden="true" />
            <div className="menus">{e} </div>   </Link> 
       </li>
      </>
    )
    
  });

  const back=()=>{
    history.replace('/candidate/my-resume');
  }

  return (
    <>
       <div id="wrapper">
        {/*======Left Menu Start=======*/}
        <div className="icon-mobile-nav"><a href="#"><i className="fa fa-2x fa-reorder" /></a></div>
        <div className="menu_main">
          <div className="left-menu">
            <div className="dash_logo"><img 
            src={require("./../../../../images/logo_dash.png")}
            // src="images/logo_dash.png" 
            className="img-responsive center-block" /></div>
            <a href="#" className="btn resume_build_btn">Cover Letter</a>
            <aside className="bg-black dk nav_side-xs aside hidden-print" id="nav">
              <section className="vbox"> 
                <section className="w-f-md scrollable">
                  <div>
                    <div data-height="auto" data-disable-fade-out="true" data-distance={0} data-size="10px" data-railopacity="0.2">
                      {/* nav */}
                      <div id="divexample1" style={{overflow: 'hidden'}} tabIndex={0}>
                        <nav className="nav_side-primary nav_pad">
                          <ul className="nav_side" data-ride="collapse">  
                            {/* <li>
                              <a href="#" className="active">  <i className="fa fa-tachometer" aria-hidden="true" />
                                <div className="menus">Select Template</div></a> 
                            </li>  
                            <li>
                              <a href="#"> <i className="fa fa-file-text-o" aria-hidden="true" />
                                <div className="menus">Edit Template </div>   </a> 
                            </li>  
                            <li>
                              <a href="#"> <i className="fa fa-file-text" aria-hidden="true" />
                                <div className="menus">Preview Cover Letter</div> </a>  
                            </li>  
                            <li>
                              <a href="#"> <i className="fa fa-files-o" aria-hidden="true" />
                                <div className="menus">My Cover letter</div> </a>
                            </li>  */}
                            {_titles}
                            <li>
                            <Link to='#' onClick={back}><i className="fa fa-th-large" aria-hidden="true" />
                                <div className="menus">Dashboard</div></Link>
                            </li> 
                          </ul>
                          <ul className="nav_side nav_side1" data-ride="collapse"> 
                            <li>
                              <a href="#"> <i className="fa fa-question-circle" aria-hidden="true" />
                                <div className="menus">Help</div> </a>
                            </li>
                            <li>
                              <a href="#"> <i className="fa fa-power-off" aria-hidden="true" />
                                <div className="menus">Log Out </div>   </a> 
                            </li>
                          </ul>
                        </nav>
                        {/* / nav */}
                      </div>
                    </div>
                  </div>
                </section>
              </section>
            </aside>
          </div>
        </div>
        {/*======Left Menu End=======*/}
        {/*=================Content Section Start================*/}
        <Switch>
        <Route exact path={`${path}`}>
        {/* <ResumeMyInformationContainer/>  */}
        <Redirect to={`${path}/Select_template`}/>

        </Route>
        <Route exact path={`${path}/Select_template`}>
        <CoverLetterSelectTemplateContainer/> 
        </Route>
        <Route exact path={`${path}/Edit_template`}>
        <CoverLetterEditTemplateContainer/>
        </Route>
        <Route exact path={`${path}/Preview_Cover`}>
        <CoverLetterPreviewContainer/>
        </Route>
        <Route exact path={`${path}/My_Cover`}>
        <MyCoverLetterContainer/>
        </Route>
        
        </Switch>
       
        {/* <CoverLetterEditTemplateContainer/> */}
        {/* <CoverLetterPreviewContainer/> */}
        {/* <MyCoverLetterContainer/> */}
        {/*=================Content Section End================*/}
      </div>
    </>
  );
};
export default CoverLetterLayoutComponent;
