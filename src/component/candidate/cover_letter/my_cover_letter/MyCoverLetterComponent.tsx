import React from "react";


interface IMyCoverLetterComponentProps { }

interface IMyCoverLetterComponentState { }

const initialState = {};

const MyCoverLetterComponent: React.FC<IMyCoverLetterComponentProps> = (props) => {
  const [myCoverLetterComponentState, setMyCoverLetterComponentState] = React.useState<IMyCoverLetterComponentState>(
    initialState
  );

  return (
    <>
        <div className="content-wrapper">
        <div className="container-fluid">    
          {/* <h1 className="heading">Cover Letter</h1> */}
          <div className="clearfix" />
          <div className="row">
            <div className="col-sm-12 col-lg-12"> 
              <div className="heading_sec2">
                <h1>Cover Letter</h1>
                <div><a href="#" className="btn resume_build_but"> Build your cover letter with  us</a></div>
              </div> 
              <div className="row">
                <div className="prodict_list"> 
                  <div id="owl-demo2" className="owl-carousel owl-theme" style={{opacity: 1, display: 'block'}}> 
                    <div className="owl-wrapper-outer"><div className="owl-wrapper" style={{width: '2568px', left: '0px', display: 'block', transition: 'all 0ms ease 0s', transform: 'translate3d(0px, 0px, 0px)'}}><div className="owl-item" style={{width: '321px'}}><div className="item"> 
                            <div className="matched_jobs_sec">
                              <div className="digis_expairs">Created on - 16- 09-2020</div> 
                              <div className="matched_jobs">  
                                <img 
                                 src={require("./../../../../images/cover_letter.jpg")}
                                // src="images/cover_letter.jpg"
                                 alt="" className="image2" />
                                <div className="overlay">
                                  <div className="myresume_edits1 ">
                                    <a href="#" className="myresume_clr1"><i className="fa fa-download" aria-hidden="true" /></a>                                         
                                    <a href="#" className="myresume_clr2"><i className="fa fa-share-alt" aria-hidden="true" /></a>                                        
                                    <a href="#" className="myresume_clr3"><i className="fa fa-pencil" aria-hidden="true" /></a>                                         
                                    <a href="#" className="myresume_clr4"><i className="fa fa-trash-o" aria-hidden="true" /></a>
                                  </div> 
                                </div> 
                              </div>
                              <div className="digis_documents"><i className="fa fa-file-text-o" aria-hidden="true" /> Cover Letter 1</div> 
                              <div className="action_btn1">  
                                <button type="button" className="actions_bt4"><i className="fa fa-ellipsis-h mdi-toggle-switch-off" aria-hidden="true" /></button>
                                <div className="dash_action4" tabIndex={-1}>
                                  <div className="action_sec">
                                    <ul>
                                      <li><a href="#">Dummy</a> </li>
                                      <li><a href="#"> ummy</a></li>
                                      <li><a href="#">Dummy</a></li>
                                      <li><a href="#">Dummy</a></li> 
                                    </ul>  
                                  </div> 
                                </div>
                              </div>
                            </div>
                          </div></div><div className="owl-item" style={{width: '321px'}}><div className="item"> 
                            <div className="matched_jobs_sec">
                              <div className="digis_expairs">Created on - 16- 09-2020</div> 
                              <div className="matched_jobs">  
                                <img src={require("./../../../../images/cover_letter.jpg")} alt="" className="image2" />
                                <div className="overlay">
                                  <div className="myresume_edits1 ">
                                    <a href="#" className="myresume_clr1"><i className="fa fa-download" aria-hidden="true" /></a>                                         
                                    <a href="#" className="myresume_clr2"><i className="fa fa-share-alt" aria-hidden="true" /></a>                                        
                                    <a href="#" className="myresume_clr3"><i className="fa fa-pencil" aria-hidden="true" /></a>                                         
                                    <a href="#" className="myresume_clr4"><i className="fa fa-trash-o" aria-hidden="true" /></a>
                                  </div> 
                                </div> 
                              </div>
                              <div className="digis_documents"><i className="fa fa-file-text-o" aria-hidden="true" /> Cover Letter 1</div> 
                              <div className="action_btn1">  
                                <button type="button" className="actions_bt4"><i className="fa fa-ellipsis-h mdi-toggle-switch-off" aria-hidden="true" /></button>
                                <div className="dash_action4" tabIndex={-1}>
                                  <div className="action_sec">
                                    <ul>
                                      <li><a href="#">Dummy</a> </li>
                                      <li><a href="#"> ummy</a></li>
                                      <li><a href="#">Dummy</a></li>
                                      <li><a href="#">Dummy</a></li> 
                                    </ul>  
                                  </div> 
                                </div>
                              </div>
                            </div>
                          </div></div><div className="owl-item" style={{width: '321px'}}><div className="item"> 
                            <div className="matched_jobs_sec">
                              <div className="digis_expairs">Created on - 16- 09-2020</div> 
                              <div className="matched_jobs">  
                                <img src={require("./../../../../images/cover_letter.jpg")} alt="" className="image2" />
                                <div className="overlay">
                                  <div className="myresume_edits1 ">
                                    <a href="#" className="myresume_clr1"><i className="fa fa-download" aria-hidden="true" /></a>                                         
                                    <a href="#" className="myresume_clr2"><i className="fa fa-share-alt" aria-hidden="true" /></a>                                        
                                    <a href="#" className="myresume_clr3"><i className="fa fa-pencil" aria-hidden="true" /></a>                                         
                                    <a href="#" className="myresume_clr4"><i className="fa fa-trash-o" aria-hidden="true" /></a>
                                  </div> 
                                </div> 
                              </div>
                              <div className="digis_documents"><i className="fa fa-file-text-o" aria-hidden="true" /> Cover Letter 2</div> 
                              <div className="action_btn1">  
                                <button type="button" className="actions_bt5"><i className="fa fa-ellipsis-h mdi-toggle-switch-off" aria-hidden="true" /></button>
                                <div className="dash_action5" tabIndex={-1}>
                                  <div className="action_sec">
                                    <ul>
                                      <li><a href="#">Dummy</a> </li>
                                      <li><a href="#"> ummy</a></li>
                                      <li><a href="#">Dummy</a></li>
                                      <li><a href="#">Dummy</a></li> 
                                    </ul>  
                                  </div> 
                                </div>
                              </div>
                            </div>
                          </div></div><div className="owl-item" style={{width: '321px'}}><div className="item"> 
                            <div className="matched_jobs_sec">
                              <div className="digis_expairs">Created on - 16- 09-2020</div> 
                              <div className="matched_jobs">  
                                <img src={require("./../../../../images/cover_letter.jpg")} alt="" className="image2" />
                                <div className="overlay">
                                  <div className="myresume_edits1 ">
                                    <a href="#" className="myresume_clr1"><i className="fa fa-download" aria-hidden="true" /></a>                                         
                                    <a href="#" className="myresume_clr2"><i className="fa fa-share-alt" aria-hidden="true" /></a>                                        
                                    <a href="#" className="myresume_clr3"><i className="fa fa-pencil" aria-hidden="true" /></a>                                         
                                    <a href="#" className="myresume_clr4"><i className="fa fa-trash-o" aria-hidden="true" /></a>
                                  </div> 
                                </div> 
                              </div>
                              <div className="digis_documents"><i className="fa fa-file-text-o" aria-hidden="true" /> Cover Letter 3</div> 
                              <div className="action_btn1">  
                                <button type="button" className="actions_bt6"><i className="fa fa-ellipsis-h mdi-toggle-switch-off" aria-hidden="true" /></button>
                                <div className="dash_action6" tabIndex={-1}>
                                  <div className="action_sec">
                                    <ul>
                                      <li><a href="#">Dummy</a> </li>
                                      <li><a href="#"> ummy</a></li>
                                      <li><a href="#">Dummy</a></li>
                                      <li><a href="#">Dummy</a></li> 
                                    </ul>  
                                  </div> 
                                </div>
                              </div>
                            </div>
                          </div></div></div></div> 
                    <div className="owl-controls clickable" style={{display: 'none'}}><div className="owl-pagination"><div className="owl-page active"><span className="" /></div></div><div className="owl-buttons">
                    <div className="owl-prev"><img src={require("./../../../../images/products_ar1.png")}/> </div>
                  <div className="owl-next"> <img src={require("./../../../../images/products_ar2.png")}/> </div>
                      </div></div></div>
                </div>
              </div> 
              <div className="m-t-25" />
              <div className="heading_sec2">
                <h1>Resume Builder </h1>
                <div><a href="#" className="btn resume_build_but">Build your resume with us</a></div>
              </div> 
              <div className="row">
                <div className="prodict_list"> 
                  <div id="owl-demo1" className="owl-carousel owl-theme" style={{opacity: 1, display: 'block'}}> 
                    <div className="owl-wrapper-outer"><div className="owl-wrapper" style={{width: '2568px', left: '0px', display: 'block', transition: 'all 0ms ease 0s', transform: 'translate3d(0px, 0px, 0px)'}}><div className="owl-item" style={{width: '321px'}}><div className="item"> 
                            <div className="matched_jobs_sec">
                              <div className="digis_expairs">Created on - 16- 09-2020</div> 
                              <div className="matched_jobs">  
                                <img src={require("./../../../../images/reume_img.png")} alt="" className="image2" />
                                <div className="overlay">
                                  <div className="myresume_edits1 ">
                                    <a href="#" className="myresume_clr1"><i className="fa fa-download" aria-hidden="true" /></a>                                         
                                    <a href="#" className="myresume_clr2"><i className="fa fa-share-alt" aria-hidden="true" /></a>                                        
                                    <a href="#" className="myresume_clr3"><i className="fa fa-pencil" aria-hidden="true" /></a>                                         
                                    <a href="#" className="myresume_clr4"><i className="fa fa-trash-o" aria-hidden="true" /></a>
                                  </div> 
                                </div> 
                              </div>
                              <div className="digis_documents"><i className="fa fa-file-text-o" aria-hidden="true" /> Resume 1</div> 
                              <div className="action_btn1">  
                                <button type="button" className="actions_bt"><i className="fa fa-ellipsis-h mdi-toggle-switch-off" aria-hidden="true" /></button>
                                <div className="dash_action" tabIndex={-1}>
                                  <div className="action_sec">
                                    <ul>
                                      <li><a href="#">Dummy</a> </li>
                                      <li><a href="#"> ummy</a></li>
                                      <li><a href="#">Dummy</a></li>
                                      <li><a href="#">Dummy</a></li> 
                                    </ul>  
                                  </div> 
                                </div>
                              </div>
                            </div>
                          </div></div><div className="owl-item" style={{width: '321px'}}><div className="item"> 
                            <div className="matched_jobs_sec">
                              <div className="digis_expairs">Created on - 16- 09-2020</div> 
                              <div className="matched_jobs">  
                                <img src={require("./../../../../images/reume_img.png")} alt="" className="image2" />
                                <div className="overlay">
                                  <div className="myresume_edits1 ">
                                    <a href="#" className="myresume_clr1"><i className="fa fa-download" aria-hidden="true" /></a>                                         
                                    <a href="#" className="myresume_clr2"><i className="fa fa-share-alt" aria-hidden="true" /></a>                                        
                                    <a href="#" className="myresume_clr3"><i className="fa fa-pencil" aria-hidden="true" /></a>                                         
                                    <a href="#" className="myresume_clr4"><i className="fa fa-trash-o" aria-hidden="true" /></a>
                                  </div> 
                                </div> 
                              </div>
                              <div className="digis_documents"><i className="fa fa-file-text-o" aria-hidden="true" /> Resume 2</div> 
                              <div className="action_btn1">  
                                <button type="button" className="actions_bt1"><i className="fa fa-ellipsis-h mdi-toggle-switch-off" aria-hidden="true" /></button>
                                <div className="dash_action1" tabIndex={-1}>
                                  <div className="action_sec">
                                    <ul>
                                      <li><a href="#">Dummy</a> </li>
                                      <li><a href="#"> ummy</a></li>
                                      <li><a href="#">Dummy</a></li>
                                      <li><a href="#">Dummy</a></li> 
                                    </ul>  
                                  </div> 
                                </div>
                              </div>
                            </div>
                          </div></div><div className="owl-item" style={{width: '321px'}}><div className="item"> 
                            <div className="matched_jobs_sec">
                              <div className="digis_expairs">Created on - 16- 09-2020</div> 
                              <div className="matched_jobs">  
                                <img src={require("./../../../../images/reume_img.png")} alt="" className="image2" />
                                <div className="overlay">
                                  <div className="myresume_edits1 ">
                                    <a href="#" className="myresume_clr1"><i className="fa fa-download" aria-hidden="true" /></a>                                         
                                    <a href="#" className="myresume_clr2"><i className="fa fa-share-alt" aria-hidden="true" /></a>                                        
                                    <a href="#" className="myresume_clr3"><i className="fa fa-pencil" aria-hidden="true" /></a>                                         
                                    <a href="#" className="myresume_clr4"><i className="fa fa-trash-o" aria-hidden="true" /></a>
                                  </div> 
                                </div> 
                              </div>
                              <div className="digis_documents"><i className="fa fa-file-text-o" aria-hidden="true" /> Resume 3</div> 
                              <div className="action_btn1">  
                                <button type="button" className="actions_bt2"><i className="fa fa-ellipsis-h mdi-toggle-switch-off" aria-hidden="true" /></button>
                                <div className="dash_action2" tabIndex={-1}>
                                  <div className="action_sec">
                                    <ul>
                                      <li><a href="#">Dummy</a> </li>
                                      <li><a href="#"> ummy</a></li>
                                      <li><a href="#">Dummy</a></li>
                                      <li><a href="#">Dummy</a></li> 
                                    </ul>  
                                  </div> 
                                </div>
                              </div>
                            </div>
                          </div></div><div className="owl-item" style={{width: '321px'}}><div className="item"> 
                            <div className="matched_jobs_sec">
                              <div className="digis_expairs">Created on - 16- 09-2020</div> 
                              <div className="matched_jobs">  
                                <img src={require("./../../../../images/reume_img.png")} alt="" className="image2" />
                                <div className="overlay">
                                  <div className="myresume_edits1 ">
                                    <a href="#" className="myresume_clr1"><i className="fa fa-download" aria-hidden="true" /></a>                                         
                                    <a href="#" className="myresume_clr2"><i className="fa fa-share-alt" aria-hidden="true" /></a>                                        
                                    <a href="#" className="myresume_clr3"><i className="fa fa-pencil" aria-hidden="true" /></a>                                         
                                    <a href="#" className="myresume_clr4"><i className="fa fa-trash-o" aria-hidden="true" /></a>
                                  </div> 
                                </div> 
                              </div>
                              <div className="digis_documents"><i className="fa fa-file-text-o" aria-hidden="true" /> Resume 4</div> 
                              <div className="action_btn1">  
                                <button type="button" className="actions_bt3"><i className="fa fa-ellipsis-h mdi-toggle-switch-off" aria-hidden="true" /></button>
                                <div className="dash_action3" tabIndex={-1}>
                                  <div className="action_sec">
                                    <ul>
                                      <li><a href="#">Dummy</a> </li>
                                      <li><a href="#"> ummy</a></li>
                                      <li><a href="#">Dummy</a></li>
                                      <li><a href="#">Dummy</a></li> 
                                    </ul>  
                                  </div> 
                                </div>
                              </div>
                            </div>
                          </div></div></div></div> 
                    <div className="owl-controls clickable" style={{display: 'none'}}><div className="owl-pagination"><div className="owl-page active"><span className="" /></div></div><div className="owl-buttons">
                    <div className="owl-prev"><img src={require("./../../../../images/products_ar1.png")}/> </div>
                  <div className="owl-next"> <img src={require("./../../../../images/products_ar2.png")}/> </div>

                         </div>
                         </div>
                         </div>
                </div>
              </div>
            </div>
          </div> 
        </div>
        <div className="clearfix" />                      
      </div>
    </>
  );
};
export default MyCoverLetterComponent;
