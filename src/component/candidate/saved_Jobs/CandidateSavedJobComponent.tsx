import React, { useState, useEffect } from "react";
import { Modal } from "react-bootstrap";
import Scrollbars from "react-custom-scrollbars";
import { useForm } from "react-hook-form";
import { useHistory } from "react-router-dom";
import {
  FacebookShareButton,
  LinkedinShareButton,
  TwitterShareButton,
} from "react-share";
import {
  deleteJobBookMarkList,
  getJobBookMarkList,
  useJobBookMarkContext,
  useJobBookMarkDispatcher,
} from "../../../action/candidate/JobBookMarkAction";
import {
  getJobAppliedList,
  saveJobApplied,
  useJobAppliedDispatcher,
} from "../../../action/JobAppliedAction";
import { useMyProfileContext } from "../../../action/MyProfileAction";
import {
  jobBookMarkDataWithId,
  jobBookMarkDataWithUserId,
  jobBookMarkRequestModel,
} from "../../../models/candidate/BookMarkedJob";
import {
  jobAppliedRequestModel,
  jobAppliedSaveRequestModel,
} from "../../../models/candidate/JobApplied";
import AuthService from "../../../services/AuthService";
import JobAppliedForm from "../jobs_Applied/JobAppliedForm";
import { getAllResumeCandidateMap } from "../../../apis/resumebuilder";
import { ResumeAllList } from "../../../models/candidate/Resume";

interface ICandidateSavedJobComponentProps {}

interface ICandidateSavedJobComponentState {
  currentJobId: number;
  filterFlag: boolean;
}

const defaultValues = {
  currentJobId: 0,
  filterFlag: true,
};

const CandidateSavedJobComponent: React.FC<ICandidateSavedJobComponentProps> = (
  props
) => {
  const [
    CandidateSavedJobComponentState,
    setCandidateSavedJobComponentState,
  ] = React.useState<ICandidateSavedJobComponentState>(defaultValues);
  const authorizationToken = AuthService.accessToken;

  const { filterFlag, currentJobId } = CandidateSavedJobComponentState;

  const jobBookMarkDispatcher = useJobBookMarkDispatcher();
  const jobBookMarkContext = useJobBookMarkContext();
  const { jobBookMark, jobBookMarkSaveRespond } = jobBookMarkContext;

  console.group(6754, jobBookMark);
  let history = useHistory();

  const myProfileContext = useMyProfileContext();
  const { basicInfo, loggedUserId, myProfile } = myProfileContext;

  const [isJobAppliedOpen, setIsJobAppliedOpen] = useState<boolean>(false);
  const [isShareOpen, setIsShareOpen] = useState<boolean>(false);

  const candidateId = loggedUserId;

  const [candidateResumes, setCandidateResumes] = useState({} as ResumeAllList);

  const {
    register,
    handleSubmit,
    watch,
    errors,
    setValue,
    getValues,
    control,
  } = useForm<ICandidateSavedJobComponentState>({
    defaultValues,
  });

  const jobAppliedDispatcher = useJobAppliedDispatcher();

  React.useEffect(() => {
    if (
      authorizationToken != null &&
      loggedUserId != 0 &&
      jobBookMarkSaveRespond.isSuccess
    ) {
      (async () => {
        await getJobBookMarkList(
          jobBookMarkDispatcher,
          {
            candidateId: loggedUserId,
            page: 1,
            pageSize: 60,
            searchTerm: "",
            showInactive: false,
            sortOrder: "",
          } as jobBookMarkRequestModel,
          authorizationToken
        );
      })();
    }
  }, [jobBookMarkSaveRespond]);

  const handleDeleteBookMarked = (jobId: number) => {
    if (authorizationToken != null)
      deleteJobBookMarkList(
        jobBookMarkDispatcher,
        {
          jobId: jobId,
          candidateId: loggedUserId,
        } as jobBookMarkDataWithUserId,
        authorizationToken
      );
  };

  useEffect(() => {
    if (candidateId) {
      getAllResumeCandidateMap({
        CandidateId: candidateId,
        Page: 1,
        PageSize: 10,
        SearchTerm: "",
        SortOrder: "",
        ShowInactive: false,
      })
        .then((res) => {
          //console.log(res.data);
          setCandidateResumes(res.data);
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }, [candidateId]);

  const handleJobApplied = () => {
    // console.log(89, jobId);
    // saveJobApplied(jobAppliedDispatcher, {
    //   isActive: true,
    //   jobId: jobId,
    //   candidateId: loggedUserId,
    //   jobAppliedId: 0,
    //   rowId: 0,
    // } as jobAppliedSaveRequestModel);
  };

  const handleJobSelect = (id: any) => {
    history.push(`/job_search/b/${id}`);
  };

  const handleShareButtonClick = (id: any, value: any) => {
    setCandidateSavedJobComponentState({
      ...CandidateSavedJobComponentState,
      currentJobId: id,
      filterFlag: value,
    });
    setIsShareOpen(!isShareOpen);
  };

  const handleJobApplayClick = (id: any) => {
    setCandidateSavedJobComponentState({
      ...CandidateSavedJobComponentState,
      currentJobId: id,
    });
    handleJobOpen();
  };

  const handleJobOpen = () => {
    setIsJobAppliedOpen(!isJobAppliedOpen);
  };

  return (
    <React.Fragment>
      <div className="content-wrapper">
        <div className="container-fluid">
          <h1 className="heading">Saved jobs</h1>
          <div className="clearfix" />
          <div className="row ">
            <div className="col-sm-12">
              <div className="section_box3">
                <div className="row">
                  <Scrollbars
                    style={{ height: 750 }}
                    autoHide
                    renderThumbVertical={({ style, ...props }) => (
                      <div
                        {...props}
                        style={{
                          ...style,
                          position: "relative",
                          display: "block",
                          width: "5px",
                          cursor: "pointer",
                          borderRadius: "inherit",
                          backgroundColor: "rgb(73, 69, 69)",
                          height: "115px",
                        }}
                      />
                    )}
                  >
                    {jobBookMark.data != undefined
                      ? jobBookMark.data.map((Job, i) => {
                          return (
                            <div
                              // onClick={() => handleJobSelect(Job.rowId)}
                              className="col-sm-3"
                              key={`${i}`}
                            >
                              <div className="matched_jobs">
                                <div className="matched_jobs_cat_t">
                                  {Job.categoryName}
                                </div>
                                <div className="jobs_start1">
                                  {Job.postedDate}
                                </div>
                                <div className="matched_star">
                                  {Job.isBookmarked ? (
                                    <i
                                      onClick={() =>
                                        handleDeleteBookMarked(Job.jobId)
                                      }
                                      className="fa fa-star"
                                      aria-hidden="true"
                                    />
                                  ) : (
                                    <i
                                      // onClick={() =>
                                      //   handleSaveJobBookMarked(Job.jobId)
                                      // }
                                      className="fa fa-star-o"
                                      aria-hidden="true"
                                    />
                                  )}
                                </div>
                                <div className="clearfix" />
                                <div onClick={() => handleJobSelect(Job.jobId)}>
                                  <div className="matched_heading">
                                    {Job.title}
                                  </div>
                                  <div className="matched_jobs_cat">
                                    <i
                                      className="fa fa-map-marker"
                                      aria-hidden="true"
                                    />
                                    {""}
                                    San Fransisco
                                  </div>
                                  <div className="matched_jobs_cat text-right">
                                    <i
                                      style={{ marginRight: 5 }}
                                      className="fa fa-clock-o"
                                      aria-hidden="true"
                                    />
                                    {"  "}
                                    {Job.jobType}
                                  </div>
                                  <div className="clearfix" />
                                  <p>{Job.description}....</p>
                                </div>
                                <div className="matched_jobs_share">
                                  <a
                                    onClick={() =>
                                      handleShareButtonClick(Job.jobId, true)
                                    }
                                  >
                                    <i
                                      className="fa fa-share-alt"
                                      aria-hidden="true"
                                    />
                                  </a>
                                </div>

                                <div className="jobs_apply">
                                  {Job.isApplied ? (
                                    <a>Applied</a>
                                  ) : (
                                    <a
                                      onClick={(id: any) =>
                                        handleJobApplayClick(Job.jobId)
                                      }
                                    >
                                      Apply Now
                                    </a>
                                  )}
                                </div>
                              </div>
                            </div>
                          );
                        })
                      : null}
                  </Scrollbars>
                </div>
              </div>
            </div>
          </div>
          <div className="clearfix" />

          <JobAppliedForm
            isJobAppliedOpen={isJobAppliedOpen}
            handleJobOpen={handleJobOpen}
            currentJobId={currentJobId}
            loggedUserId={loggedUserId}
            authorizationToken={authorizationToken}
            candidateResumes={candidateResumes}
          />
          {/* modal for share */}

          <Modal
            show={isShareOpen}
            onHide={() => {
              setIsShareOpen(!isShareOpen);
            }}
          >
            <Modal.Header closeButton>
              <Modal.Title>Share this job post on</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <div className="col-sm-12 m_t_30 text-center">
                <div className="social1">
                  <FacebookShareButton
                    url={`http://careerappui.clubactive.in/#/job_search/${currentJobId}`}
                    quote={`${
                      filterFlag
                        ? jobBookMark != undefined &&
                          jobBookMark.data != undefined
                          ? jobBookMark.data.filter(
                              (data) => data.rowId === currentJobId
                            )[0] != undefined
                            ? jobBookMark.data.filter(
                                (data) => data.rowId === currentJobId
                              )[0].description
                            : ""
                          : ""
                        : jobBookMark.data != undefined &&
                          jobBookMark.data != undefined
                        ? jobBookMark.data.filter(
                            (data) => data.rowId === currentJobId
                          )[0] != undefined
                          ? jobBookMark.data.filter(
                              (data) => data.rowId === currentJobId
                            )[0].description
                          : ""
                        : ""
                    }`}
                    hashtag={`${
                      filterFlag
                        ? jobBookMark != undefined &&
                          jobBookMark.data != undefined
                          ? jobBookMark.data.filter(
                              (data) => data.rowId === currentJobId
                            )[0] != undefined
                            ? jobBookMark.data.filter(
                                (data) => data.rowId === currentJobId
                              )[0].description
                            : ""
                          : ""
                        : jobBookMark.data != undefined &&
                          jobBookMark.data != undefined
                        ? jobBookMark.data.filter(
                            (data) => data.rowId === currentJobId
                          )[0] != undefined
                          ? jobBookMark.data.filter(
                              (data) => data.rowId === currentJobId
                            )[0].description
                          : ""
                        : ""
                    }`}
                    className={""}
                  >
                    {/* <FacebookIcon size={36} /> */}
                    <a href="#" className="social_face">
                      <i className="fa fa-facebook" aria-hidden="true" />
                    </a>
                  </FacebookShareButton>

                  <TwitterShareButton
                    url={`http://careerappui.clubactive.in/#/job_search/${currentJobId}`}
                    via={`${
                      filterFlag
                        ? jobBookMark != undefined &&
                          jobBookMark.data != undefined
                          ? jobBookMark.data.filter(
                              (data) => data.rowId === currentJobId
                            )[0] != undefined
                            ? jobBookMark.data.filter(
                                (data) => data.rowId === currentJobId
                              )[0].description
                            : ""
                          : ""
                        : jobBookMark.data != undefined &&
                          jobBookMark.data != undefined
                        ? jobBookMark.data.filter(
                            (data) => data.rowId === currentJobId
                          )[0] != undefined
                          ? jobBookMark.data.filter(
                              (data) => data.rowId === currentJobId
                            )[0].description
                          : ""
                        : ""
                    }`}
                    hashtags={[
                      `${
                        filterFlag
                          ? jobBookMark != undefined &&
                            jobBookMark.data != undefined
                            ? jobBookMark.data.filter(
                                (data) => data.rowId === currentJobId
                              )[0] != undefined
                              ? jobBookMark.data.filter(
                                  (data) => data.rowId === currentJobId
                                )[0].description
                              : ""
                            : ""
                          : jobBookMark.data != undefined &&
                            jobBookMark.data != undefined
                          ? jobBookMark.data.filter(
                              (data) => data.rowId === currentJobId
                            )[0] != undefined
                            ? jobBookMark.data.filter(
                                (data) => data.rowId === currentJobId
                              )[0].description
                            : ""
                          : ""
                      }`,
                    ]}
                    className={""}
                  >
                    {/* <FacebookIcon size={36} /> */}
                    <a href="#" className="social_twit">
                      <i className="fa fa-twitter" aria-hidden="true" />
                    </a>
                  </TwitterShareButton>

                  <LinkedinShareButton
                    url={`http://careerappui.clubactive.in/#/job_search/${currentJobId}`}
                    title={`${
                      filterFlag
                        ? jobBookMark != undefined &&
                          jobBookMark.data != undefined
                          ? jobBookMark.data.filter(
                              (data) => data.rowId === currentJobId
                            )[0] != undefined
                            ? jobBookMark.data.filter(
                                (data) => data.rowId === currentJobId
                              )[0].title
                            : ""
                          : ""
                        : jobBookMark.data != undefined &&
                          jobBookMark.data != undefined
                        ? jobBookMark.data.filter(
                            (data) => data.rowId === currentJobId
                          )[0] != undefined
                          ? jobBookMark.data.filter(
                              (data) => data.rowId === currentJobId
                            )[0].title
                          : ""
                        : ""
                    }`}
                    summary={`${
                      filterFlag
                        ? jobBookMark != undefined &&
                          jobBookMark.data != undefined
                          ? jobBookMark.data.filter(
                              (data) => data.rowId === currentJobId
                            )[0] != undefined
                            ? jobBookMark.data.filter(
                                (data) => data.rowId === currentJobId
                              )[0].description
                            : ""
                          : ""
                        : jobBookMark.data != undefined &&
                          jobBookMark.data != undefined
                        ? jobBookMark.data.filter(
                            (data) => data.rowId === currentJobId
                          )[0] != undefined
                          ? jobBookMark.data.filter(
                              (data) => data.rowId === currentJobId
                            )[0].description
                          : ""
                        : ""
                    }`}
                    source={`http://careerappui.clubactive.in/#/job_search/${currentJobId}`}
                  >
                    <a href="#" className="social_twit">
                      <i className="fa fa-linkedin" aria-hidden="true" />
                    </a>
                  </LinkedinShareButton>
                </div>
              </div>
              <div className="modal-footer  m-t-30"></div>
            </Modal.Body>
          </Modal>
          {/* end Model for share */}
        </div>
      </div>
    </React.Fragment>
  );
};
export default CandidateSavedJobComponent;
