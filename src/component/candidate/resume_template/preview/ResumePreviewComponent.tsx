import React, { useState, useEffect, useContext } from "react";
import { useForm } from "react-hook-form";
import PreviewHtmlTemplate from "./PreviewHtmlTemplate";
import { useHistory } from "react-router-dom";
import { GlobalSpinnerContext } from "../../../../context/GlobalSpinner";
import { toast, ToastContainer } from 'react-toastify';
import {
  saveResumeCandidateMap,
} from "../../../../apis/resumebuilder";
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';
import { Modal } from "react-bootstrap";
import { ErrorMessage } from '@hookform/error-message';

type objectIndexing = {
  [key: string]: any
}

interface IResumePreviewComponentProps {
  resumeInfo: any
  candidateId: Number
  resumeTemplateID: any
  resumeTemplateHtml: any
}

interface IResumePreviewComponentState { }

//const initialState = {};

interface ICandidateResumeState {
  resumeName: string
}

function SaveCandidateResume(props: any) {

  const defaultValues = {
    resumeName: ""
  };

  const { register, handleSubmit, watch, errors, setValue, getValues, control, reset } = useForm<ICandidateResumeState>({
    defaultValues
  });
  const onSubmit = (data: any) => {

    var file_name_string = data["resumeName"]
    var resume_name_array = file_name_string.split(".");
    //var resume_name_extension = resume_name_array[resume_name_array.length - 1];
    //console.log(resume_name_array[0]);
    //console.log(resume_name_extension);
    props.saveTemplateWithName(resume_name_array[0]);

  };

  return (
    <form onSubmit={handleSubmit(onSubmit)} noValidate>
      <Modal.Header closeButton>
        <Modal.Title>Add Resume</Modal.Title>
      </Modal.Header>
      <Modal.Body>

        <div className="col-sm-12">
          <div className="form-group">
            <label htmlFor="email">Resume Name</label>
            <input type="text" className="form-control" placeholder="Type Here" name="resumeName" ref={register({
              required: "Resume name is required"
            })} />

            <ErrorMessage errors={errors} name="resumeName" render={({ message }) => <div className="login_validation">{message}</div>} />
          </div>
        </div>

      </Modal.Body>
      <div className="modal-footer  m-t-30">
        <button className="btn btn-success save-event waves-effect waves-light" type="submit"> Add </button>
        <button onClick={() => { props.setIsOpen(false); }} data-dismiss="modal" className="btn btn-default waves-effect _cursor-pointer" type="button"> Cancel</button>
      </div>
      <div className="clearfix" />
    </form>
  );
}



const ResumePreviewComponent: React.FC<IResumePreviewComponentProps> = (props) => {
  // const [resumePreviewComponentState, setResumePreviewComponentState] = React.useState<IResumePreviewComponentState>(
  //   initialState
  // );
  const globalSpinner = useContext(GlobalSpinnerContext);
  const [isOpen, setIsOpen] = useState<boolean>(false);

  let history = useHistory()

  const handleReselectTemplate = () => {
    history.push({
      pathname: '/candidate/my-resume/resume/select_template',
    });
  };

  const saveTemplateWithName = (resumeName: any) => {
    let input = document.getElementById('pdfdiv');
    globalSpinner.showSpinner();
    if (input) {

      const divHeight = input.clientHeight;
      const divWidth = input.clientWidth;
      const ratio = divHeight / divWidth;
      let _pdfData: objectIndexing = {};
      _pdfData["Resume"] = input.innerHTML;
      html2canvas(input,
        { height: divHeight, width: divWidth, logging: true, useCORS: true, allowTaint: true, scale: 2 }
      ).then((canvas) => {
        // const componentWidth = divWidth;
        // const componentHeight = divHeight;
        // const orientation = componentWidth >= componentHeight ? 'l' : 'p';
        //let imgData = canvas.toDataURL('image/png');
        // const pdf = new jsPDF({
        //   orientation,
        //   unit: 'mm', //'px',
        //   format: 'a4'
        // })
        // pdf.internal.pageSize.width = componentWidth
        // pdf.internal.pageSize.height = componentHeight
        //pdf.addImage(imgData, 'PNG', 0, 0, componentWidth, componentHeight);
        let imgData = canvas.toDataURL("image/jpeg");
        const pdf = new jsPDF(); // using defaults: orientation=portrait, unit=mm, size=A4
        var width = pdf.internal.pageSize.getWidth();
        var height = pdf.internal.pageSize.getHeight();
        // console.log(divHeight, divWidth);
        // console.log(height, width);
        height = ratio * width;
        // console.log(height);
        // console.log(ratio);
        //pdf.addImage(imgData, 'JPEG', 0, 0, width - 3, height - 30);
        let sub = 10;
        if (ratio > 1.5 && ratio < 2) {
          sub = 50  //tested 1.6
        }
        else if (ratio > 1 && ratio < 1.5) {
          sub = 20 //tested 1.3
        }
        pdf.addImage(imgData, 'JPEG', 0, 0, width - 3, height - sub);
        var b64toBlob = require('b64-to-blob');
        // var info = imgData.replace("data:image/png;base64,", "");
        // var blob = b64toBlob(info, 'image/png');
        var info = imgData.replace("data:image/jpeg;base64,", "");
        var blob = b64toBlob(info, 'image/jpg');
        _pdfData["ResumeImageDocument"] = blob;
        var binary = pdf.output();
        // pdf.save('myPage.pdf');
        // binary = binary ? btoa(binary) : "";
        var blob2 = b64toBlob(btoa(binary), 'application/pdf');
        _pdfData["ResumeFileDocument"] = blob2;
        // var FileSaver = require('file-saver');
        // FileSaver.saveAs(blob, "1.png");
        // FileSaver.saveAs(blob2, "blob2.pdf");
        const formData = new FormData();
        formData.append("RowId", JSON.stringify(0));
        formData.append("ResumeCandidateMapId", JSON.stringify(0));
        formData.append("ResumeTemplateId", JSON.stringify(props.resumeTemplateID));
        formData.append("CandidateId", JSON.stringify(props.candidateId));
        formData.append("Resume", _pdfData["Resume"]);
        formData.append("ResumeName", resumeName);
        formData.append("ResumeFile", resumeName + ".pdf");
        formData.append("ResumeFileDocument", _pdfData["ResumeFileDocument"], resumeName + ".pdf");
        formData.append("ResumeImage", resumeName + ".jpg");
        formData.append("ResumeImageDocument", _pdfData["ResumeImageDocument"], resumeName + ".jpg");
        formData.append("IsActive", JSON.stringify(true));
        // console.log(formData);
        // for (var key of (formData as any).entries()) {
        //   console.log(key[0] + ", " + key[1]);
        // }
        saveResumeCandidateMap(formData).then((res) => {
          console.log(res.data);
          globalSpinner.hideSpinner();
          if (res.data.isSuccess) {
            toast.success(res.data.message);
          }
          else {
            toast.error(res.data.message);
          }
        }).catch((err) => {
          console.log(err);
          globalSpinner.hideSpinner();
          toast.error(err.toString());
        });

      })
    }
    else {
      globalSpinner.hideSpinner();
      toast.error("Unable to save,Please try again.");
    }
  };

  return (
    <>
      <ToastContainer />
      <div className="content-wrapper">
        <div className="container-fluid">

          <button className="btn cutomise_but"
            disabled={props.resumeTemplateID == 0 ? true : false}
            onClick={() => { setIsOpen(!isOpen) }}> Save Resume</button>
          <button className="btn cutomise_but" onClick={() => { handleReselectTemplate(); }}> Reselect Template</button>
          <div className="clearfix" />
          <PreviewHtmlTemplate
            resumeInfo={props.resumeInfo}
            resumeTemplateID={props.resumeTemplateID}
            resumeTemplateHtml={props.resumeTemplateHtml} />
        </div>
        <div className="clearfix" />
      </div>

      <Modal show={isOpen} onHide={() => { setIsOpen(!isOpen) }}>
        <SaveCandidateResume setIsOpen={setIsOpen} isOpen={isOpen} saveTemplateWithName={saveTemplateWithName} />
      </Modal>
    </>
  );
};
export default ResumePreviewComponent;
