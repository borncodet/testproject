import React, { useState, useContext, useEffect } from "react";
import moment from "moment";


function Skill(props: any) {

  const { rowSkilldata } = props;

  return (
    rowSkilldata.filter((f: any) => f.isActive == true).map((e: any, i: any) =>
      <td style={{ fontSize: '13px', textAlign: 'left', color: '#2d2d2d', padding: '15px 0px 10px 0px', fontWeight: 600, lineHeight: '20px' }}> {e.skillName} </td>
    )
  );
}

function Training(props: any) {

  const { rowTrainingdata } = props;

  return (
    rowTrainingdata.filter((f: any) => f.isActive == true).map((e: any, i: any) =>
      <td style={{ fontSize: '13px', textAlign: 'left', color: '#2d2d2d', padding: '15px 0px 10px 0px', fontWeight: 600, lineHeight: '20px' }}> {e.trainingCertificate} </td>
    )
  );
}


function PreviewTemplate(props: any) {

  const { resumeInfo } = props;
  let profileSummary = "";
  let profileImage = ""


  const renderCandidate = () => {

    let candidateHtml;
    if (resumeInfo.candidateInfo != undefined && resumeInfo.candidateInfo.length > 0) {
      if (resumeInfo.candidateProfileImageInfo != undefined && resumeInfo.candidateProfileImageInfo.length > 0) {
        //profileImage = resumeInfo.candidateProfileImageInfo[0].imageUrl
        profileImage = `https://localhost:44361/Upload/ProfileImage/` + resumeInfo.candidateProfileImageInfo[0].imageUrl
      }
      else {
        profileImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMEAAACmCAYAAABjs+t+AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAAAAhdEVYdENyZWF0aW9uIFRpbWUAMjAyMDoxMToxMSAxMjoyNzoxMK7z7acAABEiSURBVHhe7Z33i1RXFMfvbjQWlFiwYW+xgAUs2GLDqLCxGwiRBH/zX/KH5BdFUTRRREGISESjYsEWYtegYsUSDMbEhI2f67vr7Djz5r2ZNzvv3Hs+sMnM7Lr7yvneU+659zX99ddfrSZwOnXqZJqamsyrV6/M9evXzd27d829e/fMRx99ZL9857///rNfffv2NSNGjDDDhg0zAwYMMK2trebff/+NfspfghVBseHfuHHDPHnyxH4vFOMvhRNEt27dzMSJE60g+vfvb7/nqyCCEwHGD4z0Fy5cMA8ePLA3PWTDL4cTRL9+/cyYMWOsKDp37uydh/BaBG60f/nypbly5Yq5dOmS+eeff9TgY8Do4c2bN2bdunXeewHwSgRulMfgf/31V/P06VM1+Bpx3oBrOHXqVDNp0iTTpUsX9QR5A+PnphDXnzp1yrw9J/Pxxx9H31WywnmJsWPHmmnTppkePXp4IQbRIigc+dX4Ow7nHcaPH29mzpxpk2jJYhAtAh35G4sTA16BUEkqzdH/xbF3715z5MgRexNUAI2BPIFrf/78eeuVnWeWhjgRUKJ79uyZrfio8ecDxLBlyxZbdub+SENMOORGmePHj9tSpwogf1B+Hj16tJk1a5aopDnXIsDw//77b3P06FFz8+ZNNXxBIAjaMJYvX266du0afZpPci2CO3futMX9WuuXh0ucN23alGuvkNucAC/w008/2dcqAJm4xHn37t3m9evXuU2ccykCkqvHjx9r+OMJL168MDt27LDzOXlMnHMXDjFaXLx40Zw4cUJF4BkucV64cKG9z3kJkXIjApcEHzp0yJbaVAB+4uZ1SJj79OkTfdpYchEOIYA///zT7Ny507Y2qwD8hTwBj7Bnz57chEYNFwEXgkUt27Zt0ypQILiE+ZdffsmFEBoaDnEBTp8+bc6ePaujf4DgEViws3btWvu+UTlCQ0Xw888/6yRY4OD9e/XqZXOERs0yd6gIXOx/8OBBWzbzOfRxE0VAqzE3mBlUVmyxWssJ3yWHjIr0RPE5r58/f25/luvEl/td7pr5eO0471WrVpmBAwd2qBjqLgJn+Lt27fIm5ncGPmTIEPPpp5/aRSYs4+zotbdcW6CahkcltwIfrjGTa3PnzjVTpkyxg0E9qasInAB+/PFHq3LpN8cZP0sMJ0+ebHr27JmLRedODG/vpRUCayw4TulhJjazYMECM2HChLoKoW4i8EkAGBSwimrcuHG5XmPLdefYfvvtN1tw4NglX3vnERh06nXNMxeBL8bPsRPDz58/325EVW+XXC+owLH2gjyMjQekegfuR73yhbp4gn379tkkT7IAWDI4Y8YML/bYceHSmTNnRJejuS8bNmzIvIqUqQgYdX744QexAnBx9NKlS82gQYPEjv7l4P4wI79//36xAxTHzbxClkLIbMaYC0zdn60MpV5gypVffvmldbm+CQA4J86N83R5jjTwBoR2WXqCTESAANjSkFZZya525cqV9lykhz9xcG6cJ0LgnKXBAMu8CSE39yoLahYB8Sa7ONMHIlkA7KETEmyxyDlLFQIRR1a9RzWJwFWCDhw4kPt1pOVwAqDHPSQIjThnqUJgwGXDhWvXrrUl/tVSkwhwrZRCpeYAhQLwMQeoBOdMDZ4mNok5AkI4fPiwHYhrEUJNIiARxpCkioC2h1AFUAjVFgxKqhAYiGvJ46oWAcqjX0WqAKClpSV4AThcO7NEGIgZkKvND6oSgcsFpCbCILVEWA8YRam7L168WGR+wEDMgHz58uWqwqKqPQG1Wqlwo2nM8rkUmhauBYvgaQ6UKAQGZPaoYnBOS2oR4HLYCpE+FIngAbjZtEAr7SE0nDNnju2ZkggegcE5rTdIJQJ++aNHj0TvBcqF+uyzzzQXiGHevHliwyIGZ3qk0pDaE6A0qQLgxs6ePdu2QiulISyibwpvKRFskybBNN4glQgIg1i4IRXcPPMCmgvEg5fEW0olbcUysQhQluQwCC+Am1eSgbeUWkFDBOximJRUniCtwvICN5OJMdy8eoFkSL9OLDFNSiIR4AVQlmQRsEiGBTJKcnh4t+T5lKSTZ4lEwKjABrlSIRfo6G08fICBQ3JIRHdzkgQ5VgT8AlaJff/992JzAW7i6tWrVQBVQF5Aa4nEcinQ3cwal0pCiBUBe+mwTaLkMIg9gbJafBEaDBzkUmweJhEG7mPHjtndzuMoKwLUw4ZZrEmVKgKOm1ZhnRirDbZIlOoNsIHt27fHhnUlRYAA2NWM2TfJXmDo0KGJYkKlPHiDwm0jJYItnDx5smxEUFIEhEH8I6kCAE58+PDh0TulVhhQ4kbTPIMdkxuw/1IpPhABIydtqZK9gEPnBbKD3ECqCABbpsxfKjL4QAR4AR7TL10AbidopXZcgiwZ7JmtKUslye1EgEroEpW8dxBoKJQ9DCiS8wIHrT/F3qCdCKSXRAvp3bt39ErJAmyDBFl6SIQ3KKadCEgcJJdEHdwoZoiV7KDlhI2JpUMXNNvXF3qDNhHwYZrOu7zTvXt3TYoz5pNPPhHtCYCQjt0S8WyONhFgMLgKH0IhzkGT4uxBBD5A5ZPc13kDKwLe3L59W7zKHTwIrlDpSu0wSOJdfRkk2ZnCYUWAwZA1+3CCCBkRaNt09vjSg4Wdkxe4cqkVgQ9l0UIYsZTsoauUAcYHGCyJfoiCmvnPjRs3vBEAaMNcffApxMTeyYE5J+sJfBOBzhEolcDeiX6YFmh++PCh6B0kisHNSe1/zzvkWVJ3sC4FQqCxrvnWrVteeQFFSQp2//vvv5tm30IhRUkDcwbNPoVCipIWHECzegEldFQEStBYTxC9VpRg8VIEvpTw8gYTS9U8BCPveCcC3BttIEp9oJDiWwjtpSfQzbaUNHgpAu0dqg9c11evXkXv/MFLETAB4lOzV14g15K6E10cXuYEJG+6niB7/vjjDy+LDl56AkYrXV+cLbTc++gFwEsRMFqxpbySLXhYHydXvRQBcMPcQmolG9ik2Ue8FAGjlXqC7Hnx4oV6Ailwo9hPlUfOqjfIhu+++67srs7S8TYcQgj379+P3inVwiDC6kOfW1G8FQEwX+Bjr0tHc+fOHS/DIIfXIgCSOQ2JasP31Ydei4AbxwNHdPa4evCkvq8+9F4E7LKtvUTVgQdlNwafvQB4Hw6R0BHTakiUHjwouzGoCITDDaRcqiFReliXQXHBd4IQARNnPrYA15tQtuPxXgSO4qeTKJW5evWqisAXuJHXrl2L3ilJYMDwtWu0mGA8AX0vBw4cUG+QEEqjPjytMgnBiABvwMSZziBXhjXaZ8+ejd75TzAiAIRw8uRJXYhfAYoI5FChEJwImEH2tRsyCwgXz507F70Lg6BEAOoN4uE5XqFUhRxBikC9QWnwAjzA0ee26VIEJwJw3kBpD41yJMQheQEIVgR4Ay2XvofwkFwgNAFAkCIAbrb2E72H8JCnOaoIAkK9wXvwAkeOHInehUewIoBjx44Fv0kXgwAtJUwkhugFIGgR0Btz6tSpoMullEQZDEJpkShF0CJg5KMkSDwcYljEOR86dCi4kmgxQYsAEMLBgwejd+GA97t48WLQYZBDRfDWAFg9debMmegT/8EDsGrsxIkTQYdBjuBFABgCk0ShhETkAXg/FcA7VAQRIYUE5AGhLJhJgoogAhFQK/e9UsT5aR7QHhVBAeyx4/MEGud19+5dDYOKUBEUgHEcPnzY22cbuFxAaY+KoAQ+Ggph0NGjR6N3SiEqgiKIlVmUT/LoS36AV7t8+bIN9TQX+BAVQQkwFGLnzZs3m9evX4sOjTh2Ev7QWyPiUBHEgBikh0bkNyFsqlsLKoIYMBz6irZs2SLWG+zYsUM9QAVUBAlg2eHjx49FCYFjZTfu0JvjkqAiSAAeged2SYNj1jCoMiqChOAJJMHS0efPn0fvlDhUBB5Dd6x6gsqoCBLie09RyKgIEkByOXTo0OidHLp37x69UuJQESRk8ODBohblt7a2mn79+ml1KAHNepHioe9+2rRppkuXLtEncsB76f2tjIogBgQwevRoM336dHFbs3C8o0aNUm9QAa6NhkNlcAJYsmSJ2L2JOO6WlhbTq1cvFUIZrAh0pGiPW3a4ePFi8/nnn4sVgIMS6YoVK8ygQYN0SWUJuD5Nly5daqXLUPtL3jFp0iQzdepU23YgXQCFUOKlnZodJpR3MPiPGDHCNA8fPjz6KEy4ELRL9+zZ03zzzTc2/gefBABv3rwx48ePN19//bX1COr939178qamtze7df/+/ebBgwdBzS46Ixg7dqyZMGGCGTBggDWUEOA82YadXagh1FllbGDjxo2m6e0FaeUhbaytDSEk4sT5IhciaXSlT99G/koQ7tEdy70/f/68fY0YQhEENsAAuGDBAtP09uRb+XDr1q32m77ijJ+Kz+TJk4Ma+eNw7eFsw3LhwgUbEYDvYiAkXLVqlRk4cOA7EXAhSI4ZFXw7eQyfcxo3bpw1fmJ/ZlNDG/krgQ3Qeeoe38p27ay19lUM2MH69eutHbR5gmfPnpk9e/Z4ExJh/EClh4oPYY8afjKcd2BgvHXrlh01EYMvgsA2Zs6caQfFdiKghLZr1y7xT3V0Iz+Gr8ZfG04MCIG84cmTJ16IARuhEujOo00Eju3bt7eNohLgWPmi/Ddr1iw1+jriQiYGShbv82wHvISk6IHj3bBhg+natWv0SZEIOEn2rJewZbcz/iFDhtgMv0ePHmr8HQi24h78jZeQMHByjMycf/HFF+2KIu16hzAiEkgJ0A+zZs0ae0KoWgXQsXC9CSeIq7/99ls7wuYdRDBlyhRbGCnkg3DIgVLYaqTRHoGL60Z7Mnota8rAeQqe+9DoR8NiQ/SBMTtcarAs20XarVs3m1g20s1x4Th4N9qrAOTgPMWcOXNsf04jPUXfvn3LCgDKioB/wGKSRomAv0ufCwevxi8X7IiBjNl5RNHR9oT4KJjEEbuegErL7NmzO1zF/L0+ffrY1+XUq8iBQYyQltxh4sSJtmGxI0BwtMewwi7OjsrmBIV0VNmUv8EXwnMTGYpfMB/FQwPZ47XeNsVgWlwOLUXFlWUkOPPmzau7N+D3U/HhoFUA/oJX6N+/v/UK9fYIzB1ROq9ERRFgjDSd4VbqBQIgCaeXQ+v9/sP95WvZsmVt3r8ezJ07N5EtVRQBoF68Qda4kydpcgesAggHih5fffWVLcNnLQQGVqKYJCQSAeDCsjxQF/5wEUiatAIUHgx4eH6qgFmvgSZySTqgJhYBvzCriTNOlnht9erVNlHS0T9c3L1nLgibyEoIaSKXxCIAdmCo9SD598w/LFy4MPpEUd6F3HQFYBtZ2BiLppKSqERaTDUlUyoBixYtsut5NfRR4iA6OH36tG25SBt9YGfr1q1rm2dKQipPACQbS5cuTaVWflYFoCQFG2HXj7ThET87Y8aMVF4AUouAGI4kBreVBA6Mn1UBKGnAzgiPkm4Ox8/QI4R40tpZahGAU2qlg+P7zDGgThWAkhaEQPk8aUi0fPnyqoosVYkAkvwxSqDs5akCUKqF/rVKBRm+hwCSzA6XomoRQNzBkaDMnz8/eqco1cFgSwMc+UGpyAP7o9ugUpNcHDWJgFCnVEmLg3UJSrUHpigOIgnaoYsX5mBn5AysWagl2qiqRFoMB7Nz504rBl7jIdjdSwWgZAmVSVY7YmPYGstr6WSo1c5q8gQO4ra1a9dapZLEjBw5UgWg1AX2kSLUZqFOFgKATDwBoFK28uMB0hKf7KLIAAGw3UuWNpaZCMB17akAlHqCnWVpY5mKQFEkkklOoCiSUREowaMiUIJHRaAEj4pACR4VgRI4xvwPmYiAC4AtHyIAAAAASUVORK5CYII="
      }
      profileSummary = resumeInfo.candidateInfo[0].profileSummary
      candidateHtml = <>
        <tr>
          <td valign="top" style={{ background: '#e2e2e2' }}>
            {/* <img src={require("./../../../../images/resume_preview_img.jpg")} /> */}
            <img width="100%" height="100%" src={profileImage} /> :
            </td>
        </tr>
        <tr>
          <td valign="top" style={{ background: '#222222' }}>
            <table width="100%"
              //  border={0}
              cellSpacing={0} cellPadding={0}>
              <tbody><tr>
                <td style={{ fontSize: '26px', textAlign: 'center', color: '#fff', padding: '10px 0px 5px 0px', fontWeight: 600 }}>{resumeInfo.candidateInfo[0].firstName} {resumeInfo.candidateInfo[0].lastName}</td>
              </tr>
                <tr>
                  <td style={{ fontSize: '16px', textAlign: 'center', color: '#fff', padding: '0px 0px 10px 0px', fontWeight: 600 }}>{resumeInfo.candidateInfo[0].designationName}</td>
                </tr>
              </tbody></table>
          </td>
        </tr>
        <tr>
          <td valign="top" style={{ background: '#e2e2e2' }}>
            {/* <table width="100%"
              // border={0}
              cellSpacing={0} cellPadding={0}>
              <tbody><tr>
                <td colSpan={2} style={{ fontSize: '18px', textAlign: 'left', color: '#222222', padding: '25px 0px 20px 25px', fontWeight: 600 }}>Contact</td>
              </tr>
                <tr>
                  <td width="25%" style={{ fontSize: '14px', textAlign: 'left', color: '#222222', padding: '0px 0px 15px 25px', fontWeight: 600 }}>
                    <img
                      src={require("./../../../../images/resume_map.png")}
                    // src="images/resume_map.png" 
                    /></td>
                  <td width="75%" style={{ fontSize: '14px', textAlign: 'left', color: '#222222', padding: '0px 0px 15px 0px', fontWeight: 500 }}>{resumeInfo.candidateInfo[0].currentAddress1}</td>
                </tr>
                <tr>
                  <td width="25%" style={{ fontSize: '14px', textAlign: 'left', color: '#222222', padding: '0px 0px 15px 25px', fontWeight: 500 }}><img
                    src={require("./../../../../images/resume_phone.png")}
                  //  src="images/resume_phone.png" 
                  /></td>
                  <td width="75%" style={{ fontSize: '14px', textAlign: 'left', color: '#222222', padding: '0px 0px 15px 0px', fontWeight: 500 }}>{resumeInfo.candidateInfo[0].phoneNumber}</td>
                </tr>
                <tr>
                  <td width="25%" style={{ fontSize: '14px', textAlign: 'left', color: '#222222', padding: '0px 0px 15px 25px', fontWeight: 600 }}>
                    <img
                      src={require("./../../../../images/resume_mail.png")}
                    //  src="images/resume_mail.png" 
                    /></td>
                  <td width="75%" style={{ fontSize: '14px', textAlign: 'left', color: '#222222', padding: '0px 0px 15px 0px', fontWeight: 600 }}>{resumeInfo.candidateInfo[0].email}</td>
                </tr>
              </tbody></table> */}
            <table width="100%" cellSpacing={0} cellPadding={0}>
              <tbody><tr>
                <td colSpan={2} style={{ fontSize: '18px', textAlign: 'left', color: '#222222', padding: '25px 0px 20px 25px', fontWeight: 600 }}>Basic Information</td>
              </tr>
                <tr>
                  <td width="25%" height={70} valign="top" style={{ fontSize: '14px', textAlign: 'left', color: '#222222', padding: '0px 0px 15px 25px', fontWeight: 600 }}>Current Address</td>
                  <td width="75%" valign="top" style={{ fontSize: '14px', textAlign: 'left', color: '#222222', padding: '0px 0px 15px 15px', fontWeight: 500 }}> {resumeInfo.candidateInfo[0].currentAddress1}</td>
                </tr>
                <tr>
                  <td width="25%" valign="top" style={{ fontSize: '14px', textAlign: 'left', color: '#222222', padding: '0px 0px 15px 25px', fontWeight: 600 }}> Permanent Address </td>
                  <td width="75%" valign="top" style={{ fontSize: '14px', textAlign: 'left', color: '#222222', padding: '0px 0px 15px 15px', fontWeight: 500 }}>{resumeInfo.candidateInfo[0].permanantAddress1}</td>
                </tr>
                <tr>
                  <td width="25%" valign="top" style={{ fontSize: '14px', textAlign: 'left', color: '#222222', padding: '0px 0px 15px 25px', fontWeight: 600 }}>DOB</td>
                  <td width="75%" valign="top" style={{ fontSize: '14px', textAlign: 'left', color: '#222222', padding: '0px 0px 15px 15px', fontWeight: 500 }}>{resumeInfo.candidateInfo[0].dob}</td>
                </tr>
                <tr>
                  <td valign="top" style={{ fontSize: '14px', textAlign: 'left', color: '#222222', padding: '0px 0px 15px 25px', fontWeight: 600 }}>Gender</td>
                  <td valign="top" style={{ fontSize: '14px', textAlign: 'left', color: '#222222', padding: '0px 0px 15px 15px', fontWeight: 500 }}>{resumeInfo.candidateInfo[0].genderTitle}</td>
                </tr>
                <tr>
                  <td valign="top" style={{ fontSize: '14px', textAlign: 'left', color: '#222222', padding: '0px 0px 15px 25px', fontWeight: 600 }}>Nationality</td>
                  <td valign="top" style={{ fontSize: '14px', textAlign: 'left', color: '#222222', padding: '0px 0px 15px 15px', fontWeight: 500 }}> {resumeInfo.candidateInfo[0].countryName}</td>
                </tr>
              </tbody></table>
          </td>
        </tr>
        <tr>
          <td valign="top" style={{ background: '#e2e2e2' }}>
            <table width="100%" cellSpacing={0} cellPadding={0}>
              <tbody><tr>
                <td colSpan={2} style={{ fontSize: '18px', textAlign: 'left', color: '#222222', padding: '25px 0px 20px 25px', fontWeight: 600 }}>Contact</td>
              </tr>
                <tr>
                  <td width="25%" style={{ fontSize: '14px', textAlign: 'left', color: '#222222', padding: '0px 0px 15px 25px', fontWeight: 600 }}><img src={require("./../../../../images/land_phone.png")} /></td>
                  <td width="75%" style={{ fontSize: '14px', textAlign: 'left', color: '#222222', padding: '0px 0px 15px 0px', fontWeight: 500 }}>{resumeInfo.candidateInfo[0].telephoneNumber}</td>
                </tr>
                <tr>
                  <td width="25%" style={{ fontSize: '14px', textAlign: 'left', color: '#222222', padding: '0px 0px 15px 25px', fontWeight: 500 }}><img src={require("./../../../../images/resume_phone.png")} /></td>
                  <td width="75%" style={{ fontSize: '14px', textAlign: 'left', color: '#222222', padding: '0px 0px 15px 0px', fontWeight: 500 }}>{resumeInfo.candidateInfo[0].phoneNumber}</td>
                </tr>
                <tr>
                  <td width="25%" style={{ fontSize: '14px', textAlign: 'left', color: '#222222', padding: '0px 0px 15px 25px', fontWeight: 600 }}><img src={require("./../../../../images/resume_mail.png")} /></td>
                  <td width="75%" style={{ fontSize: '14px', textAlign: 'left', color: '#222222', padding: '0px 0px 15px 0px', fontWeight: 500 }}>{resumeInfo.candidateInfo[0].email}</td>
                </tr>
              </tbody></table>
          </td>
        </tr>
        <tr>
          <td valign="top" style={{ background: '#e2e2e2' }}>
            <table width="100%"
              // border={0}
              cellSpacing={0} cellPadding={0}>
              <tbody><tr>
              </tr>
                <tr>
                  <td style={{ fontSize: '18px', textAlign: 'left', color: '#222222', padding: '25px 0px 20px 25px', fontWeight: 600 }}>Languages</td>
                </tr>
                {resumeInfo.candidateLanguageMapInfo != undefined && resumeInfo.candidateLanguageMapInfo.length > 0 && resumeInfo.candidateLanguageMapInfo.map((e: any, i: any) => {
                  return <tr>
                    <td style={{ fontSize: '14px', textAlign: 'left', color: '#222222', padding: '0px 0px 10px 25px', fontWeight: 500 }}>{e.languageName} </td>
                  </tr>
                })}
              </tbody></table>
          </td>
        </tr>
      </>


    }

    return candidateHtml

  }

  const renderExperiences = () => {
    let experienceHTml = ""
    if (resumeInfo.candidateExperienceInfo != undefined && resumeInfo.candidateExperienceInfo.length > 0) {
      experienceHTml = resumeInfo.candidateExperienceInfo.filter((f: any) => f.isActive == true).map((e: any, i: any) =>
        <>
          <tr>
            <td style={{ fontSize: '14px', textAlign: 'left', color: '#4b56c0', padding: '15x 0px 10px 0px', fontWeight: 600, lineHeight: '20px' }}>{e.employerName}</td>
            <td style={{ fontSize: '13px', textAlign: 'right', color: '#2d2d2d', padding: '15px 0px 10px 0px', fontWeight: 600, lineHeight: '20px' }}> {moment(e.toDate, "DD/MM/YYYY").year()}</td> {/*  {moment(e.toDate, "DD/MM/YYYY").format('MMMM')} */}
          </tr>
          <tr>
            <td colSpan={2} style={{ fontSize: '14px', textAlign: 'left', color: '#4b56c0', padding: '15x 0px 10px 0px', fontWeight: 600, lineHeight: '30px' }}>{e.jobRole}</td>

          </tr>
          {/* <tr>
            <td colSpan={2} style={{ fontSize: '12px', textAlign: 'left', color: '#2d2d2d', padding: '0px 0px 0px 0px', fontWeight: 600, lineHeight: '20px' }}>{e.locationId}</td>
          </tr> */}
          {/* <tr>
            <td colSpan={2} style={{ fontSize: '12px', textAlign: 'left', color: '#2d2d2d', padding: '0px 0px 0px 0px', fontWeight: 600, lineHeight: '20px' }}>{e.jobRole}</td>
          </tr> */}
          <tr>
            <td colSpan={2} style={{ fontSize: '15px', textAlign: 'left', color: '#222222', padding: '0px 0px 10px 0px', fontWeight: 600, lineHeight: '20px' }}>Responsibilities</td>
          </tr>
          <tr>

            <td colSpan={2} style={{ fontSize: '12px', textAlign: 'left', color: '#222222', padding: '0px 0px 15px 0px', fontWeight: 500, lineHeight: '20px' }}>{e.responsibilities}</td>
          </tr>
          <tr>
            <td colSpan={2} style={{ fontSize: '15px', textAlign: 'left', color: '#222222', padding: '0px 0px 10px 0px', fontWeight: 600, lineHeight: '20px' }}>Achievements</td>
          </tr>
          <tr>
            <td colSpan={2} style={{ fontSize: '12px', textAlign: 'left', color: '#222222', padding: '0px 0px 15px 0px', fontWeight: 500, lineHeight: '20px' }}>{e.achievements}</td>
          </tr>

        </>

      )
    }
    return experienceHTml
  }

  const renderSkill = () => {

    let skillHtml = ""
    if (resumeInfo.candidateSkillsInfo != undefined && resumeInfo.candidateSkillsInfo.length > 0) {

      const skillColumns = resumeInfo.candidateSkillsInfo.filter((f: any) => f.isActive != false).map((e: any, i: any) => {
        return i % 4 === 0 ? resumeInfo.candidateSkillsInfo.filter((f: any) => f.isActive != false).slice(i, i + 4) : null;
      })

      skillHtml = skillColumns.map((e: any, i: any) => {
        return e != null ? <tr><Skill rowSkilldata={e} /></tr> : <></>;
      }
      )
    }

    return skillHtml

  }

  const renderTraining = () => {

    let trainingHtml = ""
    if (resumeInfo.trainingInfo != undefined && resumeInfo.trainingInfo.length > 0) {

      const trainingColumns = resumeInfo.trainingInfo.filter((f: any) => f.isActive != false).map((e: any, i: any) => {
        return i % 4 === 0 ? resumeInfo.trainingInfo.filter((f: any) => f.isActive != false).slice(i, i + 4) : null;
      })

      trainingHtml = trainingColumns.map((e: any, i: any) => {
        return e != null ? <tr><Training rowTrainingdata={e} /></tr> : <></>;
      }
      )
    }

    return trainingHtml

  }
  const renderSocial = () => {

    let socialAccountList: any[] = [];
    if (resumeInfo.socialAccountsInfo != undefined && resumeInfo.socialAccountsInfo.length > 0) {

      if (resumeInfo.socialAccountsInfo[0].google) {
        socialAccountList.push(<tr>
          <td width="25%" style={{ fontSize: '14px', textAlign: 'left', color: '#222222', padding: '0px 0px 15px 25px', fontWeight: 600 }}><img
            src={require("./../../../../images/google.png")
            }
          />

          </td>
          <td width="75%" style={{ fontSize: '14px', textAlign: 'left', color: '#222222', padding: '0px 0px 15px 0px', fontWeight: 500 }}>{resumeInfo.socialAccountsInfo[0].google}</td>
        </tr>
        );
      }

      if (resumeInfo.socialAccountsInfo[0].facebooks) {
        socialAccountList.push(
          <tr>
            <td width="25%" style={{ fontSize: '14px', textAlign: 'left', color: '#222222', padding: '0px 0px 15px 25px', fontWeight: 600 }}><img
              src={require("./../../../../images/facebook.png")}
            /></td>
            <td width="75%" style={{ fontSize: '14px', textAlign: 'left', color: '#222222', padding: '0px 0px 15px 0px', fontWeight: 500 }}>{resumeInfo.socialAccountsInfo[0].facebooks}</td>
          </tr>
        );
      }


      if (resumeInfo.socialAccountsInfo[0].twitter) {
        socialAccountList.push(
          <tr>
            <td width="25%" style={{ fontSize: '14px', textAlign: 'left', color: '#222222', padding: '0px 0px 15px 25px', fontWeight: 600 }}><img
              src={require("./../../../../images/twitter.png")}
            /></td>
            <td width="75%" style={{ fontSize: '14px', textAlign: 'left', color: '#222222', padding: '0px 0px 15px 0px', fontWeight: 500 }}>{resumeInfo.socialAccountsInfo[0].twitter}</td>
          </tr>

        );
      }


      if (resumeInfo.socialAccountsInfo[0].linkedIn) {
        socialAccountList.push(
          <tr>
            <td width="25%" style={{ fontSize: '14px', textAlign: 'left', color: '#222222', padding: '0px 0px 15px 25px', fontWeight: 600 }}><img
              src={require("./../../../../images/linkedin.png")}
            /></td>
            <td width="75%" style={{ fontSize: '14px', textAlign: 'left', color: '#222222', padding: '0px 0px 15px 0px', fontWeight: 500 }}>{resumeInfo.socialAccountsInfo[0].linkedIn}</td>
          </tr>
        );
      }

    }

    return socialAccountList
  }

  return (
    <React.Fragment>
      <div style={{ padding: '30px 30px 0px 30px', display: props.resumeTemplateID != 0 ? 'block' : 'none' }}>
        <table width="100%" cellSpacing={0} cellPadding={0} style={{ background: '#fff' }}>
          <tbody>
            <tr>
              <td width="25%" valign="top" style={{ background: '#e2e2e2' }}>
                <table width="100%" cellSpacing={0} cellPadding={0}>
                  <tbody>
                    <tr>
                      <td valign="top" style={{ background: '#4b56c0', fontSize: '16px', textAlign: 'center', color: '#fff', padding: '10px 0px 10px 0px', fontWeight: 600 }}>Resume</td>
                    </tr>
                    {renderCandidate()}
                    <tr>
                      <td valign="top" style={{ background: '#e2e2e2' }}>
                        <table width="100%" cellSpacing={0} cellPadding={0}>
                          <tbody>
                            <tr>
                              <td colSpan={2} style={{ fontSize: '18px', textAlign: 'left', color: '#222222', padding: '25px 0px 20px 25px', fontWeight: 600 }}>Social</td>
                            </tr>
                            {renderSocial()}
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
              <td width="75%" valign="top" style={{ fontSize: '14px', textAlign: 'left', color: '#222222', padding: '30px', fontWeight: 500 }}>
                <table width="100%" cellSpacing={0} cellPadding={0}>
                  <tbody>
                    <tr>
                      <td style={{ padding: '0px 0px 30px 0px' }}>
                        <table width="100%" cellSpacing={0} cellPadding={0}>
                          <tbody>
                            <tr>
                              <td valign="top" style={{ fontSize: '20px', textAlign: 'left', background: '#f9f9f9', color: '#222222', padding: '7px 15px 7px 15px', fontWeight: 500 }}>Personal Summary</td>
                            </tr>
                            <tr>
                              <td valign="top" style={{ fontSize: '12px', textAlign: 'left', color: '#222222', padding: '10px 0px 10px 0px', fontWeight: 500, lineHeight: '20px' }}>
                                {profileSummary}
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td style={{ padding: '0px 0px 30px 0px' }}>
                        <table width="100%" cellSpacing={0} cellPadding={0}>
                          <tbody>
                            <tr>
                              <td colSpan={2} valign="top" style={{ fontSize: '20px', textAlign: 'left', background: '#f9f9f9', color: '#222222', padding: '7px 15px 7px 15px', fontWeight: 500 }}>Experience</td>
                            </tr>
                            {renderExperiences()}
                          </tbody>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td style={{ padding: '0px 0px 30px 0px' }}>
                        <table width="100%" cellSpacing={0} cellPadding={0}>
                          <tbody>
                            <tr>
                              <td colSpan={4} valign="top" style={{ fontSize: '20px', textAlign: 'left', background: '#f9f9f9', color: '#222222', padding: '7px 15px 7px 15px', fontWeight: 500 }}>Skills</td>
                            </tr>
                            {renderSkill()}
                          </tbody>
                        </table>
                      </td>
                    </tr>
                    <tr >
                      <td style={{ padding: '0px 0px 30px 0px' }}>
                        <table width="100%" cellSpacing={0} cellPadding={0}>
                          <tbody><tr>
                            <td colSpan={4} valign="top" style={{ fontSize: '20px', textAlign: 'left', background: '#f9f9f9', color: '#222222', padding: '7px 15px 7px 15px', fontWeight: 500 }}>Training</td>
                          </tr>
                            {renderTraining()}
                            {/* <tr>
                              <td style={{ fontSize: '13px', textAlign: 'left', color: '#2d2d2d', padding: '15px 0px 10px 0px', fontWeight: 600, lineHeight: '20px' }}> Photoshop</td>
                              <td style={{ fontSize: '13px', textAlign: 'left', color: '#2d2d2d', padding: '15px 0px 10px 0px', fontWeight: 600, lineHeight: '20px' }}> InDesign</td>
                              <td style={{ fontSize: '13px', textAlign: 'left', color: '#2d2d2d', padding: '15px 0px 10px 0px', fontWeight: 600, lineHeight: '20px' }}> Illustrater</td>
                              <td style={{ fontSize: '13px', textAlign: 'left', color: '#2d2d2d', padding: '15px 0px 10px 0px', fontWeight: 600, lineHeight: '20px' }}>&nbsp; </td>
                            </tr> */}
                          </tbody></table>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <table width="100%" cellSpacing={0} cellPadding={0}>
                          <tbody>
                            <tr>
                              <td colSpan={3} valign="top" style={{ fontSize: '20px', textAlign: 'left', background: '#f9f9f9', color: '#222222', padding: '7px 15px 7px 15px', fontWeight: 500 }}>Education</td>

                            </tr>

                            {resumeInfo.educationQualificationInfo != undefined &&
                              resumeInfo.educationQualificationInfo.filter((f: any) => f.isActive == true).map((e: any, i: any) =>
                                <tr>
                                  <td style={{ fontSize: '14px', textAlign: 'left', color: '#2d2d2d', padding: '15x 0px 10px 0px', fontWeight: 600, lineHeight: '20px' }}><img src={require("./../../../../images/education_icon.png")} width="33" height="24" /> {e.course}</td>
                                  <td style={{ fontSize: '14px', textAlign: 'left', color: '#2d2d2d', padding: '15x 0px 10px 0px', fontWeight: 600, lineHeight: '20px' }}><img src={require("./../../../../images/univercity_icon.png")} />{e.university}</td>
                                  <td style={{ fontSize: '13px', textAlign: 'right', color: '#2d2d2d', padding: '15px 0px 10px 0px', fontWeight: 600, lineHeight: '20px' }}>{moment(e.dateTo, "DD/MM/YYYY").year()}</td>
                                </tr>
                              )}
                          </tbody>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>
                        <table width="100%" cellSpacing={0} cellPadding={0}>
                          <tbody>
                            <tr>
                              <td colSpan={2} valign="top" style={{ fontSize: '20px', textAlign: 'left', background: '#f9f9f9', color: '#222222', padding: '7px 15px 7px 15px', fontWeight: 500 }}>Declaration</td>
                            </tr>
                            <tr>
                              <td colSpan={2} style={{ fontSize: '14px', textAlign: 'left', color: '#2d2d2d', padding: '15px 0px 10px 0px', fontWeight: 500, lineHeight: '20px' }}>I hereby declare that the information provided is true and correct. I also understand that any willful   dishonesty may render for refusal of this application.</td>
                            </tr>
                            <tr>
                              <td colSpan={2} style={{ fontSize: '14px', textAlign: 'left', color: '#2d2d2d', padding: '15px 0px 10px 0px', fontWeight: 500, lineHeight: '20px' }}>&nbsp;</td>
                            </tr>
                            <tr>
                              <td style={{ fontSize: '14px', textAlign: 'left', color: '#2d2d2d', padding: '0px 0px 10px 0px', fontWeight: 500, lineHeight: '20px' }}>Date : {moment().format("DD/MM/YYYY")}</td>
                              <td style={{ fontSize: '14px', textAlign: 'right', color: '#2d2d2d', padding: '0px 0px 10px 0px', fontWeight: 500, lineHeight: '20px' }}>Signature</td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td style={{ background: '#4b56c0', height: '50px' }} />
              <td valign="top" style={{ fontSize: '14px', textAlign: 'left', color: '#222222', padding: '30px', fontWeight: 500 }} />
            </tr>
          </tbody></table>
      </div>
    </React.Fragment>
  );

};
export default PreviewTemplate;