import React, { useState, useEffect } from "react";
import ResumeComponent from "./ResumeComponent";
import { useMyProfileContext } from "../../../../action/MyProfileAction";
import { getAllResumeCandidateMap } from "../../../../apis/resumebuilder";


interface IResumeContainerProps {
  digiLockerVew: any
}

interface IResumeContainerState { }

const initialState = {};

const ResumeContainer: React.FC<IResumeContainerProps> = (props) => {
  const [resumeContainerState, setResumeContainerState] = React.useState<IResumeContainerState>(
    initialState
  );

  const myProfileContext = useMyProfileContext();
  const { loggedUserId } = myProfileContext;
  const candidateId = loggedUserId;

  const [candidateResumes, setCandidateResumes] = useState({});

  useEffect(() => {
    if (candidateId) {
      getAllResumeCandidateMap(
        {
          "CandidateId": candidateId,
          "Page": 1,
          "PageSize": 10,
          "SearchTerm": "",
          "SortOrder": "",
          "ShowInactive": false
        }
      ).then((res) => {
        //console.log(res.data);
        setCandidateResumes(res.data);

      }).catch((err) => {
        console.log(err);
      });
    }
  }, [candidateId]);

  return (
    <>
      <ResumeComponent candidateResumes={candidateResumes} candidateId={candidateId} setCandidateResumes={setCandidateResumes} digiLockerVew={props.digiLockerVew} />
    </>
  );
};
export default ResumeContainer;
