import React from "react";
import IndexFooterContainer from "../index/indexFooter/IndexFooterContainer";
import IndexHeaderContainer from "../index/indexHeader/IndexHeaderContainer";
import AboutUsComponent from "./AboutUsComponent";

interface IAboutUsContainerProps { }

interface IAboutUsContainerState { }

const initialState = {};

const AboutUsContainer: React.FC<IAboutUsContainerProps> = (props) => {
  const [AboutUsContainerState, setAboutUsContainerState] = React.useState<IAboutUsContainerState>(
    initialState
  );

  return (
    <>
    {/* <IndexHeaderContainer /> */}
    <AboutUsComponent/>
    <IndexFooterContainer />
    </>
  );
};
export default AboutUsContainer;
