import React, { useState, useContext } from "react";
import { useForm } from "react-hook-form";
import { Link, withRouter } from "react-router-dom";
import { ErrorMessage } from '@hookform/error-message';
import { GlobalSpinnerContext } from "./../../context/GlobalSpinner";
import { forgotPasswordLink } from "./../../apis/misc";
import { reactHookFormServerErrors } from "./../../utils/utility";
import { toast, ToastContainer } from 'react-toastify';

interface IForgotPasswordOtpState {
  emailOrMobile: string
}

function ForgotPasswordOtp(props: any) {

  const defaultValues = {
    emailOrMobile: ""
  };

  const { register, handleSubmit, watch, errors, setValue, getValues, setError, trigger, clearErrors } = useForm<IForgotPasswordOtpState>({
    defaultValues
  });

  const globalSpinner = useContext(GlobalSpinnerContext);

  const [isEmail, setIsEmail] = useState<Boolean>(true);

  let val = {};

  if (isEmail) {
    val = {
      required: "Email is required"
    }
  } else {
    val = {
      required: "Mobile Number is required"
    }
  }

  const onSubmit = (data: any) => {

    if (isEmail) {
      if (/\S+@\S+\.\S+/.test(data["emailOrMobile"])) {
        console.log("ok", data);

        globalSpinner.showSpinner();

        forgotPasswordLink({
          "Email": data["emailOrMobile"]
        }).then((res: any) => {

          console.log(res.data);

          globalSpinner.hideSpinner();
          toast.success("Forgot link has been send to email " + data["emailOrMobile"]);
        }).catch((err: any) => {

          console.log(err);
          toast.error(err.toString());
          globalSpinner.hideSpinner();

        });

      } else {
        setError("emailOrMobile", { type: "manual", message: "Shold be a valid email" });
      }
    } else {
      console.log("ok", data);

      globalSpinner.showSpinner();

      forgotPasswordLink({
        "Email": data["emailOrMobile"]
      }).then((res: any) => {

        console.log(res.data);

        globalSpinner.hideSpinner();
        toast.success("Forgot link has been send to email " + data["emailOrMobile"]);
      }).catch((err: any) => {

        console.log(err);
        toast.error(err.toString());
        globalSpinner.hideSpinner();

      });

    }

  };

  if (/\S+@\S+\.\S+/.test(watch("emailOrMobile")) && "emailOrMobile" in errors && errors.emailOrMobile!.type == "manual") {
    clearErrors("emailOrMobile");
  }


  const resend = () => {
    if (isEmail) {
      if (/\S+@\S+\.\S+/.test(watch("emailOrMobile"))) {
        console.log("ok", watch("emailOrMobile"));

        globalSpinner.showSpinner();

        forgotPasswordLink({
          "Email": watch("emailOrMobile")
        }).then((res: any) => {

          console.log(res.data);

          globalSpinner.hideSpinner();
          toast.success("Forgot link has been send to email " + watch("emailOrMobile"));
        }).catch((err: any) => {

          console.log(err);
          toast.error(err.toString());
          globalSpinner.hideSpinner();

        });

      } else {
        setError("emailOrMobile", { type: "manual", message: "Shold be a valid email" });
      }
    } else {
      console.log("ok", watch("emailOrMobile"));

      globalSpinner.showSpinner();

      forgotPasswordLink({
        "Email": watch("emailOrMobile")
      }).then((res: any) => {

        console.log(res.data);

        globalSpinner.hideSpinner();
        toast.success("Forgot link has been send to email " + watch("emailOrMobile"));
      }).catch((err: any) => {

        console.log(err);
        toast.error(err.toString());
        globalSpinner.hideSpinner();

      });

    }
  }

  return (
    <div className=" container">
      <ToastContainer />
      <div className="row">
        <div className="col-sm-6 desk">
          <div className="login_img"><img src={require("./../../images/forgot_img.jpg")} className="img-responsive" /></div>
        </div>
        <div className="col-sm-6 col-lg-5 col-lg-offset-1">
          <form className="needs-validation" onSubmit={handleSubmit(onSubmit)} noValidate>
            <div className="registration_forms registration_sec">
              <div className="sub_heading">
                <h2>Forgot Password</h2>
              </div>
              <div className="already_login2">Send OTP on</div>
              <div className="forgot_otp_btn">
                <div className="m_t_20"><button onClick={() => { setIsEmail(!isEmail); }} type="button" className={isEmail ? "btn btn-success sig_but" : "btn btn-success create_but"}>Email</button></div>
                <div className="m_t_20"><button onClick={() => { setIsEmail(!isEmail); }} type="button" className={isEmail ? "btn btn-success create_but" : "btn btn-success sig_but"}>Mobile</button></div>
              </div>
              <div className="form-group">
                <input type="text" className="form-control form-control-n" placeholder="Type here" name="emailOrMobile" ref={register(val)} />
                <ErrorMessage errors={errors} name="emailOrMobile" render={({ message }) => <div className="register_validation">{message}</div>} />
              </div>
              <div className="already_login1">Didn't get the code? <a className="_cursor-pointer" onClick={() => { resend() }}>Resend again.</a></div>
              <div className="m_t_20"><button type="submit" className="btn btn-success reg_but">Send</button></div>
            </div>
            <div className="clearfix" />
          </form>
        </div>
      </div>
    </div>
  );
}

export default withRouter(ForgotPasswordOtp);
