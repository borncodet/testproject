import React, { useState, useContext } from "react";
import { useForm } from "react-hook-form";
import { Link, withRouter } from "react-router-dom";
import { ErrorMessage } from "@hookform/error-message";
import AuthService from "../../services/AuthService";
import {
  GoogleLoginButton,
  FacebookLoginButton,
  TwitterLoginButton,
  LinkedinLoginButton,
} from "./components/SocialLoginButton";
import { GlobalSpinnerContext } from "./../../context/GlobalSpinner";
import { toast, ToastContainer } from "react-toastify";
import { socialLoginHandler } from "./../../apis/misc";

interface ILoginState {
  email: string;
  password: string;
  rememberMe: boolean;
}

function Login(props: any) {
  const defaultValues = {
    email: "",
    password: "",
    rememberMe: true,
  };

  const {
    register,
    handleSubmit,
    watch,
    errors,
    setValue,
    getValues,
  } = useForm<ILoginState>({
    defaultValues,
  });

  const [showPassword, setShowPassword] = useState<Boolean>(false);

  const globalSpinner = useContext(GlobalSpinnerContext);

  const onSubmit = async (data: any) => {
    globalSpinner.showSpinner();
    try {
      // const user = await AuthService.login(data.email, data.password, data.rememberMe);
      const user = await AuthService.login(data.email, data.password, true);
      if (user) {
        console.log(1, user);
        console.log("-----------------logged user----------: "+user.roles[0]);
        if(user.roles[0] == "Candidate")
        {
          props.history.push("/candidate");
        }
        else if(user.roles[0] == "Vendor")
        {
          props.history.push("/vendor");
        }
       
        globalSpinner.hideSpinner();
      } else {
        globalSpinner.hideSpinner();
      }
    } catch (error) {
      console.log(error);
      if (error.response.status == 400) {
        toast.error("Username/Password incorrect.");
      } else {
        toast.error("Error occurred while login.");
      }

      globalSpinner.hideSpinner();
    }
  };

  const handleFbLogin = (data: any) => {
    console.log(data);

    const code = JSON.parse(atob(data.signedRequest.split(".")[1])).code;

    console.log(code);

    // socialLoginHandler({
    //   code: JSON.parse(atob(data.signedRequest.split(".")[1])).code + "",
    //   accessToken: data.accessToken,
    //   social: "fb"
    // }).then((res) => {
    //   console.log(res.data);
    // }).catch((err) => {
    //   console.log(err);
    // });
  };

  return (
    <div className=" container">
      <ToastContainer />
      <div className="row">
        <div className="col-sm-6 desk">
          <div className="login_img">
            <img
              src={require("./../../images/login_img.jpg")}
              className="img-responsive"
            />
          </div>
        </div>
        <div className="col-sm-6 col-lg-5 col-lg-offset-1">
          <div className="login_forms registration_sec">
            <div className="sub_heading">
              <h1>Welcome</h1>
              <p>
                To keep connected with us please login with your email address
                and password.
              </p>
            </div>
            <form
              className="needs-validation"
              onSubmit={handleSubmit(onSubmit)}
              noValidate
            >
              <div className="login_form_sec">
                <div className="form-group">
                  <div className="login_icon">
                    <img src={require("./../../images/mailicon.jpg")} />
                  </div>
                  <div className="login_f">
                    <label>Email Address</label>
                    <input
                      type="text"
                      className="form-control input-name"
                      placeholder="Username"
                      name="email"
                      ref={register({
                        required: "Email is required",
                        pattern: {
                          value: /\S+@\S+\.\S+/,
                          message: "Shold be a valid email"
                        }
                      })}
                    />
                    {/* <div className="login_validation">Error</div> */}
                    <ErrorMessage
                      errors={errors}
                      name="email"
                      render={({ message }) => (
                        <div className="login_validation">{message}</div>
                      )}
                    />
                  </div>
                </div>
                <div className="login_br" />
                <div className="form-group">
                  <div className="login_icon">
                    <img src={require("./../../images/lock_icon.jpg")} />
                  </div>
                  <div className="login_f">
                    <div className="fons_lg">
                      <label>Password</label>
                      <input
                        type={showPassword ? "text" : "password"}
                        className="form-control validate input-pwd"
                        placeholder="Password"
                        name="password"
                        ref={register({
                          required: "Password is required"
                        })}
                      />
                      <span
                        onClick={(event: any) => {
                          setShowPassword(!showPassword);
                        }}
                        className={
                          showPassword
                            ? "fa fa-fw fa-eye field-icon toggle-password"
                            : "fa fa-fw fa-eye-slash field-icon toggle-password"
                        }
                      />
                      {/* <div className="login_validation">Error</div> */}
                      <ErrorMessage
                        errors={errors}
                        name="password"
                        render={({ message }) => (
                          <div className="login_validation">{message}</div>
                        )}
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="form-group">
                <div className="row m_t_25">
                  <div className="col-xs-6">
                    <input
                      id="checkbox1"
                      type="checkbox"
                      name="rememberMe"
                      ref={register}
                    />
                    <label htmlFor="checkbox1">
                      <span /> Remember Me{" "}
                    </label>
                  </div>
                  <div className="col-xs-6">
                    <div className="forgot">
                      <Link to="/forgot-password-otp">Forgot Password</Link>
                    </div>
                  </div>
                  <div className="col-xs-6">
                    <div className="forgot">
                      <Link to="/forgot-password-otp">Forgot Password</Link>
                    </div>
                  </div>
                </div>
              </div>
              {/* <Link  to="/candidate"> */}
              <button type="submit" className="btn sig_but">
                Login Now
              </button>
              {/* </Link> */}
              <a href="index.html">
                <button
                  type="button"
                  className="btn create_but"
                  onClick={event => {
                    event.preventDefault();
                    props.history.push("/registration");
                  }}
                >
                  Create An Acount
                </button>
              </a>
            </form>
            <div className="clearfix" />
            <div className="already_login">Or connect with </div>
            <div className="sign_with">
              <GoogleLoginButton />
              <FacebookLoginButton handleLogin={handleFbLogin} />
              <TwitterLoginButton />
              <LinkedinLoginButton />
            </div>
            <div className="clearfix" />
          </div>
        </div>
      </div>
    </div>
  );
}

export default withRouter(Login);
