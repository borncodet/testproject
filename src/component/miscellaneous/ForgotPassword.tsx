import React, { useState, useContext } from "react";
import { useForm } from "react-hook-form";
import { Link, withRouter } from "react-router-dom";
import { ErrorMessage } from "@hookform/error-message";
import { resetPassword } from "./../../apis/misc";
import { GlobalSpinnerContext } from "./../../context/GlobalSpinner";
import { reactHookFormServerErrors } from "./../../utils/utility";
import AuthService from "../../services/AuthService";
import { toast, ToastContainer } from "react-toastify";

interface IForgotPasswordState {
  password: string;
  passwordConfirm: string;
}

function ForgotPassword(props: any) {
  const defaultValues = {
    password: "",
    passwordConfirm: "",
  };

  const {
    register,
    handleSubmit,
    watch,
    errors,
    setValue,
    getValues,
  } = useForm<IForgotPasswordState>({
    defaultValues,
  });

  const [showPassword, setShowPassword] = useState<Boolean>(false);
  const [showPassword2, setShowPassword2] = useState<Boolean>(false);

  const query = new URLSearchParams(props.location.search);

  console.log(query.get("email"));
  console.log(query.get("token"));

  const globalSpinner = useContext(GlobalSpinnerContext);

  const onSubmit = (data: any) => {
    console.log(data);
    globalSpinner.showSpinner();

    const _data = {
      Email: query.get("email"),
      Password: data["password"],
      ConfirmPassword: data["passwordConfirm"],
      Token: query.get("token"),
    };

    resetPassword(_data)
      .then((res: any) => {
        console.log(res.data);

        globalSpinner.hideSpinner();
        toast.error("Password reset done.");
      })
      .catch((err: any) => {
        console.log(err);
        globalSpinner.hideSpinner();
        toast.error("Error occurred while reset password.");
      });
  };

  return (
    <div className=" container">
      <ToastContainer />
      <div className="row">
        <div className="col-sm-6 desk">
          <div className="login_img">
            <img
              src={require("./../../images/forgot_img.jpg")}
              className="img-responsive"
            />
          </div>
        </div>
        <div className="col-sm-6 col-lg-5 col-lg-offset-1">
          <form
            className="needs-validation"
            onSubmit={handleSubmit(onSubmit)}
            noValidate
          >
            <div className="registration_forms registration_sec">
              <div className="sub_heading">
                <h2>Create New Password</h2>
              </div>
              {/*<div className="form-group m_t_30">
                <div className="fons_lg">
                  <input type={showPassword ? "text" : "password"} name="password" ref={register({
                    required: "Password is required",
                    // minLength: {
                    //   value: 9,
                    //   message: "Password contain atleast 9 characters"
                    // },
                    maxLength: {
                      value: 20,
                      message: "Shold not be grater than 20 char"
                    },
                    validate: function (value) {
                      let re = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{9,}$/;
                      return re.test(value) ? undefined : "Mush have 9 char, capital letter, small letter and special char";
                    }
                  })} className="password-field-title-without-tooltip form-control input-pwd" placeholder="Password" required />
                  <span onClick={(event: any) => { setShowPassword(!showPassword); }} className={showPassword ? "fa fa-fw fa-eye field-icon toggle-password" : "fa fa-fw fa-eye-slash field-icon toggle-password"} />
                  <ErrorMessage errors={errors} name="password" render={({ message }) => <div className="register_validation">{message}</div>} />
                </div>
                <div className="register_icon"><img src={require("./../../images/password_r.png")} /></div>
                </div>*/}
              <div className="form-group">
                <div className="fons_lg">
                  <input
                    type={showPassword ? "text" : "password"}
                    name="password"
                    ref={register({
                      required: "Password is required",
                      minLength: {
                        value: 9,
                        message:
                          "Password should be 9 digits long with one uppercase and one lowercase letter, a number and a special character",
                      },
                      maxLength: {
                        value: 20,
                        message: "Should not be greater than 20 characters",
                      },
                      validate: function (value) {
                        let re = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{9,}$/;
                        return re.test(value)
                          ? undefined
                          : "Password should be 9 digit long which inludes capital letter, small letter and special characters";
                      },
                    })}
                    className="password-field-title-without-tooltip form-control input-pwd"
                    placeholder="Password"
                    required
                  />
                  <span
                    onClick={(event: any) => {
                      setShowPassword(!showPassword);
                    }}
                    className={
                      showPassword
                        ? "fa fa-fw fa-eye field-icon toggle-password"
                        : "fa fa-fw fa-eye-slash field-icon toggle-password"
                    }
                  />
                </div>
                <div className="register_icon">
                  <img src={require("./../../images/password_r.png")} />
                </div>
                <ErrorMessage
                  errors={errors}
                  name="password"
                  render={({ message }) => (
                    <div className="register_validation">{message}</div>
                  )}
                />
              </div>
              <div className="form-group">
                <div className="fons_lg">
                  <input
                    type={showPassword2 ? "text" : "password"}
                    name="passwordConfirm"
                    ref={register({
                      required: "Confirm password is required",
                      validate: (value) =>
                        value === watch("password")
                          ? undefined
                          : "Should be same as password",
                    })}
                    className="form-control validate input-pwd"
                    placeholder="Re-enter Password"
                    required
                  />
                  <span
                    onClick={(event: any) => {
                      setShowPassword2(!showPassword2);
                    }}
                    className={
                      showPassword2
                        ? "fa fa-fw fa-eye field-icon toggle-password"
                        : "fa fa-fw fa-eye-slash field-icon toggle-password"
                    }
                  />
                </div>
                <div className="register_icon">
                  <img src={require("./../../images/password_r.png")} />
                </div>
                <ErrorMessage
                  errors={errors}
                  name="passwordConfirm"
                  render={({ message }) => (
                    <div className="register_validation">{message}</div>
                  )}
                />
              </div>
              <div className="clearfix" />
              <div className="already_login1">
                Note: Password should 9 digit long
              </div>
              <div className="m_t_20">
                <button type="submit" className="btn btn-success reg_but">
                  Reset Password
                </button>
              </div>
            </div>
            <div className="clearfix" />
          </form>
        </div>
      </div>
    </div>
  );
}

export default withRouter(ForgotPassword);
