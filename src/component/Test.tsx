import * as React from "react";
import { useForm, FieldErrors } from "react-hook-form";
import { ErrorMessage } from "@hookform/error-message";

type ErrorSummaryProps<T> = {
  errors: FieldErrors<T>;
};
function ErrorSummary<T>({ errors }: ErrorSummaryProps<T>) {
  if (Object.keys(errors).length === 0) {
    return null;
  }
  return (
    <div className="error-summary">
      {Object.keys(errors).map((fieldName) => (
        <ErrorMessage
          errors={errors}
          name={fieldName as any}
          as="div"
          key={fieldName}
        />
      ))}
    </div>
  );
}

type postDataResult<T> = {
  success: boolean;
  errors?: { [P in keyof T]?: string[] };
};

const postData = ({ name }: FormData): Promise<postDataResult<FormData>> => {
  return new Promise((resolve) => {
    setTimeout(() => {
      if (name === "Fred") {
        resolve({
          success: false,
          errors: { name: ["The name must be unique"] }
        });
      } else {
        resolve({ success: true });
      }
    }, 100);
  });
};

type FormData = {
  name: string;
};

function addServerErrors<T>(
  errors: { [P in keyof T]?: string[] },
  setError: (
    fieldName: keyof T,
    error: { type: string; message: string }
  ) => void
) {
  return Object.keys(errors).forEach((key) => {
    setError(key as keyof T, {
      type: "server",
      message: errors[key as keyof T]!.join(". ")
    });
  });
}

export default function App() {
  const { register, handleSubmit, errors, setError } = useForm<FormData>();
  const submitForm = async (data: FormData) => {
    const result = await postData(data);
    if (!result.success && result.errors) {
      addServerErrors(result.errors, setError);
    }
  };

  return (
    <div className="app">
      <form onSubmit={handleSubmit(submitForm)}>
        <input
          ref={register({
            required: { value: true, message: "You must enter your name" }
          })}
          type="text"
          name="name"
          placeholder="Enter your name"
        />
        <ErrorSummary errors={errors} />
        <button type="submit" >Register</button>
      </form>
    </div>
  );
}
