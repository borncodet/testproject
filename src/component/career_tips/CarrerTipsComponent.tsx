import React from "react";
import { Link, withRouter } from "react-router-dom";
import { useHistory } from 'react-router-dom';

interface ICareerTipsComponentProps { }

interface ICareerTipsComponentState { }

const initialState = {};

const CareerTipsComponent: React.FC<ICareerTipsComponentProps> = (props) => {
  const history = useHistory();
  const [startUpState, setCareerTipsComponentState] = React.useState<ICareerTipsComponentState>(
    initialState
  );

  return (
    <>
    {/* Header Start */}

    <div className="header header1"> 
        <header> 
          <div className=" container">
            <div className="row"> 
              <div className="col-sm-2">
                <div className=" logo">  <a
                            onClick={() => {
                              history.push("/");
                            }}><img 
                src={require("../../images/logo.png")}
                // src={require("../../images/logo.png" 
                className="img-responsive" alt="" /> </a></div> 
              </div> 
              <div className="col-sm-10"> 
                <div className="bs-example">
                  <nav role="navigation" className="navbar navbar-default navbar-static" id="navbar-example">
                    <div className="navbar-header">
                      <button data-target=".bs-example-js-navbar-collapse" data-toggle="collapse" type="button" className="navbar-toggle"> <span className="sr-only">Toggle navigation</span> <span className="icon-bar" /> <span className="icon-bar" /> <span className="icon-bar" /> </button>
                      <div className="collapse navbar-collapse bs-example-js-navbar-collapse">
                        <ul className="nav navbar-nav">  
                        <li><Link to='/'> Home</Link> </li>  
                          <li> <a href="#">Career Developer</a></li>
                          <li> <a href="#">Client </a></li>
                          <li> <a href="#">Vendor</a> </li>  
                          <li><Link to='/help'>Help</Link></li>
                          <li><Link to='/about_us'>About Us</Link></li>
                          <li><Link to='/contact_us'>Contact Us</Link></li> 
                        </ul> 
                      </div> 
                    </div> 
                  </nav> 
                </div> 
              </div>
            </div>
          </div>
        </header>
      </div>
{/* Header ends */}

{/* Sub baneer start */}

<div className="sub_banner">  
        <div className="post_career_tip"><a href="#" data-target="#post_career" data-toggle="modal">Post a Career Tip</a></div>
        <div className="desk"><img 
         src={require("../../images/careertips_banner.jpg")}
        // src="images/careertips_banner.jpg"
         className=" img-responsive" /></div>
        <div className=" mob"><img 
         src={require("../../images/careertips_banner_mob.jpg")}
        // src="images/careertips_banner_mob.jpg" 
        className=" img-responsive" /></div>
        <div className="career_banner">
          <div className=" container">
            <div className="row">     
              <div className="col-sm-6 col-lg-4">  
                <div className="career_banner_heading">Get best career advices here </div>  
                <div className="career_banner_con">Career tips are secret success stories of Experts outperforming in their career. You may step up onto their footprints to make your professional journey successful and memorable one.</div>
              </div>
            </div>
          </div> 
        </div>  
        <div className="clearfix" /> 
      </div>
{/* Sub banner ends */}

{/* model start */}
<div id="post_career" className="modal fade none-border" style={{display: 'none'}}>
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <button aria-hidden="true" data-dismiss="modal" className="close" type="button">×</button>
              <h4 className="modal-title">Post a Career Tip </h4>
            </div>
            <div className="modal-body"> 
              <div className="form-group">
                <label htmlFor="email">Name</label>
                <input type="text" className="form-control " placeholder="Enter Your Name" />
              </div>
              <div className="form-group">
                <label htmlFor="email">Email</label>
                <input type="text" className="form-control " placeholder="Enter Your Email" />
              </div>
              <div className="form-group">
                <label htmlFor="email">Contact</label>
                <input type="text" className="form-control " placeholder="Enter Your Contact" />
              </div>
              <div className="form-group">
                <label htmlFor="email">Category</label>
                <span className="select-wrapper-sec"><select name="timepass" className="custom-select-sec">
                    <option value="">Select Category</option>
                    <option value="">2018</option>  
                    <option value="">2019</option> 
                  </select><span className="holder">Select Category</span></span> 
              </div>
              <div className="form-group">
                <div className="upload_box"><input id="uploadFile" placeholder="Upload Prescription " 
                disabled className="form-control upload_box_form" /></div>
                <div className="fileUpload btn btn-primary">
                  <span>Upload</span>
                  <input id="uploadBtn" type="file" className="upload" />
                </div>
              </div> 
              <div className="clearfix" />
              <div className="form-group">
                <label htmlFor="email">Description</label>
                <textarea 
                name="" 
                // cols rows 
                className="form-control form-contro11" placeholder="Type here" defaultValue={""} />
              </div>
            </div>
            <div className="modal-footer  m-t-30">  
              <button className="btn btn-success save-event waves-effect waves-light" type="button">Save</button>
              <button data-dismiss="modal" className="btn btn-default waves-effect" type="button">Cancel</button>
            </div>
            <div className="clearfix" />
          </div>
        </div>
      </div>
{/* model end */}

<div className="career_tips_sub">   
        <a href="career_tips.html">
          <div className=" container">
            <div className="row">     
              <div className="col-sm-12 col-lg-12">  
                <h1>Users Featured Career Tips</h1>   
              </div> 
            </div>
          </div>  
        </a>       
      </div>

      {/* Carrer Tip Categories start */}
      <div className="career_categories">
        <div className="container">
          <div className="row"> 
            <h1>Categories</h1>
            <p>Select any one category to learn more about it. </p>  
            <div className="col-sm-3">
              <Link to='/career_tips_category'>
                <div className="career_categories_sec"> 
                  <div className="career_tips_icon"><img src={require("../../images/general_icon.png")} /></div>
                  <div className="career_tips_subcon">General </div> 
                </div>
              </Link>
            </div>
            <div className="col-sm-3">
              <a href="#">
                <div className="career_categories_sec">
                  <div className="career_tips_icon"><img src={require("../../images/training_icon.png")} /></div>
                  <div className="career_tips_subcon">Training </div>
                </div>
              </a>
            </div>
            <div className="col-sm-3">
              <a href="career_tips_categories.html">
                <div className="career_categories_sec">
                  <div className="career_tips_icon"><img src={require("../../images/job_hunt_icon.png")} /></div>
                  <div className="career_tips_subcon">Job hunt </div>
                </div>
              </a>
            </div>
            <div className="col-sm-3">
              <a href="career_tips_categories.html">
                <div className="career_categories_sec">
                  <div className="career_tips_icon"><img src={require("../../images/profile_icon.png")} /></div>
                  <div className="career_tips_subcon">Profile </div>
                </div>
              </a>
            </div>
            <div className="col-sm-3">
              <a href="career_tips_categories.html">
                <div className="career_categories_sec">
                  <div className="career_tips_icon"><img src={require("../../images/resume_icon.png")} /></div>
                  <div className="career_tips_subcon">Resume and  Cover letter </div>
                </div>
              </a>
            </div>
            <div className="col-sm-3">
              <a href="career_tips_categories.html">
                <div className="career_categories_sec">
                  <div className="career_tips_icon"><img src={require("../../images/communication_icon.png")} /></div>
                  <div className="career_tips_subcon">Communication </div>
                </div>
              </a>
            </div>
            <div className="col-sm-3">
              <a href="career_tips_categories.html">
                <div className="career_categories_sec">
                  <div className="career_tips_icon"><img src={require("../../images/interview_icon.png")} /></div>
                  <div className="career_tips_subcon">Interview </div>
                </div>
              </a>
            </div>
            <div className="col-sm-3">
              <a href="career_tips_categories.html">
                <div className="career_categories_sec">
                  <div className="career_tips_icon"><img src={require("../../images/telephonic_icon.png")} /></div>
                  <div className="career_tips_subcon">Telephonic Skills </div>
                </div>
              </a>
            </div>
            <div className="col-sm-3">
              <a href="career_tips_categories.html">
                <div className="career_categories_sec">
                  <div className="career_tips_icon"><img src={require("../../images/personality_icon.png")} /></div>
                  <div className="career_tips_subcon">Personality Skills </div>
                </div>
              </a>
            </div>
            <div className="col-sm-3">
              <a href="career_tips_categories.html">
                <div className="career_categories_sec">
                  <div className="career_tips_icon"><img src={require("../../images/workfile_icon.png")} /></div>
                  <div className="career_tips_subcon">Work life  management </div>
                </div>
              </a>
            </div>
            <div className="col-sm-3">
              <a href="career_tips_categories.html">
                <div className="career_categories_sec">
                  <div className="career_tips_icon"><img src={require("../../images/job_shifting_icon.png")} /></div>
                  <div className="career_tips_subcon">Job shifting</div>
                </div>
              </a>
            </div>
            <div className="col-sm-3">
              <a href="career_tips_categories.html">
                <div className="career_categories_sec">
                  <div className="career_tips_icon"><img src={require("../../images/role_shifting_icon.png")} /></div>
                  <div className="career_tips_subcon">Role switching </div>
                </div>
              </a>
            </div>
            <div className="col-sm-3">
              <a href="career_tips_categories.html">
                <div className="career_categories_sec">
                  <div className="career_tips_icon"><img src={require("../../images/taking_break_icon.png")} /></div>
                  <div className="career_tips_subcon">Taking a brake </div>
                </div>
              </a>
            </div>
            <div className="col-sm-3">
              <a href="career_tips_categories.html">
                <div className="career_categories_sec">
                  <div className="career_tips_icon"><img src={require("../../images/exelling_icon.png")} /></div>
                  <div className="career_tips_subcon">Excelling in career </div>
                </div>
              </a>
            </div>
            <div className="col-sm-3">
              <a href="career_tips_categories.html">
                <div className="career_categories_sec">
                  <div className="career_tips_icon"><img src={require("../../images/postive_attitude_icon.png")} /></div>
                  <div className="career_tips_subcon">Positive attitude </div>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
      {/* Carrer Tip Categories end */}

      {/* Achievement start  */}
      <div className="achievements">   
        <div className=" container">
          <div className="row">     
            <div className="col-sm-12 ">  
              <h1>Blog</h1>
            </div>
            <div className="col-sm-12 ">
              <div id="owl-example" className="owl-carousel owl-theme" style={{opacity: 1, display: 'block'}}>
                <div className="owl-wrapper-outer"><div className="owl-wrapper" style={{width: '3660px', left: '0px', display: 'block'}}><div className="owl-item" style={{width: '305px'}}><div className="item">
                        <div className="blogs_sec">
                          <div className="achievements_sec">
                            <div className="achieve_img"><img src={require("../../images/technology.jpg")} className="img-responsive" /></div>
                            <h2>Technology </h2>
                            <p>Lorem Ipsum is simply dummy text of the  printing and typesetting industry</p>
                            <div className="achieve_date"> <img src={require("../../images/achive_date.png")} /> June 27, 2020 </div>
                            <div className="achieve_more"><a href="#"><img src={require("../../images/achive_more.png")} /> Read More </a></div>
                          </div>
                        </div>
                      </div></div><div className="owl-item" style={{width: '305px'}}><div className="item">
                        <div className="blogs_sec">
                          <div className="achievements_sec">
                            <div className="achieve_img"><img src={require("../../images/ecnomy.jpg")} className="img-responsive" /></div>
                            <h2>Economy </h2>
                            <p>Lorem Ipsum is simply dummy text of the  printing and typesetting industry</p>
                            <div className="achieve_date"> <img src={require("../../images/achive_date.png")} /> June 27, 2020 </div>
                            <div className="achieve_more"><a href="#"><img src={require("../../images/achive_more.png")} /> Read More </a></div>
                          </div> 
                        </div>
                      </div></div><div className="owl-item" style={{width: '305px'}}><div className="item">
                        <div className="blogs_sec">
                          <div className="achievements_sec">
                            <div className="achieve_img"><img src={require("../../images/business.jpg")} className="img-responsive" /></div>
                            <h2>Technology </h2>
                            <p>Lorem Ipsum is simply dummy text of the  printing and typesetting industry</p>
                            <div className="achieve_date"> <img src={require("../../images/achive_date.png")} /> June 27, 2020 </div>
                            <div className="achieve_more"><a href="#"><img src={require("../../images/achive_more.png")} /> Read More </a></div>
                          </div> 
                        </div>
                      </div></div><div className="owl-item" style={{width: '305px'}}><div className="item">
                        <div className="blogs_sec">
                          <div className="achievements_sec">
                            <div className="achieve_img"><img src={require("../../images/technology.jpg")} className="img-responsive" /></div>
                            <h2>Technology </h2>
                            <p>Lorem Ipsum is simply dummy text of the  printing and typesetting industry</p>
                            <div className="achieve_date"> <img src={require("../../images/achive_date.png")} /> June 27, 2020 </div>
                            <div className="achieve_more"><a href="#"><img src={require("../../images/achive_more.png")} /> Read More </a></div>
                          </div>
                        </div>
                      </div></div><div className="owl-item" style={{width: '305px'}}><div className="item">
                        <div className="blogs_sec">
                          <div className="achievements_sec">
                            <div className="achieve_img"><img src={require("../../images/ecnomy.jpg")} className="img-responsive" /></div>
                            <h2>Economy </h2>
                            <p>Lorem Ipsum is simply dummy text of the  printing and typesetting industry</p>
                            <div className="achieve_date"> <img src={require("../../images/achive_date.png")} /> June 27, 2020 </div>
                            <div className="achieve_more"><a href="#"><img src={require("../../images/achive_more.png")} /> Read More </a></div>
                          </div> 
                        </div>
                      </div></div><div className="owl-item" style={{width: '305px'}}><div className="item">
                        <div className="blogs_sec">
                          <div className="achievements_sec">
                            <div className="achieve_img"><img src={require("../../images/business.jpg")} className="img-responsive" /></div>
                            <h2>Technology </h2>
                            <p>Lorem Ipsum is simply dummy text of the  printing and typesetting industry</p>
                            <div className="achieve_date"> <img src={require("../../images/achive_date.png")} /> June 27, 2020 </div>
                            <div className="achieve_more"><a href="#"><img src={require("../../images/achive_more.png")} /> Read More </a></div>
                          </div> 
                        </div>
                      </div></div></div></div>
                <div className="owl-controls clickable"><div className="owl-pagination"><div className="owl-page active"><span className="" /></div><div className="owl-page"><span className="" /></div></div></div></div>
            </div>
          </div> 
        </div>
      </div>
       {/* Achievement ends  */}
    </>
  );
};
export default withRouter(CareerTipsComponent);
