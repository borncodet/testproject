import React, { useState } from "react";
import { useMyProfileContext } from "../../../action/MyProfileVendorAction";
import { useUserListContext} from "../../../context/vendor/VendorMyProfile";
import AuthService from "../../../services/AuthService";

function VendorMessageUsers(props: any) {
 
  const userListContext = useUserListContext();

  const {
    userList,
    getUserList
  } = userListContext;

  const myProfileContext = useMyProfileContext();
  const { myProfile, loggedUserId, basicInfo } = myProfileContext;
  const authorizationToken = AuthService.accessToken;
  const user = AuthService.currentUser;

  const vendorId = loggedUserId;

  React.useEffect(() => {
    getUserList({
        Page: 1,
        PageSize: 10,
        SearchTerm: "",
        SortOrder: ""
      });
  },[vendorId]);

   console.log(loggedUserId,'vendorlist------------>',userList);

   const list = userList.data.map((item, index) => {
    return (
      <li><a href="#" className="active">
                              <div className="connect_icon"><div className="icon_clr1">A</div></div>
                              <div className="connect_con">
                                <div className="connect_con_name"> {item["userName"]}<span className="dot dot-busy" /></div>
                                <div className="connect_con_des">Admin - Jitmarine</div>
                              </div>
                              <div className="chat_time_sec">
                                <div className="chat_time">2 Min</div>
                                <div className="connect_con_noti">2</div>
                              </div>
                            </a></li>
    );
    });

   return (
    <React.Fragment>   
            <div className="col-sm-5 col-lg-4 p-r-0">
              <div className="panel panel-default panel_n">
                <div className="panel-body panel-body1">
                  <div className="connect_left">
                    <div>
                      <input type="email" className="form-control" placeholder="Search" />
                      <div className="search_icon"><i className="fa fa-search" aria-hidden="true" /></div>
                    </div>
                    <div className=" m-t-25">
                      <div className="message_chat_scroll" style={{ overflow: 'hidden', outline: 'none' }} tabIndex={0}>
                        <div className="connect_scroll">
                         
                          <ul>

                       {list}
        
                           
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>      
    </React.Fragment>
  );
};
export default VendorMessageUsers;
