import React, { useState, useEffect } from "react";
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import {
  getLoggedUserId,
  getMyProfileBasicInfo,
  getMyProfileSelectBoxList,
  getProfileProgressBarResult,
  useMyProfileContext,
  useMyProfileDispatcher,
} from "../../../action/MyProfileVendorAction";
import AuthService from "../../../services/AuthService";
import { useUserListContext, useMessageListContext} from "../../../context/vendor/VendorMyProfile";
import { isNullOrUndefined } from "util";


interface IVendorMessageComponentProps { }

interface IVendorMessageComponentState {
      // sendUserName: string;
      // message: string;
      // messages: string[];
      // hubConnection: null;
 }

 const defaultValues = {
      // sendUserName: "",
      // message: "",
      // messages: [],
      // hubConnection: null,
};

const VendorMessageComponent: React.FC<IVendorMessageComponentProps> = (props) => {
  
  const [ VendorMessageComponentState, 
          setVendorMessageComponentState
        ] = React.useState<IVendorMessageComponentState>(defaultValues);

    const authorizationToken = AuthService.accessToken;
    const [message, setMessage] = useState<string>('');
    const [messages, setMessages] = useState<string[]>([]);
    const [sendUserId, setSendUserId] = useState<string>();
    const [sendUserName, setSendUserName] = useState<string>();
    const [receiveUserId, setReceiveUserId] = useState<string>();
    const [receiveUserName, setReceiveUserName] = useState<string>();
    const [filter, setFilter] = useState<string>("");
    const [hubConnection, setHubConnection] = useState<HubConnection>();
    const [list, setList] = useState();
    const [DBMessageList, setDBMessageList] = useState();
    const [messagesList, setMessagesList] = useState();
    const [showing, setShowing] = useState(false);

    const myProfileDispatcher = useMyProfileDispatcher();
    const myProfileContext = useMyProfileContext();
    const {
    basicInfo,
    loggedUserId,
    myProfile,
    myProfileProgressBar,
    profileImage,
   } = myProfileContext;
   
  const userListContext = useUserListContext();
  const messageListContext = useMessageListContext();  

  const {
    userList,
    getUserList
  } = userListContext;

  const {
    messageList,
    getMessageList
  } = messageListContext;

   const user = AuthService.currentUser;
 
  console.log('getLoggedUserId-------------------',loggedUserId);
  console.log('basicInfo-------------------',basicInfo.fullName != undefined ? basicInfo.fullName : "");
  
               
  const vendorId = loggedUserId;

  React.useEffect(() => {
    getUserList({
        Page: 1,
        PageSize: 10,
        SearchTerm: "",
        SortOrder: ""
      });
  },[vendorId]);

 React.useEffect(() => {
    getMessageList({
        FromUserId:Number(sendUserId),
        ToUserId:Number(receiveUserId),
        Page: 1,
        PageSize: 10,
        SearchTerm: "",
        SortOrder: ""
      });
  },[sendUserId, receiveUserId]);

  React.useEffect(() => {
    setList(         
      userList.data.map((item, index) => {
      const lowercasedFilter = filter.toLowerCase();
      if(String(item["userName"]).toLowerCase().includes(lowercasedFilter)||filter==""){
              return (
                <li><a onClick={()=>handleReceiveUser(item["userId"],item["userName"])} className="active">
                                        <div className="connect_icon"><div className="icon_clr1">A</div></div>
                                        <div className="connect_con">
                                          <div className="connect_con_name"> {item["userName"]}<span className="dot dot-busy" /></div>
                                          <div className="connect_con_des">Admin - Jitmarine</div>
                                        </div>
                                        <div className="chat_time_sec">
                                          <div className="chat_time">2 Min</div>
                                          <div className="connect_con_noti">2</div>
                                        </div>
                                      </a></li>
              );
      }
    })       
    );
  },[filter,userList]);

  React.useEffect(() => {
    setDBMessageList(         
      messageList.data.map((item, index) => {
      if(item["fromUserId"]==sendUserId){
      return (
               <div className="chat_left">
               <div className="chat_icon1"><div className="icon_clr_rs1">A</div></div>
               <div className="chat_name">{basicInfo.fullName != undefined ? basicInfo.fullName : ""}<span> 2 Hours</span></div>
               <div className="clearfix" />
               <div className="chat_box_l" key={index}>{item["message"]}</div>
               </div>
           );
      }
      else{
        return (
                <div className="chat_right">
                <div className="chat_icon2"><div className="icon_clr_rs2">B</div></div>
                <div className="chat_name1"><span>2 Hours</span> {receiveUserName}</div>
                <div className="clearfix" />
                <div className="chat_box_r" key={index}>{item["message"]}</div>
                </div>
           );
      }
    })       
    );
  },[receiveUserId,messageList]);


 React.useEffect(() => {
    setMessagesList(         
      messages.map((msg: React.ReactNode, index: number ) =>{
       return (
                              <div className="chat_left">
                              <div className="chat_icon1"><div className="icon_clr_rs1">A</div></div>
                              <div className="chat_name">{basicInfo.fullName != undefined ? basicInfo.fullName : ""}<span> 2 Hours</span></div>
                              <div className="clearfix" />
                              <div className="chat_box_l" key={index}> {msg}</div>
                              </div>
              );
      })       
    );
  },[messages]);


   console.log(loggedUserId,'vendorlist------------>',userList);
   console.log(loggedUserId,'sendUserId------------>',sendUserId);
   console.log(loggedUserId,'receiveUserId------------>',receiveUserId);
   console.log(loggedUserId,'messagelist------------>',messageList);

 
   const handleReceiveUser = async (receiveUserId:any,receiveUserName:any) => {
       setReceiveUserId(receiveUserId);
       setReceiveUserName(receiveUserName);
       setMessages([]);
       setShowing(true);
   }

   
    // Set the Hub Connection on mount.
    useEffect(() => {

            // Set the initial SignalR Hub Connection.
            const createHubConnection = async () => {

            const signalR = require("@aspnet/signalr");
           
            // Build new Hub Connection, url is currently hard coded.
            const hubConnect = new HubConnectionBuilder()
                .configureLogging(signalR.LogLevel.Debug)
                .withUrl("https://localhost:44361/chathub", {
                   skipNegotiation: true,
                   transport: signalR.HttpTransportType.WebSockets
                })
               .build();
            
               try {
                await hubConnect.start()            
                console.log('Connection successful!!!');
                console.log('getLoggedUserId',loggedUserId);
                console.log('user',user);
                console.log('user id',user?.id);
                setSendUserId(user?.id);
                setSendUserName(basicInfo.fullName);
                console.log('Basic Info',basicInfo);
                console.log(basicInfo.fullName)
                console.log("authorizationToken",authorizationToken)
               
                // Bind event handlers to the hubConnection.
                hubConnect.on('ReceiveMessage', (sendUserId:string, sendUserName: string, receiveUserId: string, receiveUserName:string, message: string) => {
                    setMessages(m => [...m, `${message}`]);
                    console.log("sendUserId :",`${sendUserId}`);
                    console.log("sendUserName :",`${sendUserName}`);
                    console.log("receiveUserId :",`${receiveUserId}`);
                    console.log("receiveUserName :",`${receiveUserName}`);
                })
                
                hubConnect.on('newuserconnected', (nick: string) => {
                    setMessages(m => [...m, `${nick} has connected.`]);
                })
                // **TODO**
                // hubConnection.off('userdisconnected', (nick: string) => {
                //  
                //     setMessages(m => [...m, `${nick} has disconnected.`]);
                // })
                   }
               catch (err) {
                        alert(err);
                        console.log('Error while establishing connection: ' + { err })
                    }
            setHubConnection(hubConnect);
        }
        createHubConnection();
    }, [loggedUserId]);

    const handleMessage = async () => {
    await sendMessage()
    setMessage('');
   }

    const onEnter = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === 'Enter') {
      handleMessage();
    }
   }

   /** Send message to server with client's nickname and message. */
    async function sendMessage(): Promise<void> {
        try {
            if (hubConnection && message !== '') {
                await hubConnection.invoke('SendMessage', sendUserId, sendUserName, receiveUserId, receiveUserName, message)

            }
        }
        catch (err) {
            console.error(err);
        }
    } 

   return (
    <React.Fragment>
    <div className="content-wrapper">
        <div className="container-fluid">
          <h1 className="heading">Message</h1>
          <div className="clearfix" />
          <div className="row ">
             <div className="col-sm-5 col-lg-4 p-r-0">
              <div className="panel panel-default panel_n">
                <div className="panel-body panel-body1">
                  <div className="connect_left">
                    <div>
                      <input value={filter} onChange={e => setFilter(e.target.value)} className="form-control" placeholder="Search" />
                      <div className="search_icon"><i className="fa fa-search" aria-hidden="true" /></div>
                    </div>
                    <div className=" m-t-25">
                      <div className="message_chat_scroll" style={{ overflow: 'hidden', outline: 'none' }} tabIndex={0}>
                        <div className="connect_scroll">
                         
                          <ul>

                       {list}
        
                           
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>  
            <div className="col-sm-7 col-lg-8">
              <div className="panel panel-default panel_n"  style={{ display: ({showing} ? 'block' : 'none') }}>
                <div className="panel-body panel-body1">
                  <div className="connect_right">
                    <div className="connect_right_top">
                        {/* {profileImage != undefined && profileImage.data != undefined && profileImage.data.length > 0 ?(
                            <div className="chat_icon1"><img
                className="img-responsive"
                src={`https://localhost:44361/Upload/VendorProfileImage/${profileImage.data[0]["imageUrl"]}`}
                alt=""
              /></div>
                        ):( 
                           <div className="chat_icon1"><div className="icon_clr_rs1">A</div></div>
                       )} */}
                       <div className="chat_icon1"><div className="icon_clr_rs1">A</div></div>
                      {/* <div className="connect_con_name_r">{basicInfo.fullName != undefined ? basicInfo.fullName : ""}</div> */}
                       <div className="connect_con_name_r">{receiveUserName}</div>
                      <div className="connect_con_ac">Online{showing}</div>
                    </div>
                    <div className=" m-t-10">
                      <div className="message_chat_des_scroll" style={{ overflow: 'revert', outline: 'none' }} tabIndex={0}>
                        <div className="connect_scroll_r">
                           {DBMessageList}
                           {messagesList}
                           {/* { messages.map((msg: React.ReactNode, index: number ) => (   
                            //  index%2==0 ? (
                              <div className="chat_left">
                              <div className="chat_icon1"><div className="icon_clr_rs1">A</div></div>
                              <div className="chat_name">{basicInfo.fullName != undefined ? basicInfo.fullName : ""}<span> 2 Hours</span></div>
                              <div className="clearfix" />
                              <div className="chat_box_l" key={index}> {msg}</div>
                              </div>
        
                          //     ) : (
                          //   <div className="chat_right">
                          //   <div className="chat_icon2"><div className="icon_clr_rs2">B</div></div>
                          //   <div className="chat_name1"><span>2 Hours</span> Name 2</div>
                          //   <div className="clearfix" />
                          //   <div className="chat_box_r" key={index}> {msg}</div>
                          //   <div className="chat_icon3"><div className="icon_clr_nr1">A</div></div>
                          //  </div>
                          //  )                         
                           ))} */}
                        </div>
                      </div>
                    </div>
                    <div className="connect_right_bottom">
                      <div className="connect_right_forms"> 
                      <input 
                        type="text" 
                        value={message} 
                        className="form-control" 
                        placeholder="Type message here" 
                        onChange={e => setMessage(e.target.value)}
                        onKeyDown={onEnter}
                      />                     
                      </div>
                      <div className="smile_icon"><img
                        src={require("../../../images/smile.png")}
                      // src="images/smile.png"
                      /></div>
                      <div className="connect_right_icons">
                        <a href="#"><img
                          src={require("../../../images/attach_icon.png")}
                        // src="images/attach_icon.png" 
                        /></a>
                        <a href="#"><img
                          src={require("../../../images/speaker_icon.png")}
                        // src="images/speaker_icon.png" 
                        /></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="clearfix" />
        </div>
      </div>
    </React.Fragment>
  );
};
export default VendorMessageComponent;
