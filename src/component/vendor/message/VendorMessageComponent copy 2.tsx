import React, { Component } from "react";

class VendorMessageComponent extends React.Component<any, any> {
  
  constructor(props:any) {
    super(props);
    this.state = {
      nick: '',
      message: '',
      messages: [],
      hubConnection: null,
    };
    this.keyPress = this.keyPress.bind(this);
  }

  keyPress(e:any){
      if(e.keyCode == 13){
        this.sendMessage()
      }
   }

  componentDidMount = () => {
    const nick='';
    const signalR = require("@aspnet/signalr");
 
    const hubConnection = new signalR.HubConnectionBuilder()
    .configureLogging(signalR.LogLevel.Debug)
    .withUrl("https://localhost:44361/chathub", {
      skipNegotiation: true,
      transport: signalR.HttpTransportType.WebSockets
    })
    .build();

   this.setState({ hubConnection, nick }, () => {     
      this.state.hubConnection
        .start()
        .then(() => console.log('Connection started!'))
        .catch((err:any) => console.log('Error while establishing connection :(',err));

      this.state.hubConnection.on('ReceiveMessage', (nick: any, receivedMessage: any) => {
        const text = `${receivedMessage}`;
        const messages = this.state.messages.concat([text]);
        this.setState({ messages });
      });
    });
  };

  sendMessage = () => {
    this.state.hubConnection
      .invoke('SendMessage', this.state.nick, this.state.message)
      .catch((err:any) => console.error(err));

      this.setState({message: ''});      
  };

  render() {
    return (
      <div className="content-wrapper">
        <div className="container-fluid">
          <h1 className="heading">Message</h1>
          <div className="clearfix" />
          <div className="row ">
            <div className="col-sm-5 col-lg-4 p-r-0">
              <div className="panel panel-default panel_n">
                <div className="panel-body panel-body1">
                  <div className="connect_left">
                    <div>
                      <input type="email" className="form-control" placeholder="Search" />
                      <div className="search_icon"><i className="fa fa-search" aria-hidden="true" /></div>
                    </div>
                    <div className=" m-t-25">
                      <div className="message_chat_scroll" style={{ overflow: 'hidden', outline: 'none' }} tabIndex={0}>
                        <div className="connect_scroll">
                          <ul>
                            <li><a href="#" className="active">
                              <div className="connect_icon"><div className="icon_clr1">A</div></div>
                              <div className="connect_con">
                                <div className="connect_con_name">Name <span className="dot dot-busy" /></div>
                                <div className="connect_con_des">Head of Development </div>
                              </div>
                              <div className="chat_time_sec">
                                <div className="chat_time">2 Min</div>
                                <div className="connect_con_noti">2</div>
                              </div>
                            </a></li>
                            <li><a href="#">
                              <div className="connect_icon"><div className="icon_clr2">B</div></div>
                              <div className="connect_con">
                                <div className="connect_con_name">Name <span className="dot dot-notactive" /></div>
                                <div className="connect_con_des">Head of Development </div>
                              </div>
                              <div className="chat_time_sec">
                                <div className="chat_time">2 Min</div>
                                <div className="connect_con_noti">2</div>
                              </div>
                            </a></li>
                            <li><a href="#">
                              <div className="connect_icon"><div className="icon_clr3">C</div></div>
                              <div className="connect_con">
                                <div className="connect_con_name">Name <span className="dot dot-active  " /></div>
                                <div className="connect_con_des">Head of Development </div>
                              </div>
                              <div className="chat_time_sec">
                                <div className="chat_time">2 Min</div>
                                <div className="connect_con_noti">2</div>
                              </div>
                            </a></li>
                            <li><a href="#">
                              <div className="connect_icon"><div className="icon_clr4">D</div></div>
                              <div className="connect_con">
                                <div className="connect_con_name">Name <span className="dot dot-active  " /></div>
                                <div className="connect_con_des">Head of Development </div>
                              </div>
                              <div className="chat_time_sec">
                                <div className="chat_time">2 Min</div>
                                <div className="connect_con_noti">2</div>
                              </div>
                            </a></li>
                            <li><a href="#">
                              <div className="connect_icon"><div className="icon_clr5">E</div></div>
                              <div className="connect_con">
                                <div className="connect_con_name">Name <span className="dot dot-active  " /></div>
                                <div className="connect_con_des">Head of Development </div>
                              </div>
                              <div className="chat_time_sec">
                                <div className="chat_time">2 Min</div>
                                <div className="connect_con_noti">2</div>
                              </div>
                            </a></li>
                            <li><a href="#">
                              <div className="connect_icon"><div className="icon_clr6">F</div></div>
                              <div className="connect_con">
                                <div className="connect_con_name">Name <span className="dot dot-active  " /></div>
                                <div className="connect_con_des">Head of Development </div>
                              </div>
                            </a></li>
                            <li><a href="#">
                              <div className="connect_icon"> <div className="icon_clr7">G</div></div>
                              <div className="connect_con">
                                <div className="connect_con_name">Name <span className="dot dot-active  " /></div>
                                <div className="connect_con_des">Head of Development </div>
                              </div>
                            </a></li>
                            <li><a href="#">
                              <div className="connect_icon"><div className="icon_clr8">H</div></div>
                              <div className="connect_con">
                                <div className="connect_con_name">Name <span className="dot dot-active  " /></div>
                                <div className="connect_con_des">Head of Development </div>
                              </div>
                            </a></li>
                            <li><a href="#">
                              <div className="connect_icon"><div className="icon_clr9">J</div></div>
                              <div className="connect_con">
                                <div className="connect_con_name">Name <span className="dot dot-active  " /></div>
                                <div className="connect_con_des">Head of Development </div>
                              </div>
                            </a></li>
                            <li><a href="#">
                              <div className="connect_icon"><div className="icon_clr10">K</div></div>
                              <div className="connect_con">
                                <div className="connect_con_name">Name <span className="dot dot-active  " /></div>
                                <div className="connect_con_des">Head of Development </div>
                              </div>
                            </a></li>
                            <li><a href="#">
                              <div className="connect_icon"><div className="icon_clr11">L</div></div>
                              <div className="connect_con">
                                <div className="connect_con_name">Name <span className="dot dot-active  " /></div>
                                <div className="connect_con_des">Head of Development </div>
                              </div>
                            </a></li>
                            <li><a href="#">
                              <div className="connect_icon"><div className="icon_clr12">M</div></div>
                              <div className="connect_con">
                                <div className="connect_con_name">Name <span className="dot dot-active  " /></div>
                                <div className="connect_con_des">Head of Development </div>
                              </div>
                            </a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-7 col-lg-8">
              <div className="panel panel-default panel_n">
                <div className="panel-body panel-body1">
                  <div className="connect_right">
                    <div className="connect_right_top">
                      <div className="chat_icon1"><div className="icon_clr_rs1">A</div></div>
                      <div className="connect_con_name_r">Name</div>
                      <div className="connect_con_ac">Online</div>
                    </div>
                    <div className=" m-t-10">
                      <div className="message_chat_des_scroll" style={{ overflow: 'revert', outline: 'none' }} tabIndex={0}>
                        <div className="connect_scroll_r">
                           {this.state.messages.map((message: React.ReactNode, index: number ) => (   
                             index%2==0 ? (
                              <div className="chat_left">
                              <div className="chat_icon1"><div className="icon_clr_rs1">A</div></div>
                              <div className="chat_name">Name 1 <span> 2 Hours</span></div>
                              <div className="clearfix" />
                              <div className="chat_box_l" key={index}> {message}</div>
                              </div>
        
                              ) : (
                            <div className="chat_right">
                            <div className="chat_icon2"><div className="icon_clr_rs2">B</div></div>
                            <div className="chat_name1"><span>2 Hours</span> Name 2</div>
                            <div className="clearfix" />
                            <div className="chat_box_r" key={index}> {message}</div>
                            <div className="chat_icon3"><div className="icon_clr_nr1">A</div></div>
                           </div>
                           )                         
                           ))}
                        </div>
                      </div>
                    </div>
                    <div className="connect_right_bottom">
                      <div className="connect_right_forms"> 
                      <input 
                        type="text" 
                        value={this.state.message} 
                        className="form-control" 
                        placeholder="Search" 
                        onChange={e => this.setState({ message: e.target.value })}
                        onKeyDown={this.keyPress} 
                      />                     
                      </div>
                      <div className="smile_icon"><img
                        src={require("../../../images/smile.png")}
                      // src="images/smile.png"
                      /></div>
                      <div className="connect_right_icons">
                        <a href="#"><img
                          src={require("../../../images/attach_icon.png")}
                        // src="images/attach_icon.png" 
                        /></a>
                        <a href="#"><img
                          src={require("../../../images/speaker_icon.png")}
                        // src="images/speaker_icon.png" 
                        /></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="clearfix" />
        </div>
      </div>
  );
  }
}
export default VendorMessageComponent;
