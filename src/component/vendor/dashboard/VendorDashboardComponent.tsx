import React from "react";
interface IVendorDashboardComponentProps {}
interface IVendorDashboardComponentState {}
const VendorDashboardComponent: React.FC<IVendorDashboardComponentProps> = (
  props
) => {
  return (
    <>
      <div className="content-wrapper">
        <div className="container-fluid">
          <div className="row m-t-25">
            <div className="col-sm-12">
              <div className="dash_sec">
                <div className="row">
                  <div className="col-sm-2">
                    <div className="dash_notification">
                      <img src={require("../../../images/my_candidates.png")} />{" "}
                      12 My candidates
                    </div>
                  </div>
                  <div className="col-sm-2">
                    <div className="dash_notification">
                      <img src={require("../../../images/shortlisted.png")} />{" "}
                      60.34 Sucess %
                    </div>
                  </div>
                  <div className="col-sm-2">
                    <div className="dash_notification">
                      <img
                        src={require("../../../images/expiring_documents.png")}
                      />{" "}
                      1 Job Posts
                    </div>
                  </div>
                  <div className="col-sm-2">
                    <div className="dash_notification">
                      <img src={require("../../../images/jobs_applied.png")} />{" "}
                      12 Matched Jobs
                    </div>
                  </div>
                  <div className="col-sm-2">
                    <div className="dash_notification">
                      <img src={require("../../../images/new_messages.png")} />{" "}
                      10 New Messages
                    </div>
                  </div>
                  <div className="col-sm-2">
                    <div className="dash_notification">
                      <img src={require("../../../images/new_alerts.png")} /> 3
                      New Alerts
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-8">
              <div className="section_box1">
                <h1>Matched Jobs</h1>
                <div className="row">
                  <div className="prodict_list">
                    <div
                      id="owl-demo1"
                      className="owl-carousel owl-theme"
                      style={{ opacity: 1, display: "block" }}
                    >
                      <div className="owl-wrapper-outer">
                        <div
                          className="owl-wrapper"
                          style={{
                            width: "1770px",
                            left: "0px",
                            display: "block",
                            transform: "translate3d(-275px, 0px, 0px)",
                            transition: "all 0ms ease 0s",
                          }}
                        >
                          <div className="owl-item" style={{ width: "275px" }}>
                            <div className="item">
                              <div className="matched_jobs_sec">
                                <div className="matched_jobs">
                                  <div className="matched_jobs_cat_t">
                                    Design
                                  </div>
                                  <div className="jobs_start1">2 days ago</div>
                                  <div className="matched_star1">
                                    <i
                                      className="fa fa-star-o"
                                      aria-hidden="true"
                                    />
                                  </div>
                                  <div className="clearfix" />
                                  <div className="matched_heading">
                                    UI/Designer
                                  </div>
                                  <div className="matched_jobs_cat">
                                    <i
                                      className="fa fa-map-marker"
                                      aria-hidden="true"
                                    />{" "}
                                    San Fransisco
                                  </div>
                                  <div className="matched_jobs_cat text-right">
                                    <i
                                      className="fa fa-clock-o"
                                      aria-hidden="true"
                                    />{" "}
                                    Part Time
                                  </div>
                                  <div className="clearfix" />
                                  <div className="matched_jobs_cat">
                                    <i
                                      className="fa fa-list-ul"
                                      aria-hidden="true"
                                    />{" "}
                                    7 Vecancies
                                  </div>
                                  <div className="clearfix" />
                                  <div className="matched_jobs_share">
                                    <a
                                      href="#"
                                      data-target="#share"
                                      data-toggle="modal"
                                    >
                                      <i
                                        className="fa fa-share-alt"
                                        aria-hidden="true"
                                      />
                                    </a>
                                  </div>
                                  <div className="jobs_apply">
                                    <a
                                      href="#"
                                      data-target="#apply_now_pop"
                                      data-toggle="modal"
                                    >
                                      Apply Now
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="owl-item" style={{ width: "275px" }}>
                            <div className="item">
                              <div className="matched_jobs_sec">
                                <div className="matched_jobs">
                                  <div className="matched_jobs_cat_t">
                                    Design
                                  </div>
                                  <div className="jobs_start1">2 days ago</div>
                                  <div className="matched_star1">
                                    <i
                                      className="fa fa-star-o"
                                      aria-hidden="true"
                                    />
                                  </div>
                                  <div className="clearfix" />
                                  <div className="matched_heading">
                                    UI/Designer
                                  </div>
                                  <div className="matched_jobs_cat">
                                    <i
                                      className="fa fa-map-marker"
                                      aria-hidden="true"
                                    />{" "}
                                    San Fransisco
                                  </div>
                                  <div className="matched_jobs_cat text-right">
                                    <i
                                      className="fa fa-clock-o"
                                      aria-hidden="true"
                                    />{" "}
                                    Part Time
                                  </div>
                                  <div className="clearfix" />
                                  <div className="matched_jobs_cat">
                                    <i
                                      className="fa fa-list-ul"
                                      aria-hidden="true"
                                    />{" "}
                                    7 Vecancies
                                  </div>
                                  <div className="clearfix" />
                                  <div className="matched_jobs_share">
                                    <a
                                      href="#"
                                      data-target="#share"
                                      data-toggle="modal"
                                    >
                                      <i
                                        className="fa fa-share-alt"
                                        aria-hidden="true"
                                      />
                                    </a>
                                  </div>
                                  <div className="jobs_apply">
                                    <a
                                      href="#"
                                      data-target="#apply_now_pop"
                                      data-toggle="modal"
                                    >
                                      Apply Now
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="owl-item" style={{ width: "275px" }}>
                            <div className="item">
                              <div className="matched_jobs_sec">
                                <div className="matched_jobs">
                                  <div className="matched_jobs_cat_t">
                                    Design
                                  </div>
                                  <div className="jobs_start1">2 days ago</div>
                                  <div className="matched_star1">
                                    <i
                                      className="fa fa-star-o"
                                      aria-hidden="true"
                                    />
                                  </div>
                                  <div className="clearfix" />
                                  <div className="matched_heading">
                                    UI/Designer
                                  </div>
                                  <div className="matched_jobs_cat">
                                    <i
                                      className="fa fa-map-marker"
                                      aria-hidden="true"
                                    />{" "}
                                    San Fransisco
                                  </div>
                                  <div className="matched_jobs_cat text-right">
                                    <i
                                      className="fa fa-clock-o"
                                      aria-hidden="true"
                                    />{" "}
                                    Part Time
                                  </div>
                                  <div className="clearfix" />
                                  <div className="matched_jobs_cat">
                                    <i
                                      className="fa fa-list-ul"
                                      aria-hidden="true"
                                    />{" "}
                                    7 Vecancies
                                  </div>
                                  <div className="clearfix" />
                                  <div className="matched_jobs_share">
                                    <a
                                      href="#"
                                      data-target="#share"
                                      data-toggle="modal"
                                    >
                                      <i
                                        className="fa fa-share-alt"
                                        aria-hidden="true"
                                      />
                                    </a>
                                  </div>
                                  <div className="jobs_apply">
                                    <a
                                      href="#"
                                      data-target="#apply_now_pop"
                                      data-toggle="modal"
                                    >
                                      Apply Now
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="owl-item" style={{ width: "275px" }}>
                            <div className="item">
                              <div className="matched_jobs_sec">
                                <div className="matched_jobs">
                                  <div className="matched_jobs_cat_t">
                                    Design
                                  </div>
                                  <div className="jobs_start1">2 days ago</div>
                                  <div className="matched_star1">
                                    <i
                                      className="fa fa-star-o"
                                      aria-hidden="true"
                                    />
                                  </div>
                                  <div className="clearfix" />
                                  <div className="matched_heading">
                                    UI/Designer
                                  </div>
                                  <div className="matched_jobs_cat">
                                    <i
                                      className="fa fa-map-marker"
                                      aria-hidden="true"
                                    />{" "}
                                    San Fransisco
                                  </div>
                                  <div className="matched_jobs_cat text-right">
                                    <i
                                      className="fa fa-clock-o"
                                      aria-hidden="true"
                                    />{" "}
                                    Part Time
                                  </div>
                                  <div className="clearfix" />
                                  <div className="matched_jobs_cat">
                                    <i
                                      className="fa fa-list-ul"
                                      aria-hidden="true"
                                    />{" "}
                                    7 Vecancies
                                  </div>
                                  <div className="clearfix" />
                                  <div className="matched_jobs_share">
                                    <a
                                      href="#"
                                      data-target="#share"
                                      data-toggle="modal"
                                    >
                                      <i
                                        className="fa fa-share-alt"
                                        aria-hidden="true"
                                      />
                                    </a>
                                  </div>
                                  <div className="jobs_apply">
                                    <a
                                      href="#"
                                      data-target="#apply_now_pop"
                                      data-toggle="modal"
                                    >
                                      Apply Now
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="owl-item" style={{ width: "275px" }}>
                            <div className="item">
                              <div className="matched_jobs_sec">
                                <div className="matched_jobs">
                                  <div className="matched_jobs_cat_t">
                                    Design
                                  </div>
                                  <div className="jobs_start1">2 days ago</div>
                                  <div className="matched_star1">
                                    <i
                                      className="fa fa-star-o"
                                      aria-hidden="true"
                                    />
                                  </div>
                                  <div className="clearfix" />
                                  <div className="matched_heading">
                                    UI/Designer
                                  </div>
                                  <div className="matched_jobs_cat">
                                    <i
                                      className="fa fa-map-marker"
                                      aria-hidden="true"
                                    />{" "}
                                    San Fransisco
                                  </div>
                                  <div className="matched_jobs_cat text-right">
                                    <i
                                      className="fa fa-clock-o"
                                      aria-hidden="true"
                                    />{" "}
                                    Part Time
                                  </div>
                                  <div className="clearfix" />
                                  <div className="matched_jobs_cat">
                                    <i
                                      className="fa fa-list-ul"
                                      aria-hidden="true"
                                    />{" "}
                                    7 Vecancies
                                  </div>
                                  <div className="clearfix" />
                                  <div className="matched_jobs_share">
                                    <a
                                      href="#"
                                      data-target="#share"
                                      data-toggle="modal"
                                    >
                                      <i
                                        className="fa fa-share-alt"
                                        aria-hidden="true"
                                      />
                                    </a>
                                  </div>
                                  <div className="jobs_apply">
                                    <a
                                      href="#"
                                      data-target="#apply_now_pop"
                                      data-toggle="modal"
                                    >
                                      Apply Now
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="owl-controls clickable">
                        <div className="owl-pagination">
                          <div className="owl-page active">
                            <span className="" />
                          </div>
                          <div className="owl-page">
                            <span className="" />
                          </div>
                        </div>
                        <div className="owl-buttons">
                          <div className="owl-prev"> </div>
                          <div className="owl-next"> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="section_box1">
                <h1>My Job Posts</h1>
                <div className="row">
                  <div className="prodict_list">
                    <div
                      id="owl-demo2"
                      className="owl-carousel owl-theme"
                      style={{ opacity: 1, display: "block" }}
                    >
                      <div className="owl-wrapper-outer">
                        <div
                          className="owl-wrapper"
                          style={{
                            width: "1770px",
                            left: "0px",
                            display: "block",
                            transition: "all 0ms ease 0s",
                            transform: "translate3d(0px, 0px, 0px)",
                          }}
                        >
                          <div className="owl-item" style={{ width: "275px" }}>
                            <div className="item">
                              <div className="matched_jobs_sec">
                                <div className="matched_jobs">
                                  <div className="matched_jobs_cat_t">
                                    Design
                                  </div>
                                  <div className="matched_star1">
                                    <i
                                      className="fa fa-star-o"
                                      aria-hidden="true"
                                    />
                                  </div>
                                  <div className="clearfix" />
                                  <div className="matched_heading">
                                    UI/Designer
                                  </div>
                                  <div className="matched_jobs_cat">
                                    <i
                                      className="fa fa-map-marker"
                                      aria-hidden="true"
                                    />{" "}
                                    San Fransisco
                                  </div>
                                  <div className="matched_jobs_cat text-right">
                                    <i
                                      className="fa fa-clock-o"
                                      aria-hidden="true"
                                    />{" "}
                                    Part Time
                                  </div>
                                  <div className="clearfix" />
                                  <div className="matched_jobs_cat">
                                    <i
                                      className="fa fa-list-ul"
                                      aria-hidden="true"
                                    />{" "}
                                    7 Vecancies
                                  </div>
                                  <div className="matched_jobs_cat text-right">
                                    <i
                                      className="fa fa-calendar"
                                      aria-hidden="true"
                                    />{" "}
                                    Applied On <br />
                                    3-10-2020
                                  </div>
                                  <div className="clearfix" />
                                  <div className="matched_jobs_share">
                                    <a
                                      href="#"
                                      data-target="#share"
                                      data-toggle="modal"
                                    >
                                      <i
                                        className="fa fa-share-alt"
                                        aria-hidden="true"
                                      />
                                    </a>
                                  </div>
                                  <div className="matched_jobs_status">
                                    <a
                                      href="#"
                                      data-target="#status_pop"
                                      data-toggle="modal"
                                    >
                                      Status : Selected
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="owl-item" style={{ width: "275px" }}>
                            <div className="item">
                              <div className="matched_jobs_sec">
                                <div className="matched_jobs">
                                  <div className="matched_jobs_cat_t">
                                    Design
                                  </div>
                                  <div className="matched_star1">
                                    <i
                                      className="fa fa-star-o"
                                      aria-hidden="true"
                                    />
                                  </div>
                                  <div className="clearfix" />
                                  <div className="matched_heading">
                                    UI/Designer
                                  </div>
                                  <div className="matched_jobs_cat">
                                    <i
                                      className="fa fa-map-marker"
                                      aria-hidden="true"
                                    />{" "}
                                    San Fransisco
                                  </div>
                                  <div className="matched_jobs_cat text-right">
                                    <i
                                      className="fa fa-clock-o"
                                      aria-hidden="true"
                                    />{" "}
                                    Part Time
                                  </div>
                                  <div className="clearfix" />
                                  <div className="matched_jobs_cat">
                                    <i
                                      className="fa fa-list-ul"
                                      aria-hidden="true"
                                    />{" "}
                                    7 Vecancies
                                  </div>
                                  <div className="matched_jobs_cat text-right">
                                    <i
                                      className="fa fa-calendar"
                                      aria-hidden="true"
                                    />{" "}
                                    Applied On <br />
                                    3-10-2020
                                  </div>
                                  <div className="clearfix" />
                                  <div className="matched_jobs_share">
                                    <a
                                      href="#"
                                      data-target="#share"
                                      data-toggle="modal"
                                    >
                                      <i
                                        className="fa fa-share-alt"
                                        aria-hidden="true"
                                      />
                                    </a>
                                  </div>
                                  <div className="matched_jobs_status">
                                    <a
                                      href="#"
                                      data-target="#status_pop"
                                      data-toggle="modal"
                                    >
                                      Status : Selected
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="owl-item" style={{ width: "275px" }}>
                            <div className="item">
                              <div className="matched_jobs_sec">
                                <div className="matched_jobs">
                                  <div className="matched_jobs_cat_t">
                                    Design
                                  </div>
                                  <div className="matched_star1">
                                    <i
                                      className="fa fa-star-o"
                                      aria-hidden="true"
                                    />
                                  </div>
                                  <div className="clearfix" />
                                  <div className="matched_heading">
                                    UI/Designer
                                  </div>
                                  <div className="matched_jobs_cat">
                                    <i
                                      className="fa fa-map-marker"
                                      aria-hidden="true"
                                    />{" "}
                                    San Fransisco
                                  </div>
                                  <div className="matched_jobs_cat text-right">
                                    <i
                                      className="fa fa-clock-o"
                                      aria-hidden="true"
                                    />{" "}
                                    Part Time
                                  </div>
                                  <div className="clearfix" />
                                  <div className="matched_jobs_cat">
                                    <i
                                      className="fa fa-list-ul"
                                      aria-hidden="true"
                                    />{" "}
                                    7 Vecancies
                                  </div>
                                  <div className="matched_jobs_cat text-right">
                                    <i
                                      className="fa fa-calendar"
                                      aria-hidden="true"
                                    />{" "}
                                    Applied On <br />
                                    3-10-2020
                                  </div>
                                  <div className="clearfix" />
                                  <div className="matched_jobs_share">
                                    <a
                                      href="#"
                                      data-target="#share"
                                      data-toggle="modal"
                                    >
                                      <i
                                        className="fa fa-share-alt"
                                        aria-hidden="true"
                                      />
                                    </a>
                                  </div>
                                  <div className="matched_jobs_status">
                                    <a
                                      href="#"
                                      data-target="#status_pop"
                                      data-toggle="modal"
                                    >
                                      Status : Selected
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="owl-item" style={{ width: "275px" }}>
                            <div className="item">
                              <div className="matched_jobs_sec">
                                <div className="matched_jobs">
                                  <div className="matched_jobs_cat_t">
                                    Design
                                  </div>
                                  <div className="matched_star1">
                                    <i
                                      className="fa fa-star-o"
                                      aria-hidden="true"
                                    />
                                  </div>
                                  <div className="clearfix" />
                                  <div className="matched_heading">
                                    UI/Designer
                                  </div>
                                  <div className="matched_jobs_cat">
                                    <i
                                      className="fa fa-map-marker"
                                      aria-hidden="true"
                                    />{" "}
                                    San Fransisco
                                  </div>
                                  <div className="matched_jobs_cat text-right">
                                    <i
                                      className="fa fa-clock-o"
                                      aria-hidden="true"
                                    />{" "}
                                    Part Time
                                  </div>
                                  <div className="clearfix" />
                                  <div className="matched_jobs_cat">
                                    <i
                                      className="fa fa-list-ul"
                                      aria-hidden="true"
                                    />{" "}
                                    7 Vecancies
                                  </div>
                                  <div className="matched_jobs_cat text-right">
                                    <i
                                      className="fa fa-calendar"
                                      aria-hidden="true"
                                    />{" "}
                                    Applied On <br />
                                    3-10-2020
                                  </div>
                                  <div className="clearfix" />
                                  <div className="matched_jobs_share">
                                    <a
                                      href="#"
                                      data-target="#share"
                                      data-toggle="modal"
                                    >
                                      <i
                                        className="fa fa-share-alt"
                                        aria-hidden="true"
                                      />
                                    </a>
                                  </div>
                                  <div className="matched_jobs_status">
                                    <a
                                      href="#"
                                      data-target="#status_pop"
                                      data-toggle="modal"
                                    >
                                      Status : Selected
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="owl-item" style={{ width: "275px" }}>
                            <div className="item">
                              <div className="matched_jobs_sec">
                                <div className="matched_jobs">
                                  <div className="matched_jobs_cat_t">
                                    Design
                                  </div>
                                  <div className="matched_star1">
                                    <i
                                      className="fa fa-star-o"
                                      aria-hidden="true"
                                    />
                                  </div>
                                  <div className="clearfix" />
                                  <div className="matched_heading">
                                    UI/Designer
                                  </div>
                                  <div className="matched_jobs_cat">
                                    <i
                                      className="fa fa-map-marker"
                                      aria-hidden="true"
                                    />{" "}
                                    San Fransisco
                                  </div>
                                  <div className="matched_jobs_cat text-right">
                                    <i
                                      className="fa fa-clock-o"
                                      aria-hidden="true"
                                    />{" "}
                                    Part Time
                                  </div>
                                  <div className="clearfix" />
                                  <div className="matched_jobs_cat">
                                    <i
                                      className="fa fa-list-ul"
                                      aria-hidden="true"
                                    />{" "}
                                    7 Vecancies
                                  </div>
                                  <div className="matched_jobs_cat text-right">
                                    <i
                                      className="fa fa-calendar"
                                      aria-hidden="true"
                                    />{" "}
                                    Applied On <br />
                                    3-10-2020
                                  </div>
                                  <div className="clearfix" />
                                  <div className="matched_jobs_share">
                                    <a
                                      href="#"
                                      data-target="#share"
                                      data-toggle="modal"
                                    >
                                      <i
                                        className="fa fa-share-alt"
                                        aria-hidden="true"
                                      />
                                    </a>
                                  </div>
                                  <div className="matched_jobs_status">
                                    <a
                                      href="#"
                                      data-target="#status_pop"
                                      data-toggle="modal"
                                    >
                                      Status : Selected
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="owl-controls clickable">
                        <div className="owl-pagination">
                          <div className="owl-page active">
                            <span className="" />
                          </div>
                          <div className="owl-page">
                            <span className="" />
                          </div>
                        </div>
                        <div className="owl-buttons">
                          <div className="owl-prev"> </div>
                          <div className="owl-next"> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* BEGIN MODAL */}
            <div
              className="modal fade none-border"
              id="share"
              style={{ display: "none" }}
            >
              <div className="modal-dialog">
                <div className="modal-content">
                  <div className="modal-header">
                    <button
                      aria-hidden="true"
                      data-dismiss="modal"
                      className="close"
                      type="button"
                    >
                      ×
                    </button>
                    <h4 className="modal-title">Share </h4>
                  </div>
                  <div className="col-sm-12 m_t_30 text-center">
                    <div className="social1">
                      <a href="#" className="social_face">
                        <i className="fa fa-facebook" aria-hidden="true" />
                      </a>
                      <a href="#" className="social_twit">
                        <i className="fa fa-twitter" aria-hidden="true" />
                      </a>
                      <a href="#" className="social_insta">
                        <i className="fa fa-instagram" aria-hidden="true" />
                      </a>
                      <a href="#" className="social_youtube">
                        <i className="fa fa-youtube-play" aria-hidden="true" />
                      </a>
                    </div>
                  </div>
                  <div className="modal-footer  m-t-30"></div>
                </div>
              </div>
            </div>
            {/* END MODAL */}
            <div className="col-sm-4">
              <div className="section_box2">
                <h1>Messages</h1>
                <div
                  className="dash_message_vendor"
                  style={{ overflow: "hidden", outline: "none" }}
                  tabIndex={0}
                >
                  <div className="message_sec1">
                    <a href="message.html">
                      <span className="message_icon">
                        <img
                          src={require("../../../images/4.jpg")}
                          className="img-responsive"
                        />
                      </span>
                      <div className="message_con_vend">
                        <div className="message_head">Alan Mathew</div>
                        <div className="message_pra">
                          Hi, Share your Aadhar card please!
                        </div>
                      </div>
                      <div className="message_noti">
                        <div className="message_noti_con">1 day ago</div>
                        <div className="message_noti_count">2</div>
                      </div>
                    </a>
                  </div>
                  <div className="message_sec1">
                    <a href="message.html">
                      <span className="message_icon">
                        <img
                          src={require("../../../images/5.jpg")}
                          className="img-responsive"
                        />
                      </span>
                      <div className="message_con_vend">
                        <div className="message_head">Helen</div>
                        <div className="message_pra">
                          Please find below form.
                        </div>
                      </div>
                      <div className="message_noti">
                        <div className="message_noti_con">3 day ago</div>
                        <div className="message_noti_count">2</div>
                      </div>
                    </a>
                  </div>
                  <div className="message_sec1">
                    <a href="message.html">
                      <span className="message_icon">
                        <img
                          src={require("../../../images/6.jpg")}
                          className="img-responsive"
                        />
                      </span>
                      <div className="message_con_vend">
                        <div className="message_head">Udai</div>
                        <div className="message_pra">
                          Share your availability for an interview.
                        </div>
                      </div>
                      <div className="message_noti">
                        <div className="message_noti_con">5 day ago</div>
                        <div className="message_noti_count">5</div>
                      </div>
                    </a>
                  </div>
                  <div className="message_sec1">
                    <a href="message.html">
                      <span className="message_icon">
                        <img
                          src={require("../../../images/4.jpg")}
                          className="img-responsive"
                        />
                      </span>
                      <div className="message_con_vend">
                        <div className="message_head">Alan Mathew</div>
                        <div className="message_pra">
                          Hi, Share your Aadhar card please!
                        </div>
                      </div>
                      <div className="message_noti">
                        <div className="message_noti_con">1 day ago</div>
                        <div className="message_noti_count">2</div>
                      </div>
                    </a>
                  </div>
                  <div className="message_sec1">
                    <a href="message.html">
                      <span className="message_icon">
                        <img
                          src={require("../../../images/5.jpg")}
                          className="img-responsive"
                        />
                      </span>
                      <div className="message_con_vend">
                        <div className="message_head">Helen</div>
                        <div className="message_pra">
                          Please find below form.
                        </div>
                      </div>
                      <div className="message_noti">
                        <div className="message_noti_con">3 day ago</div>
                        <div className="message_noti_count">2</div>
                      </div>
                    </a>
                  </div>
                  <div className="message_sec1">
                    <a href="message.html">
                      <span className="message_icon">
                        <img
                          src={require("../../../images/6.jpg")}
                          className="img-responsive"
                        />
                      </span>
                      <div className="message_con_vend">
                        <div className="message_head">Udai</div>
                        <div className="message_pra">
                          Share your availability for an interview.
                        </div>
                      </div>
                      <div className="message_noti">
                        <div className="message_noti_con">5 day ago</div>
                        <div className="message_noti_count">5</div>
                      </div>
                    </a>
                  </div>
                  <div className="message_sec1">
                    <a href="message.html">
                      <span className="message_icon">
                        <img
                          src={require("../../../images/6.jpg")}
                          className="img-responsive"
                        />
                      </span>
                      <div className="message_con_vend">
                        <div className="message_head">Udai</div>
                        <div className="message_pra">
                          Share your availability for an interview.
                        </div>
                      </div>
                      <div className="message_noti">
                        <div className="message_noti_con">5 day ago</div>
                        <div className="message_noti_count">5</div>
                      </div>
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-6">
              <div className="section_box2">
                <h1>Job Alert</h1>
                <div className="jobs_alert_btn">
                  <a href="#" data-target="#job_alert" data-toggle="modal">
                    Create Alert
                  </a>
                </div>
                <div className="clearfix" />
                <div
                  className="jobs_alert_scroll"
                  style={{ overflow: "hidden", outline: "none" }}
                  tabIndex={0}
                >
                  <div className="jobalert_sec">
                    <span className="jobalert_sec_icon">
                      <img
                        src={require("../../../images/job_close.png")}
                        className="img-responsive"
                      />
                    </span>
                    <div className="jobalert_con">
                      <div className="jobalert_headings jobalert_clr1">
                        Management
                      </div>
                      <div className="jobalert_head">Site Engineer </div>
                      <div className="jobalert_active">active</div>
                    </div>
                    <div className="jobalert_con1">
                      <div className="jobalert_time">1 day ago</div>
                    </div>
                  </div>
                  <div className="jobalert_sec">
                    <span className="jobalert_sec_icon">
                      <img
                        src={require("../../../images/job_close.png")}
                        className="img-responsive"
                      />
                    </span>
                    <div className="jobalert_con">
                      <div className="jobalert_headings jobalert_clr2">IT</div>
                      <div className="jobalert_head">Software Engineer </div>
                      <div className="jobalert_inactive">Inactive</div>
                    </div>
                    <div className="jobalert_con1">
                      <div className="jobalert_time">2 day ago</div>
                    </div>
                  </div>
                  <div className="jobalert_sec">
                    <span className="jobalert_sec_icon">
                      <img
                        src={require("../../../images/job_close.png")}
                        className="img-responsive"
                      />
                    </span>
                    <div className="jobalert_con">
                      <div className="jobalert_headings jobalert_clr3">
                        Fieldwork
                      </div>
                      <div className="jobalert_head">Welder</div>
                      <div className="jobalert_inactive">Inactive</div>
                    </div>
                    <div className="jobalert_con1">
                      <div className="jobalert_time">3 day ago</div>
                    </div>
                  </div>
                  <div className="jobalert_sec">
                    <span className="jobalert_sec_icon">
                      <img
                        src={require("../../../images/job_close.png")}
                        className="img-responsive"
                      />
                    </span>
                    <div className="jobalert_con">
                      <div className="jobalert_headings jobalert_clr2">IT</div>
                      <div className="jobalert_head">Software Engineer </div>
                      <div className="jobalert_inactive">Inactive</div>
                    </div>
                    <div className="jobalert_con1">
                      <div className="jobalert_time">2 day ago</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* BEGIN MODAL */}
            <div className="modal fade none-border" id="job_alert">
              <div className="modal-dialog">
                <div className="modal-content">
                  <div className="modal-header">
                    <button
                      aria-hidden="true"
                      data-dismiss="modal"
                      className="close"
                      type="button"
                    >
                      ×
                    </button>
                    <h4 className="modal-title">New Job Alert </h4>
                  </div>
                  <div className="col-sm-12 m_t_30">
                    <div className="form-group">
                      <label htmlFor="email">Job Category</label>
                      <span className="select-wrapper-sec">
                        <select name="timepass" className="custom-select-sec">
                          <option value="">Select</option>
                          <option value="">Dummy</option>
                          <option value="">Dummy</option>
                        </select>
                        <span className="holder">Select</span>
                      </span>
                    </div>
                    <div className="form-group">
                      <label htmlFor="email">Job Type</label>
                      <span className="select-wrapper-sec">
                        <select name="timepass" className="custom-select-sec">
                          <option value="">Select</option>
                          <option value="">Dummy</option>
                          <option value="">Dummy</option>
                        </select>
                        <span className="holder">Select</span>
                      </span>
                    </div>
                    <div className="form-group">
                      <label htmlFor="email">Job Title</label>
                      <span className="select-wrapper-sec">
                        <select name="timepass" className="custom-select-sec">
                          <option value="">Select</option>
                          <option value="">Dummy</option>
                          <option value="">Dummy</option>
                        </select>
                        <span className="holder">Select</span>
                      </span>
                    </div>
                    <div className="form-group">
                      <label htmlFor="email">Location</label>
                      <input
                        type="text"
                        className="form-control "
                        placeholder="Add Number"
                      />
                    </div>
                    <div className="form-group">
                      <input
                        id="radio1"
                        type="radio"
                        name="radio"
                        defaultValue={1}
                        defaultChecked
                      />
                      <label htmlFor="radio1">
                        <span>
                          <span />
                        </span>{" "}
                        Active
                      </label>
                      <input
                        id="radio2"
                        type="radio"
                        name="radio"
                        defaultValue={2}
                      />
                      <label htmlFor="radio2">
                        <span>
                          <span />
                        </span>{" "}
                        Inactive
                      </label>
                    </div>
                  </div>
                  <div className="modal-footer  m-t-30">
                    <button
                      className="btn btn-success save-event waves-effect waves-light"
                      type="button"
                    >
                      Create Alert
                    </button>
                    <button
                      data-dismiss="modal"
                      className="btn btn-default waves-effect"
                      type="button"
                    >
                      Cancel
                    </button>
                  </div>
                </div>
              </div>
            </div>
            {/* END MODAL */}
            <div className="col-sm-6">
              <div className="section_box2">
                <h1>Notifications</h1>
                <div
                  className="dash_noti_scroll"
                  style={{ overflow: "hidden", outline: "none" }}
                  tabIndex={0}
                >
                  <div className="notification_sec">
                    <a href="#">
                      <span className="notification_icon">
                        <img
                          src={require("../../../images/1.jpg")}
                          className="img-responsive"
                        />
                      </span>
                      <div className="notification_con">
                        <div className="notification_head">
                          <strong>Stephan parker</strong> has messaged you
                        </div>
                        <div className="notification_pra">4 days ago</div>
                      </div>
                    </a>
                  </div>
                  <div className="notification_sec">
                    <a href="#">
                      <span className="notification_icon">
                        <img
                          src={require("../../../images/2.jpg")}
                          className="img-responsive"
                        />
                      </span>
                      <div className="notification_con">
                        <div className="notification_head">
                          <strong>24 Matched jobs</strong> for you. Review them.
                        </div>
                        <div className="notification_pra">4 days ago</div>
                      </div>
                    </a>
                  </div>
                  <div className="notification_sec">
                    <a href="#">
                      <span className="notification_icon">
                        <img
                          src={require("../../../images/3.jpg")}
                          className="img-responsive"
                        />
                      </span>
                      <div className="notification_con">
                        <div className="notification_head">
                          <strong>2 Documents are getting expired</strong> this
                          month. Re-new them now.
                        </div>
                        <div className="notification_pra">4 days ago</div>
                      </div>
                    </a>
                  </div>
                  <div className="notification_sec">
                    <a href="#">
                      <span className="notification_icon">
                        <img
                          src={require("../../../images/3.jpg")}
                          className="img-responsive"
                        />
                      </span>
                      <div className="notification_con">
                        <div className="notification_head">
                          <strong>2 Documents are getting expired</strong> this
                          month. Re-new them now.
                        </div>
                        <div className="notification_pra">4 days ago</div>
                      </div>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="clearfix" />
        </div>
      </div>
    </>
  );
};

export default VendorDashboardComponent;
