import React from "react";
import VendorDashboardComponent from "./VendorDashboardComponent";
interface IVendorDashboardContainerProps {}
interface IVendorDashboardContainerState {}
const VendorDashboardContainer: React.FC<IVendorDashboardContainerProps> = (
  props
) => {
  return (
    <>
      <VendorDashboardComponent />
    </>
  );
};

export default VendorDashboardContainer;
