import React, { useState, useEffect } from "react";
import {
  getLoggedUserId,
  getMyProfileSelectBoxList,
  useMyProfileContext,
  useMyProfileDispatcher,
} from "../../../action/MyProfileVendorAction";
import VendorProfileComponent from "./VendorProfileComponent";
import {
 getVendorId
} from "./../../../apis/vendor";
import AuthService from "./../../../services/AuthService";
import {
  useVendorContext,
  useSocialAccountContext,
  useVendorProfileImageContext
} from "./../../../context/vendor/VendorMyProfile";
import { getVendors, getProfileProgress, getDocumentStatus, getSocialAccounts } from "./../../../apis/vendor";

interface IVendorProfileContainerProps { }

interface IVendorProfileContainerState { }

const initialState = {};

const VendorProfileContainer: React.FC<IVendorProfileContainerProps> = (
  props
) => {
  // Api For SelectBox Data
  const authorizationToken = AuthService.accessToken;
  const userId = AuthService.currentUser?.id;

  const myProfileDispatcher = useMyProfileDispatcher();
  const myProfileContext = useMyProfileContext();
  const { myProfile, basicInfo, loggedUserId } = myProfileContext;

   const vendorId = loggedUserId;

  console.log("userId", userId, "vendorId", vendorId);

  const [
    VendorExperienceSelectBoxData,
    setVendorExperienceSelectBoxData,
  ] = useState({});

  const [
    VendorRelativeSelectBoxData,
    setVendorRelativeSelectBoxData,
  ] = useState({});

  const VendorContext = useVendorContext();
  const socialAccountContext = useSocialAccountContext();
  const VendorProfileImageContext = useVendorProfileImageContext();

  const { socialAccounts, getSocialAccounts } = socialAccountContext;
  const { vendorProfileImage, getVendorProfileImage } = VendorProfileImageContext;
 
  const [progressBar, setProgressBar] = useState({});
  const [documentStatus, setDocumentStatus] = useState({});
  const {getVendors,  vendorState } = VendorContext;

  // const [socialAccounts, setSocialAccounts] = useState([]);

  useEffect(() => {
    if (vendorId) {  
      getVendors({
        VendorId: Number(vendorId),
        Page: 1,
        PageSize: 10,
        SearchTerm: "",
        SortOrder: "",
        ShowInactive: false,
      });
      console.log('@@@@@@@@@@@@@@@vendorstate@@@@@@@@@ ',vendorState);
      getProfileProgress({
        VendorId: Number(vendorId),
        Page: 1,
        PageSize: 10,
        SearchTerm: "string",
        SortOrder: "string",
        ShowInactive: false,
      }).then((res) => {
        console.log('@@@@@@@@@@@@@@@vendorstate selected candidate@@@@@@@@@ ',res.data);
        setProgressBar(res.data);
      }).catch((err) => {
        console.log(err);
      });

      getDocumentStatus({
        VendorId: Number(vendorId),
        Page: 1,
        PageSize: 10,
        SearchTerm: "string",
        SortOrder: "string",
        ShowInactive: false,
      }).then((res) => {
        console.log('@@@@@@@@@@@@@@@uploaded-document-status@@@@@@@@@ ',res.data);
        setDocumentStatus(res.data);
      }).catch((err) => {
        console.log(err);
      });

      getSocialAccounts({
        VendorId: Number(vendorId),
        Page: 1,
        PageSize: 10,
        SearchTerm: "string",
        SortOrder: "string",
        ShowInactive: false,
      });

      if (vendorProfileImage.data.length <= 0) {
        getVendorProfileImage({
          VendorId: Number(vendorId),
          Page: 1,
          PageSize: 10,
          SearchTerm: "string",
          SortOrder: "string",
          ShowInactive: false,
        });
      }


    }
  }, [vendorId]);

  React.useEffect(() => {
    (async () => {
      await getMyProfileSelectBoxList(myProfileDispatcher);
    })();
  }, [loggedUserId]);

  React.useEffect(() => {
    if (userId && authorizationToken !== null)
      (async () => {
        await getLoggedUserId(
          myProfileDispatcher,
          Number(userId),
          authorizationToken
        );
      })();
  }, [userId]);


  return (
    <>
      <VendorProfileComponent
        myProfile={myProfile}
        userId={userId}
        vendorId={vendorId}
        vendorState= {vendorState}
        getVendors={getVendors}
        progressBar={progressBar}
        documentStatus={documentStatus}
        socialAccounts={socialAccounts}
        getSocialAccounts={getSocialAccounts}
        getVendorProfileImage={getVendorProfileImage}
        vendorProfileImage={vendorProfileImage}
      />
    </>
  );
};
export default VendorProfileContainer;
