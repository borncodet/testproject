import React from "react";
import VendorLayoutComponent from "./VendorLayoutComponent";
import { VendorContextProvider } from "./../../../context/vendor/VendorMyProfile";
import VendorLayoutSidebar from "./VendorLayoutSidebar";
import {
  getLoggedUserId,
  getMyProfileBasicInfo,
  getProfileImage,
  useMyProfileContext,
  useMyProfileDispatcher,
} from "../../../action/MyProfileVendorAction";
import AuthService from "../../../services/AuthService";
import {
  MyProfileSelectBoxDataViewModel,
  profileImageRequestModel
} from "../../../models/vendor/MyProfileSelectBoxData";

interface IVendorLayoutContainerProps {}

interface IVendorLayoutContainerState {}

const initialState = {};

const VendorLayoutContainer: React.FC<IVendorLayoutContainerProps> = (
  props
) => {
  const [
    vendorLayoutContainerState,
    setVendorLayoutContainerState,
  ] = React.useState<IVendorLayoutContainerState>(initialState);

  const authorizationToken = AuthService.accessToken;
  const myProfileDispatcher = useMyProfileDispatcher();
//   const myProfileContext = useMyProfileContext();
//   const {
//     loggedUserId,
//   } = myProfileContext;
//   let user = AuthService.currentUser;

//   React.useEffect(() => {
//     if (authorizationToken != null && user?.id != null)
//       (async () => {
//         await getLoggedUserId(
//           myProfileDispatcher,
//           parseInt(user.id),
//           authorizationToken
//         );
//       })();
//   }, [authorizationToken, user]);

//   React.useEffect(() => {
//     if (authorizationToken != null)
//       (async () => {
//         await getMyProfileBasicInfo(myProfileDispatcher, authorizationToken);
//        })();
//   });

// console.log('loggedUserIdloggedUserIdloggedUserId',loggedUserId);
// React.useEffect(() => {
//     if (authorizationToken != null && loggedUserId!= 0 ) {
//       (async () => {
//         await getProfileImage(
//           myProfileDispatcher,
//           {
//             vendorId: loggedUserId,
//             page: 1,
//             pageSize: 10,
//             searchTerm: "",
//             showInactive: false,
//             sortOrder: "",
//           } as profileImageRequestModel,
//           authorizationToken
//         );
//       })();
//     }
//   }, [loggedUserId]);

  return (
    <div id="wrapper">
      <VendorContextProvider>
         <VendorLayoutComponent />
      </VendorContextProvider>
    </div>
  );
};
export default VendorLayoutContainer;
