import React from "react";
import { Switch, Route, useRouteMatch, withRouter } from "react-router-dom";
import VendorLayoutSidebar from "./VendorLayoutSidebar";
import VendorDashboardContainer from "../dashboard/VendorDashBoardContainer";
import VendorMessageContainer from "../message/VendorMessageContainer";
import VendorMessageComponent from "../message/VendorMessageComponent";
import VendorProfileContainer from "./../my_profile/VendorProfileContainer";
import { MyProfileVendorContextProvider } from "../../../context/MyProfileVendorContext";
import {
  SocialAccountContextProvider,
  VendorProfileImageContextProvider
} from "./../../../context/vendor/VendorMyProfile";

interface IVendorLayoutComponentProps {}

interface IVendorLayoutComponentState {}

const initialState = {};

const VendorLayoutComponent: React.FC<IVendorLayoutComponentProps> = props => {
  const [
    vendorLayoutComponentState,
    setVendorLayoutComponentState
  ] = React.useState<IVendorLayoutComponentState>(initialState);

  const { path, url } = useRouteMatch();

  return (
    <div id="wrapper">
      <VendorLayoutSidebar />
      <Switch>
        <Route exact path={`${path}`}>
          <VendorDashboardContainer />
        </Route>
        <Route exact path={`${path}/messages`}>
          <VendorMessageContainer />
        </Route>
        <Route exact path={`${path}/my-profile`}>
          <MyProfileVendorContextProvider>
            <SocialAccountContextProvider>
              <VendorProfileContainer />
            </SocialAccountContextProvider>
          </MyProfileVendorContextProvider>
        </Route>
      </Switch>
    </div>
  );
};
export default withRouter(VendorLayoutComponent);
