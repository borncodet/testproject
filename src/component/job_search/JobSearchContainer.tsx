import React from "react";

import { BrowserRouter } from "react-router-dom";
import { valueCaptionPair } from "../../models/General";
import JobSearchComponent from "./JobSearchComponent";

interface IJobSearchContainerProps { }

interface IJobSearchContainerState { 
  jobCategorys:valueCaptionPair[];
}

const initialState = {
  jobCategorys:[ {
    label: "Java",
    value: "java"
  }, {
    label: "JS",
    value: "js"
  }],
};

const JobSearchContainer: React.FC<IJobSearchContainerProps> = (props) => {
  const [JobSearchContainerState, setJobSearchContainerState] = React.useState<IJobSearchContainerState>(
    initialState
  );
const {jobCategorys}=JobSearchContainerState



  return (
    <>
    <JobSearchComponent
    jobCategorys={jobCategorys}
    />
    </>
  );
};
export default JobSearchContainer;
