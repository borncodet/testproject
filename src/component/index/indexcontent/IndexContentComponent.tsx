import { ErrorMessage } from "@hookform/error-message";
import React, { useState } from "react";
import { Modal } from "react-bootstrap";
import { Controller, useForm } from "react-hook-form";
import { Link, Redirect, useHistory } from "react-router-dom";
import {
  getNewlyPostedJobList,
  useNewlyPostedJobContext,
  useNewlyPostedJobDispatcher,
} from "../../../action/general/NewlyPostedJobAction";
import {
  getSearchListWithOutToken,
  getSearchListWithToken,
  useSearchContext,
  useSearchDispatcher,
} from "../../../action/general/SearchAction";
import { useMyProfileContext } from "../../../action/MyProfileAction";
import { newlyPostedJobRequestModel } from "../../../models/general/NewlyPostedJob";
import {
  searchDataWithOutTokenRequestModel,
  searchDataWithTokenRequestModel,
} from "../../../models/general/Search";
import AuthService from "../../../services/AuthService";
import SelectOption from "../../candidate/my_profile/components/SelectOption";

interface ReactSelectOption {
  value: string;
  label: string;
}
interface IIndexContentComponentProps { }

interface IIndexContentComponentState {
  jobTitle: string;
  location: string;
  type: string;
  jobTypeData: string;
}

const defaultValues = {
  jobTitle: "",
  location: "",
  type: "",
  jobTypeData: " ",
};

const IndexContentComponent: React.FC<IIndexContentComponentProps> = (
  props
) => {
  const [
    IndexContentComponentState,
    setIndexContentComponentState,
  ] = React.useState<IIndexContentComponentState>(defaultValues);

  let jobCategoryData: ReactSelectOption[] = [];
  let jobTypeDatas: ReactSelectOption[] = [];

  const authorizationToken = AuthService.accessToken;
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const location = [
    {
      label: "Java",
      value: "java",
    },
    {
      label: "JS",
      value: "js",
    },
    {
      label: "",
      value: "",
    },
  ];

  const type = [
    {
      label: "Java",
      value: "java",
    },
    {
      label: "JS",
      value: "js",
    },
    {
      label: "",
      value: "",
    },
  ];

  const newlyPostedJobDispatcher = useNewlyPostedJobDispatcher();
  const newlyPostedJobContext = useNewlyPostedJobContext();
  const { newlyPostedJob } = newlyPostedJobContext;

  const searchDispatcher = useSearchDispatcher();
  const searchContext = useSearchContext();
  const { searchWithToken, searchWithOutToken } = searchContext;

  const myProfileContext = useMyProfileContext();
  const { myProfile, loggedUserId } = myProfileContext;

  React.useEffect(() => {
    (async () => {
      await getNewlyPostedJobList(newlyPostedJobDispatcher, {
        Page: 1,
        PageSize: 10,
        SearchTerm: "",
        SortOrder: "",
      } as newlyPostedJobRequestModel);
    })();
  }, []);

  let history = useHistory();

  // React.useEffect(() => {
  //   if(1){
  //   (async () => {
  //       await getSearchListWithOutToken(searchDispatcher,{expereince:[],location:[''],
  //       title:[''],type:[''],lastDays:[],pageIndex:1,pageSize:10,showInactive:true
  //     } as searchDataWithOutTokenRequestModel)

  //   })();
  // }
  // }, [])

  // if (!newlyPostedJob || !newlyPostedJob.Data || newlyPostedJob.Data.length <= 0) {
  //   return (
  //     <>
  //       <h1>Loding...</h1>
  //       </>
  //   );
  // }

  const {
    register,
    handleSubmit,
    watch,
    errors,
    setValue,
    getValues,
    control,
  } = useForm<IIndexContentComponentState>({
    defaultValues,
  });

  const onSubmit = (data: any) => {
    authorizationToken != null
      ? getSearchListWithToken(
        searchDispatcher,
        {
          expereince: [],
          location: [data.location],
          title: [data.jobTitle],
          type: data.types != null ? [data.types] : [],
          lastDays: [],
          candidateId: loggedUserId,
          pageIndex: 1,
          pageSize: 60,
          showInactive: false,
        } as searchDataWithOutTokenRequestModel,
        authorizationToken
      )
      : getSearchListWithOutToken(searchDispatcher, {
        expereince: [],
        location: [data.location],
        title: [data.jobTitle],
        type: data.types != null ? [data.types] : [],
        lastDays: [],
        candidateId: 0,
        pageIndex: 1,
        pageSize: 60,
        showInactive: false,
      } as searchDataWithOutTokenRequestModel);

    history.push("/job_search/0");
  };

  const handleRedirect = () => {
    authorizationToken != null
      ? getSearchListWithToken(
        searchDispatcher,
        {
          expereince: [],
          location: [""],
          title: [""],
          type: [],
          lastDays: [],
          candidateId: loggedUserId,
          pageIndex: 1,
          pageSize: 60,
          showInactive: false,
        } as searchDataWithOutTokenRequestModel,
        authorizationToken
      )
      : getSearchListWithOutToken(searchDispatcher, {
        expereince: [],
        location: [""],
        title: [""],
        type: [],
        lastDays: [],
        candidateId: 0,
        pageIndex: 1,
        pageSize: 60,
        showInactive: false,
      } as searchDataWithOutTokenRequestModel);

    history.push("/job_search/0");
  };

  return (
    <>
      <div className="home_banner">
        <div className="banner_con">
          <div className=" container">
            <div className="row">
              <div className="col-sm-12">
                <div className="banner_heading">
                  Upscale your <span>skills</span>
                </div>
                <div className="banner_content">
                  to get you ready for your dream job
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="banner_border">
          <img
            src={require("../../../images/banner_sh.png")}
            //  src="images/banner_sh.png"
            className=" img-responsive"
          />
        </div>
        <div className="clearfix" />
      </div>
      <div className="categories">
        <div className=" container">
          <div className="row">
            <div className="col-sm-6 col-lg-3">
              <a
                onClick={
                  authorizationToken != null
                    ? () => {
                      history.push("/candidate/my-resume");
                    }
                    : () => {
                      setIsOpen(!isOpen);
                    }
                }
                data-target="#didgilocker"
                data-toggle="modal"
              >
                <div className="categories_sec">
                  <div>
                    <img
                      src={require("../../../images/resume_builder.png")}
                      // src="images/resume_builder.png"
                      className="center-block"
                    />
                  </div>
                  <h1>Resume Builder</h1>
                  <p>
                    Build your resume and design a cover letter in quick time
                    with our RESUME BUILDER
                  </p>
                </div>
              </a>
            </div>
            <div className="col-sm-6 col-lg-3">
              <a
                onClick={
                  authorizationToken != null
                    ? () => {
                      history.push("/candidate/digilocker");
                    }
                    : () => {
                      setIsOpen(!isOpen);
                    }
                }
                data-toggle="modal"
              >
                <div className="categories_sec">
                  <div>
                    <img
                      src={require("../../../images/digi_locker.png")}
                      // src="images/digi_locker.png"
                      className="center-block"
                    />
                  </div>
                  <h1>Digi Locker</h1>
                  <p>
                    On the go, anytime, anywhere access to your documents. Keep
                    your personal and professional documents secured and manage
                    their access level.
                  </p>
                </div>
              </a>
            </div>
            <div className="col-sm-6 col-lg-3">
              <a
                onClick={
                  authorizationToken != null
                    ? () => {
                      history.push("/");
                    }
                    : () => {
                      setIsOpen(!isOpen);
                    }
                }
                data-target="#didgilocker"
                data-toggle="modal"
              >
                <div className="categories_sec">
                  <div>
                    <img
                      src={require("../../../images/career_developer.png")}
                      // src="images/career_developer.png"
                      className="center-block"
                    />
                  </div>
                  <h1>Career Developer</h1>
                  <p>
                    Our Career Developer platform helps you strengthen your
                    professional pursuit and provides training into vast fields
                    of technology and management
                  </p>
                </div>
              </a>
            </div>
            <div className="col-sm-6 col-lg-3">
              <a onClick={handleRedirect}>
                <div className="categories_sec">
                  <div>
                    <img
                      src={require("../../../images/apply_jobs.png")}
                      // src="images/apply_jobs.png"
                      className="center-block"
                    />
                  </div>
                  <h1>Apply Jobs</h1>
                  <p>
                    {" "}
                    JIT Career is an ocean of opportunities for you to sail
                    through it and make a mark
                  </p>
                </div>
              </a>
            </div>
          </div>
        </div>
        {/* BEGIN MODAL */}
        <div className="modal fade none-border" id="didgilocker">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <button
                  aria-hidden="true"
                  data-dismiss="modal"
                  className="close"
                  type="button"
                >
                  ×
                </button>
                <h4 className="modal-title">
                  Login as candidate to use this feature.
                </h4>
              </div>
              <div className="col-sm-12 m_t_30 text-center">
                <button className="btn btn-success " type="button">
                  Login Now
                </button>
                <button
                  type="button"
                  className="btn btn-danger"
                  data-dismiss="modal"
                >
                  Cancel
                </button>
              </div>
              <div className="modal-footer  m-t-30"></div>
            </div>
          </div>
        </div>
        {/* END MODAL */}
        <div className=" container">
          <div className="carees_search">
            <div className="row">
              <form onSubmit={handleSubmit(onSubmit)} noValidate>
                <div className="search_forms">
                  <div className="cr_serach_br">
                    <input
                      name="jobTitle"
                      ref={register({ required: "Job Title is required" })}
                      type="text"
                      className="form-control"
                      placeholder="Job Title"
                    />

                    <div className="search_icons">
                      <img
                        src={require("../../../images/search_icon.png")}
                      //  src="images/search_icon.png"
                      />
                    </div>
                  </div>
                  <ErrorMessage
                    errors={errors}
                    name="jobTitle"
                    render={({ message }) => (
                      <div
                        style={{ marginLeft: 70 }}
                        className="register_validation"
                      >
                        {message}
                      </div>
                    )}
                  />
                </div>

                <div className="search_forms">
                  <div className="cr_serach_br">
                    <span className="select-search">
                      <input
                        name="location"
                        ref={register({ required: false })}
                        type="text"
                        className="form-control"
                        placeholder="location"
                      />
                    </span>

                    <div className="search_icons">
                      <img
                        src={require("../../../images/locattion_icon.png")}
                      // src="images/locattion_icon.png"
                      />
                    </div>
                  </div>
                </div>
                <div className="search_forms">
                  <div className="cr_serach_br">
                    <span className="select-search">
                      <Controller
                        control={control}
                        name="jobTypeData"
                        render={({ onChange, onBlur, value, name }) => (
                          <SelectOption
                            values={
                              myProfile.jobTypes != undefined
                                ? myProfile.jobTypes.map((e) => {
                                  return {
                                    value: e["value"],
                                    label: e["caption"],
                                  };
                                })
                                : jobTypeDatas
                            }
                            disabled={false}
                            onChange={onChange}
                            onBlur={onBlur}
                            value={value}
                            name={name}
                          />
                        )}
                      />
                      {/* <span className="holder">Type</span> */}
                    </span>
                    <div className="search_icons">
                      <img
                        src={require("../../../images/type_icon.png")}
                      // src="images/type_icon.png"
                      />
                    </div>
                  </div>
                </div>
                <div className="search_button">
                  <div className="search">
                    <button className="CustomButtonCss" type="submit">
                      Search
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div className="how_its_work">
        <div className=" container">
          <div className="row">
            <div className="col-sm-12 col-lg-12">
              <h1>How It Works</h1>
            </div>
            <div className="col-sm-3">
              <div className="how_its_work_br">
                <img
                  src={require("../../../images/how_its_work_br.png")}
                // src="images/how_its_work_br.png"
                />
              </div>
              <div>
                <img
                  src={require("../../../images/how_its_work1.png")}
                  // src="images/how_its_work1.png"
                  className="center-block"
                />
              </div>
              <h2>
                Register with <br />
                JIT Career
              </h2>
            </div>
            <div className="col-sm-3">
              <div className="how_its_work_br1">
                <img
                  //  src="images/how_its_work_br1.png"
                  src={require("../../../images/how_its_work_br1.png")}
                />
              </div>
              <div>
                <img
                  src={require("../../../images/how_its_work2.png")}
                  // src="images/how_its_work2.png"
                  className="center-block"
                />
              </div>
              <h2>Get free training with our career development platform.</h2>
            </div>
            <div className="col-sm-3">
              <div className="how_its_work_br">
                <img
                  src={require("../../../images/how_its_work_br.png")}
                // src="images/how_its_work_br.png"
                />
              </div>
              <div>
                <img
                  src={require("../../../images/how_its_work3.png")}
                  // src="images/how_its_work3.png"
                  className="center-block"
                />
              </div>
              <h2>
                Build a resume and
                <br />
                apply jobs
              </h2>
            </div>
            <div className="col-sm-3">
              <div>
                <img
                  src={require("../../../images/how_its_work4.png")}
                  // src="images/how_its_work4.png"
                  className="center-block"
                />
              </div>
              <h2>Get placed with the best employer for you</h2>
            </div>
          </div>
        </div>
      </div>
      <div className="video_sec">
        <div className=" container">
          <div className="row">
            <div className="col-sm-12 col-lg-12">
              <h1>JIT Career Developer: the learning platform</h1>
              <p>
                Team of professionals in the training and placement <br />
                industry sharing their insights.
              </p>
            </div>
            <div className="col-sm-8 col-sm-offset-2">
              <div className="video_sec_br">
                <div className="video_sec_br1">
                  <a href="#">
                    <img
                      src={require("../../../images/video_img.jpg")}
                      // src="images/video_img.jpg"
                      className=" img-responsive center-block"
                    />
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="career_tips">
        <Link to="/career_tips">
          <div className=" container">
            <div className="row">
              <div className="col-sm-12 col-lg-12">
                <h1>Career Tips</h1>
                <p>
                  Love the job you have? Good—
                  <br />
                  keep looking at other jobs anyway.
                </p>
              </div>
            </div>
          </div>
        </Link>
      </div>
      <div className="vender_h">
        <div className=" container">
          <div className="row">
            <div className="col-sm-12 col-lg-10  col-lg-offset-1">
              <h1>Vendor</h1>
              <div className="vender_sec">
                <div className="row">
                  <div className="col-sm-5">
                    <div className="vender_icon">
                      <img
                        src={require("../../../images/vendor_partner.png")}
                        // src="images/vendor_partner.png"
                        className="center-block"
                      />
                    </div>
                    <h2>Partner With JIT</h2>
                    <p>Grow your business with JIT</p>
                    <div className="view_more">
                      <a href="#">View More</a>
                    </div>
                  </div>
                  <div className="col-sm-2">
                    <div className="vender_br">
                      <img
                        src={require("../../../images/vendor_sec.png")}
                        // src="images/vendor_sec.png"
                        className="img-responsive center-block"
                      />
                    </div>
                  </div>
                  <div className="col-sm-5">
                    <div className="vender_icon">
                      <img
                        src={require("../../../images/vendor_dashboard.png")}
                        // src="images/vendor_dashboard.png"
                        className="center-block"
                      />
                    </div>
                    <h2>Dashboard</h2>
                    <p>One stop solution for you</p>
                    <div className="view_more">
                      <a href="#">View More</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="newly_posted">
        <div className=" container">
          <div className="row">
            <div className="col-sm-12 col-lg-12">
              <h1>Newly Posted jobs</h1>
              <p>Leading Employers already using job and talent.</p>
              {!newlyPostedJob ||
                !newlyPostedJob.data ||
                newlyPostedJob.data.length <= 0 ? (
                  <h1>loading..</h1>
                ) : (
                  newlyPostedJob.data.slice(0, 6).map((job, index) => {
                    return (
                      <div className="newly_posted_sec" key={index}>
                        <a
                          onClick={() => history.push(`/job_search/${job.rowId}`)}
                        >
                          <div className="col-sm-1 col-xs-3">
                            <img
                              src={require("../../../images/jobs_icon.png")}
                              // src="images/jobs_icon.png"
                              className="center-block img-responsive"
                            />
                          </div>
                          <div className="col-sm-8 col-xs-10">
                            <h2>{job.title}</h2>
                            <div className="newly_company">{job.industry}</div>
                            <div className="newly_place">
                              <img
                                src={require("../../../images/job_-place.png")}
                              // src="images/job_-place.png"
                              />{" "}
                            Ukraine
                          </div>
                          </div>
                          <div className="col-sm-3">
                            <div
                              className={`${
                                job.jobType != "Permanent"
                                  ? "parttime"
                                  : "fulltime"
                                }`}
                            >
                              {job.jobType}
                            </div>
                            <div className="clearfix" />
                            <div className="posted_time">{job.postedDate}</div>
                          </div>
                        </a>
                      </div>
                    );
                  })
                )}

              <div className="view_more1">
                <a onClick={handleRedirect}>View More Jobs</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="clients_h">
        <div className=" container">
          <div className="row">
            <div className="col-sm-12 ">
              <h1>Clients</h1>
            </div>
            <div className="col-sm-12 col-lg-6">
              <div className="clients_sec">
                <div className="row">
                  <div className="col-sm-2">
                    <img
                      src={require("../../../images/client_partner.png")}
                      // src="images/client_partner.png"
                      className="center-block"
                    />
                  </div>
                  <div className="col-sm-6">
                    <h2>Partner With JIT</h2>
                    <p>Let JIT find right resources for your company</p>
                  </div>
                  <div className="col-sm-4">
                    <div className="view_more2">
                      <a href="#">View More</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-12 col-lg-6">
              <div className="clients_sec">
                <div className="row">
                  <div className="col-sm-2">
                    <img
                      src={require("../../../images/client_dash.png")}
                      // src="images/client_dash.png"
                      className="center-block"
                    />
                  </div>
                  <div className="col-sm-6">
                    <h2>Dashboard</h2>
                    <p>One stop solution for you</p>
                  </div>
                  <div className="col-sm-4">
                    <div className="view_more2">
                      <a href="#">View More</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="achievements">
        <div className=" container">
          <div className="row">
            <div className="col-sm-12 ">
              <h1>JIT Achievements</h1>
            </div>
            <div className="col-sm-4">
              <div className="achievements_sec">
                <div className="achieve_img">
                  <img
                    src={require("../../../images/technology.jpg")}
                    // src="images/technology.jpg"
                    className="img-responsive"
                  />
                </div>
                <h2>Technology </h2>
                <p>
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry
                </p>
                <div className="achieve_date">
                  {" "}
                  <img
                    src={require("../../../images/achive_date.png")}
                  // src="images/achive_date.png"
                  />{" "}
                  June 27, 2020{" "}
                </div>
                <div className="achieve_more">
                  <a href="#">
                    <img
                      src={require("../../../images/achive_more.png")}
                    // src="images/achive_more.png"
                    />{" "}
                    Read More{" "}
                  </a>
                </div>
              </div>
            </div>
            <div className="col-sm-4">
              <div className="achievements_sec">
                <div className="achieve_img">
                  <img
                    src={require("../../../images/ecnomy.jpg")}
                    //  src="images/ecnomy.jpg"
                    className="img-responsive"
                  />
                </div>
                <h2>Economy </h2>
                <p>
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry
                </p>
                <div className="achieve_date">
                  {" "}
                  <img
                    src={require("../../../images/achive_date.png")}
                  // src="images/achive_date.png"
                  />{" "}
                  June 27, 2020{" "}
                </div>
                <div className="achieve_more">
                  <a href="#">
                    <img
                      src={require("../../../images/achive_more.png")}
                    //  src="images/achive_more.png"
                    />{" "}
                    Read More{" "}
                  </a>
                </div>
              </div>
            </div>
            <div className="col-sm-4">
              <div className="achievements_sec">
                <div className="achieve_img">
                  <img
                    src={require("../../../images/business.jpg")}
                    // src="images/business.jpg"
                    className="img-responsive"
                  />
                </div>
                <h2>Technology </h2>
                <p>
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry
                </p>
                <div className="achieve_date">
                  {" "}
                  <img
                    src={require("../../../images/achive_date.png")}
                  // src="images/achive_date.png"
                  />{" "}
                  June 27, 2020{" "}
                </div>
                <div className="achieve_more">
                  <a href="#">
                    <img
                      src={require("../../../images/achive_more.png")}
                    // src="images/achive_more.png"
                    />{" "}
                    Read More{" "}
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="associations">
        <div className="container ">
          <div className="row">
            <div className="col-sm12">
              <h1>Our Associations</h1>
            </div>
          </div>
          <div className="our_client_logos">
            <div className="col-sm-12 ">
              <div
                id="owl-example"
                className="owl-carousel owl-theme"
                style={{ opacity: 1, display: "block" }}
              >
                <div className="owl-wrapper-outer">
                  <div
                    className="owl-wrapper"
                    style={{ width: "3600px", left: "0px", display: "block" }}
                  >
                    <div className="owl-item" style={{ width: "300px" }}>
                      <div className="item">
                        <img
                          src={require("../../../images/clients1.jpg")}
                          alt="Clients"
                          className="center-block"
                        />
                      </div>
                    </div>
                    <div className="owl-item" style={{ width: "300px" }}>
                      <div className="item">
                        <img
                          src={require("../../../images/clients2.jpg")}
                          alt="Clients"
                          className="center-block"
                        />
                      </div>
                    </div>
                    <div className="owl-item" style={{ width: "300px" }}>
                      <div className="item">
                        <img
                          src={require("../../../images/clients3.jpg")}
                          alt="Clients"
                          className="center-block"
                        />
                      </div>
                    </div>
                    <div className="owl-item" style={{ width: "300px" }}>
                      <div className="item">
                        <img
                          src={require("../../../images/clients4.jpg")}
                          alt="Clients"
                          className="center-block"
                        />
                      </div>
                    </div>
                    <div className="owl-item" style={{ width: "300px" }}>
                      <div className="item">
                        <img
                          src={require("../../../images/clients1.jpg")}
                          alt="Clients"
                          className="center-block"
                        />
                      </div>
                    </div>
                    <div className="owl-item" style={{ width: "300px" }}>
                      <div className="item">
                        <img
                          src={require("../../../images/clients2.jpg")}
                          alt="Clients"
                          className="center-block"
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="owl-controls clickable">
                  <div className="owl-pagination">
                    <div className="owl-page active">
                      <span className="" />
                    </div>
                    <div className="owl-page">
                      <span className="" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="clearfix" />
      </div>
      <div className="newsletter_sec">
        <div className="container ">
          <div className="row">
            <div className="col-sm-3 col-sm-offset-1">
              <h1>Newsletter SignUp</h1>
              <p>Subscribe to get latest newsletter. </p>
            </div>
            <div className="col-sm-7  col-sm-offset-1">
              <div className="newsletter">
                <div className="newsletter_form">
                  <input
                    name=""
                    type="text"
                    className="form-control"
                    placeholder="Enter Email"
                  />
                </div>
                <div className="newsletter_btn">
                  <a href="#">Subscribe</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="clearfix" />

        <Modal
          show={isOpen}
          onHide={() => {
            setIsOpen(!isOpen);
          }}
        >
          <Modal.Header closeButton>
            <Modal.Title>Login as candidate to use this feature.</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="col-sm-12 m_t_30 text-center">
              <button
                className="btn btn-success "
                type="button"
                onClick={() => {
                  history.push("/login");
                }}
              >
                Login Now
              </button>
              <button
                type="button"
                className="btn btn-danger"
                data-dismiss="modal"
                onClick={() => {
                  setIsOpen(!isOpen);
                }}
              >
                Cancel
              </button>
            </div>
          </Modal.Body>
        </Modal>
      </div>
    </>
  );
};
export default IndexContentComponent;
