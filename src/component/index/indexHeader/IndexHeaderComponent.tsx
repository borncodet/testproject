import React, { useRef, useState } from "react";
import { Link, useRouteMatch, withRouter } from "react-router-dom";
import { useMyProfileContext } from "../../../action/MyProfileAction";
import AuthService from "./../../../services/AuthService";
import { useHistory } from 'react-router-dom';
import { useCandidateProfileImageContext } from "../../../context/candidate/CandidateMyProfile";

interface IIndexHeaderComponentProps {}

interface IIndexHeaderComponentState {}

const initialState = {};

const IndexHeaderComponent: React.FC<IIndexHeaderComponentProps> = (
  props: any
) => {
  const [
    IndexHeaderComponentState,
    setIndexHeaderComponentState,
  ] = React.useState<IIndexHeaderComponentState>(initialState);

  const myProfileContext = useMyProfileContext();
  const { myProfile, loggedUserId, profileImage, basicInfo } = myProfileContext;

  const { path, url } = useRouteMatch();
  const history = useHistory();

  const candidateProfileImageContext = useCandidateProfileImageContext();

  const { candidateProfileImage, getCandidateProfileImage } = candidateProfileImageContext;
  
  const authorizationToken = AuthService.accessToken;
  const user = AuthService.currentUser;
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const onLogout = () => {
    AuthService.logout();
    props.history.push("/");
  };

  // menu close onClick outside
  const menuRef = useRef<any>();
  
  React.useEffect(() => {
   const handler= (event:any)=>{
      if(menuRef.current!=undefined){
      if(menuRef && menuRef.current && !menuRef.current.contains(event.target)){
      setIsOpen(false);
      }
    }
    }

    document.addEventListener('mousedown',handler)
    return()=>{
document.removeEventListener('mousedown',handler)
    }
  }, []);

  console.log(44444444444444444444, profileImage);
  console.log(3333333333333333, candidateProfileImage)

  const candidateId = loggedUserId;
  
  React.useEffect(() => {
    if (candidateId) {
      getCandidateProfileImage({
        CandidateId: Number(candidateId),
        Page: 1,
        PageSize: 10,
        SearchTerm: "string",
        SortOrder: "string",
        ShowInactive: false,
      });
    }
  }, [candidateId]);

  return (
    <>
      <div className="header">
        <header>
          <div className="top_sec">
            <div className=" container">
              <div className="row">
                <div className="col-sm-6">
                  <div className="top_media">
                    <a href="#">
                      <i className="fa fa-facebook" aria-hidden="true" />
                    </a>
                    <a href="#">
                      <i className="fa fa-twitter" aria-hidden="true" />
                    </a>
                    <a href="#">
                      <i className="fa fa-instagram" aria-hidden="true" />
                    </a>
                    <a href="#">
                      <i className="fa fa-youtube-play" aria-hidden="true" />
                    </a>
                    <a href="#">
                      <i className="fa fa-phone" aria-hidden="true" /> 086 834
                      2525
                    </a>
                    <a href="#">
                      <i className="fa fa-envelope-o" aria-hidden="true" />{" "}
                      info@jit.com
                    </a>
                  </div>
                </div>
                <div className="col-sm-6">
                  <div className="language_select">
                    <span className="select-wrapper-lan">
                      <select name="timepass" className="laguage">
                        <option value="">Language</option>
                        <option value="">Dummy</option>
                        <option value="">Dummy</option>
                      </select>
                      <span className="holder">Language</span>
                    </span>
                  </div>
                  {/* <div className="top_links">
                    {authorizationToken ? <a onClick={onLogout} className="_cursor-pointer">Logout</a> :
                      <>
                        <Link to='/login'>Login</Link>
                        <Link to='/registration'>Register</Link>
                      </>}
                  </div> */}

                  {authorizationToken ? (
                    <>
                      <div
                        className="action_btn1"
                        onClick={() => {
                          setIsOpen(!isOpen);
                        }}
                      >
                        <button type="button" className="jobs_action">
                          {basicInfo.fullName != undefined
                            ? basicInfo.fullName
                            : ""}{" "}
                          <i className="fa fa-angle-down" aria-hidden="true" />
                        </button>
                        <div className="login_jobs">
                          {/* <img
                            src={
                              profileImage != null && profileImage.total > 0
                                ? `https://localhost:44361/Upload/ProfileImage/${profileImage.data[0].imageUrl}`
                                : require("../../../images/profileDefault1.jpg")
                            }
                          /> */}
                          {candidateProfileImage.data.length > 0 ?
              <img className="img-responsive" src={`https://localhost:44361/Upload/ProfileImage/${candidateProfileImage.data[0]["imageUrl"]}`} alt="" /> :
              <img className="img-responsive" src={require("./../../../images/profileDefault1.jpg")}></img>
            }
                        </div>

                        <div
                          className="jobs_action_dp"
                          tabIndex={-1}
                          style={{ display: `${isOpen ? "block" : "none"}` }}
                        >
                          <div  className="action_sec">
                            <ul ref={menuRef}>
                              <li>
                                <Link to="/candidate" className="active">
                                  {" "}
                                  <i
                                    className="fa fa-th-large"
                                    aria-hidden="true"
                                  />
                                  Dashboard{" "}
                                </Link>
                              </li>
                              <li>
                                <Link
                                  to="/candidate/jobs-applied"
                                  className="active"
                                >
                                  {" "}
                                  <i
                                    className="fa fa-briefcase"
                                    aria-hidden="true"
                                  />
                                  Job Applied{" "}
                                </Link>
                              </li>
                              <li>
                                <Link
                                  to="/candidate/digilocker/3"
                                  className="active"
                                >
                                  {" "}
                                  <i
                                    className="fa fa-lock"
                                    aria-hidden="true"
                                  />
                                  {basicInfo.fullName != undefined
                                    ? basicInfo.fullName.split(" ")[0]
                                    : ""}
                                  's DigiLocker{" "}
                                </Link>
                              </li>
                              <li>
                                <Link
                                  to="/candidate/my-resume"
                                  className="active"
                                >
                                  {" "}
                                  <i
                                    className="fa fa-files-o"
                                    aria-hidden="true"
                                  />
                                  My Resumes{" "}
                                </Link>
                              </li>
                              <li>
                                <Link
                                  to="/candidate/messages"
                                  className="active"
                                >
                                  {" "}
                                  <i
                                    className="fa fa-commenting-o"
                                    aria-hidden="true"
                                  />
                                  Messages{" "}
                                </Link>
                              </li>
                              <li>
                                <Link
                                  to="/candidate/saved-jobs"
                                  className="active"
                                >
                                  {" "}
                                  <i
                                    className="fa fa-suitcase"
                                    aria-hidden="true"
                                  />
                                  Saved Jobs{" "}
                                </Link>
                              </li>
                              <li>
                                <Link
                                  to="/candidate/my-profile"
                                  className="active"
                                >
                                  {" "}
                                  <i
                                    className="fa fa-user-o"
                                    aria-hidden="true"
                                  />
                                  My Profile{" "}
                                </Link>
                              </li>
                              {/* <li>
                                <Link to="/candidate">DashBoard</Link>
                              </li> */}
                              <li>
                                <a
                                  onClick={onLogout}
                                  className="_cursor-pointer"
                                >
                                  <i
                                    className="fa fa-power-off"
                                    aria-hidden="true"
                                  ></i>
                                  Logout
                                </a>{" "}
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </>
                  ) : (
                    <>
                      <div className="top_links">
                        <>
                          <Link to="/login">Login</Link>
                          <Link to="/registration">Register</Link>
                        </>
                      </div>
                    </>
                  )}
                </div>
              </div>
            </div>
          </div>
          <div className=" container">
            <div className="row">
              <div className="col-sm-2">
                <div className=" logo">
                <a
                            onClick={() => {
                              history.push("/");
                            }}>
                    <img
                      src={require("../../../images/logo.png")}
                      // src="images/logo.png"
                      className="img-responsive"
                      alt=""
                    />{" "}
                  </a>
                </div>
              </div>
              <div className="col-sm-10">
                <div className="bs-example">
                  <nav
                    role="navigation"
                    className="navbar navbar-default navbar-static"
                    id="navbar-example"
                  >
                    <div className="navbar-header">
                      <button
                        data-target=".bs-example-js-navbar-collapse"
                        data-toggle="collapse"
                        type="button"
                        className="navbar-toggle"
                      >
                        {" "}
                        <span className="sr-only">Toggle navigation</span>{" "}
                        <span className="icon-bar" />{" "}
                        <span className="icon-bar" />{" "}
                        <span className="icon-bar" />{" "}
                      </button>
                      <div className="collapse navbar-collapse bs-example-js-navbar-collapse">
                        <ul className="nav navbar-nav">
                          <li>
                            <Link to="/" className="active">
                              {" "}
                              Home
                            </Link>{" "}
                          </li>
                          <li>
                            {" "}
                            <a href="#">Career Developer</a>
                          </li>
                          <li>
                            {" "}
                            <a href="#">Client </a>
                          </li>
                          <li>
                            <Link to={`/vendor`}>Vendor</Link>
                          </li>
                          <li>
                            <Link to={`/help`}>Help</Link>
                          </li>
                          <li>
                            <Link to="/about_us">About Us</Link>
                          </li>
                          <li>
                            <Link to="/contact_us">Contact Us</Link>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </nav>
                </div>
              </div>
            </div>
          </div>
        </header>
      </div>
    </>
  );
};
export default withRouter(IndexHeaderComponent);
