import React from "react";
import IndexHeaderComponent from "./IndexHeaderComponent";

interface IIndexHeaderContainerProps {}

interface IIndexHeaderContainerState {}

const initialState = {};

const IndexHeaderContainer: React.FC<IIndexHeaderContainerProps> = (props) => {
  const [
    IndexHeaderContainerState,
    setIndexHeaderContainerState,
  ] = React.useState<IIndexHeaderContainerState>(initialState);

  return (
    <>
      <IndexHeaderComponent />
    </>
  );
};
export default IndexHeaderContainer;
