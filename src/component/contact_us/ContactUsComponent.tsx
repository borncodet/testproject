import React from "react";
import { Link, withRouter } from "react-router-dom";
import { useHistory } from 'react-router-dom';

interface IContactUsComponentProps { }

interface IContactUsComponentState { }

const initialState = {};

const ContactUsComponent: React.FC<IContactUsComponentProps> = (props) => {
  const [ContactUsComponentState, setContactUsComponentState] = React.useState<IContactUsComponentState>(
    initialState
  );

  const history = useHistory();
  return (
    <>
    {/* Header Start */}

      <div className="header header1"> 
        <header> 
          <div className=" container">
            <div className="row"> 
              <div className="col-sm-2">
                <div className=" logo"><a
                            onClick={() => {
                              history.push("/");
                            }}><img 
                src={require("../../images/logo.png")}
                // src="images/logo.png" 
                className="img-responsive" alt="" /> </a></div> 
              </div> 
              <div className="col-sm-10"> 
                <div className="bs-example">
                  <nav role="navigation" className="navbar navbar-default navbar-static" id="navbar-example">
                    <div className="navbar-header">
                      <button data-target=".bs-example-js-navbar-collapse" data-toggle="collapse" type="button" className="navbar-toggle"> <span className="sr-only">Toggle navigation</span> <span className="icon-bar" /> <span className="icon-bar" /> <span className="icon-bar" /> </button>
                      <div className="collapse navbar-collapse bs-example-js-navbar-collapse">
                        <ul className="nav navbar-nav">  
                           <li><Link to='/'> Home</Link> </li>    
                          <li> <a href="#">Career Developer</a></li>
                          <li> <a href="#">Client </a></li>
                          <li> <a href="#">Vendor</a> </li>  
                          <li><Link to='/help'>Help</Link></li>
                          <li><Link to='/about_us'>About Us</Link></li>
                          <li><Link to='/contact_us'>Contact Us</Link></li>  
                        </ul> 
                      </div> 
                    </div> 
                  </nav> 
                </div> 
              </div>
            </div>
          </div>
        </header>
      </div>

      {/* Header Ends */}

{/* ----------------------------- */}

{/* baner start */}
<div className="sub_banner">  
        <div className="banner_border"><img 
        src={require("../../images/contactus_banner.jpg")}
        // src="images/contactus_banner.jpg"
         className=" img-responsive" /></div>  
        <div className="clearfix" /> 
      </div>
{/* baner end */}

{/* ------------------- */}

{/* Contact details start */}
<div className="contact_details_sec">
        <div className=" container">
          <div className="row">
            <div className="col-sm-3 col-xs-6">
              <div><i className="fa fa-map-marker" aria-hidden="true" /></div>
              <div className="contact_details_sec_con">Dummy Address</div> 
            </div>     
            <div className="col-sm-3 col-xs-6">
              <div><i className="fa fa-envelope-o" aria-hidden="true" /></div>
              <div className="contact_details_sec_con">info@jit.com</div> 
            </div> 
            <div className="col-sm-3 col-xs-6">
              <div><i className="fa fa-phone" aria-hidden="true" /></div>
              <div className="contact_details_sec_con">086 834 2525</div> 
            </div> 
            <div className="col-sm-3 col-xs-6">
              <div><i className="fa fa-whatsapp" aria-hidden="true" /></div>
              <div className="contact_details_sec_con">086 834 2525</div> 
            </div>     
          </div>
        </div>  
      </div>
{/* Contact details end  */}

{/* ------------------------------------ */}

{/* contact form section start */}
<div className="contact_form_sec">
        <div className="container">
          <div className="row">
            <div className="col-sm-12"> 
              <div className="contact_form_head"><img 
              src={require("../../images/contactus_banner.jpg")}
              // src="images/contact_br.jpg"
               width={59} height={4} /> &nbsp; Contat us</div>
              <div className="contact_form_head1">Lets Get in touch</div> 
            </div>
            <div className="col-sm-5"> 
              <div className="form-group"><input type="email" className="form-control" placeholder="Name" /></div>
              <div className="form-group"><input type="email" className="form-control" placeholder="Email ID" /></div>
              <div className="form-group"><input type="email" className="form-control" placeholder="Subject" /></div> 
              <div className="form-group"><textarea name="" 
              // cols rows 
              className="form-control form-control1" placeholder="Message" defaultValue={""} /> </div>
              <div className="form-group"><a href="#" className="btn submit_btn">Submit</a></div>
            </div>
            <div className="col-sm-7"><img 
            src={require("../../images/contact_img.jpg")}
            // src="images/contact_img.jpg" 
            className="img-responsive center-block" /></div>
          </div>
        </div>
      </div>
{/* contact form section end */}

    </>
  );
};
export default withRouter(ContactUsComponent);
