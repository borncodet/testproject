import React, { useEffect } from "react";
import LayoutContainer from "./component/layout/LayoutContainer";
import {
  withRouter,
  Redirect,
  Route,
  Switch,
  useLocation
} from "react-router-dom";
import AuthService from "./services/AuthService";
import { SearchContextProvider } from "./context/general/SearchContext";
import { JobAlertContextProvider } from "./context/candidate/JobAlertContext";
import { JobTypeContextProvider } from "./context/general/JobTypeContext";
import { ExperienceContextProvider } from "./context/general/ExperienceContext";
import {
  GlobalSpinnerContext,
  GlobalSpinnerProvider
} from "./context/GlobalSpinner";
import { DatePostedContextProvider } from "./context/general/DatePostedContext";
import { MyProfileContextProvider } from "./context/MyProfileContext";
import { JobAppliedContextProvider } from "./context/JobAppliedContext";
import { JobBookMarkContextProvider } from "./context/candidate/JobBookMarkContext";
import { DigiLockerContextProvider } from "./context/candidate/DigiLockerContext";
import GlobalSpinner from "./component/GlobalSpinner";
import { MatchedJobContextProvider } from "./context/MatchedJobContext";
import { NewlyPostedJobContextProvider } from "./context/general/NewlyPostedJobContext";
import { SuggestedJobContextProvider } from "./context/candidate/SuggestedJobContext";
import { CandidateProfileImageContextProvider } from "./context/candidate/CandidateMyProfile";

// Vendor

import { MyProfileVendorContextProvider } from "./context/MyProfileVendorContext";
import { VendorProfileImageContextProvider } from "./context/vendor/VendorMyProfile";

interface IStartUpProps {}

interface IStartUpState {}

const initialState = {};

const StartUp: React.FC<IStartUpProps> = (props: any) => {
  const [startUpState, setStartUpState] = React.useState<IStartUpState>(
    initialState
  );

  const unprotectedRoutes = [
    "/registration",
    "/login",
    "/forgot-password",
    "/forgot-password-otp",
    "/otp-verification",
    "/job-search"
  ];

  const protectedRoutes = ["/candidate", "/admin", "/employee","/vendor"];

  const authorizationToken = AuthService.accessToken;

  const location = useLocation();

  const _unprotectedRoutes = unprotectedRoutes.filter(e => {
    return location.pathname.startsWith(e);
  });

  const _protectedRoutes = protectedRoutes.filter(e => {
    return location.pathname.startsWith(e);
  });

  console.log(
    "StartUp component",
    authorizationToken,
    location,
    _unprotectedRoutes,
    _protectedRoutes,
    props
  );

  const _default = (
    <GlobalSpinnerProvider>
      <GlobalSpinnerContext.Consumer>
        {(context: any) => (
          <React.Fragment>
          <GlobalSpinner visible={context.visible} />
          <CandidateProfileImageContextProvider>
            <SuggestedJobContextProvider>
              <NewlyPostedJobContextProvider>
                <MatchedJobContextProvider>
                  <DigiLockerContextProvider>
                    <JobBookMarkContextProvider>
                      <JobAppliedContextProvider>
                        <MyProfileContextProvider>
                          <DatePostedContextProvider>
                            <ExperienceContextProvider>
                              <JobTypeContextProvider>
                                <JobAlertContextProvider>
                                  <SearchContextProvider>                                  
                                  <VendorProfileImageContextProvider>
                                    <MyProfileVendorContextProvider>
                                      <SearchContextProvider>
                                        <LayoutContainer />
                                      </SearchContextProvider>
                                    </MyProfileVendorContextProvider>
                                  </VendorProfileImageContextProvider>
                                  </SearchContextProvider>
                                </JobAlertContextProvider>
                              </JobTypeContextProvider>
                            </ExperienceContextProvider>
                          </DatePostedContextProvider>
                        </MyProfileContextProvider>
                      </JobAppliedContextProvider>
                    </JobBookMarkContextProvider>
                  </DigiLockerContextProvider>
                </MatchedJobContextProvider>
              </NewlyPostedJobContextProvider>
            </SuggestedJobContextProvider>
          </CandidateProfileImageContextProvider> 

            {/* <GlobalSpinner visible={context.visible} />
            <VendorProfileImageContextProvider>
              <MyProfileVendorContextProvider>
                <SearchContextProvider>
                  <LayoutContainer />
                </SearchContextProvider>
              </MyProfileVendorContextProvider>
            </VendorProfileImageContextProvider> */}
          </React.Fragment>
        )}
      </GlobalSpinnerContext.Consumer>
    </GlobalSpinnerProvider>
  );

  if (authorizationToken) {
    if (_unprotectedRoutes.length > 0) {
      return <Redirect to="/" />;
    } else {
      return _default;
    }
  } else {
    if (_protectedRoutes.length > 0) {
      return <Redirect to="/login" />;
    } else {
      return _default;
    }
  }

  /*if (authorizationToken) {
    if (_unprotectedRoutes.length > 0 && unprotectedRoutes[5]!="/job-search") {
      return <Redirect to="/" />
    } else if(unprotectedRoutes[5]!="/job-search"){

       return <Redirect to="/job-search"/>   
    }
    else {
      
      return<>
      <ExperienceContextProvider>
      <JobTypeContextProvider>
      <JobAlertContextProvider>
      <SearchContextProvider>
       <LayoutContainer />;
       </SearchContextProvider>
       </JobAlertContextProvider>
       </JobTypeContextProvider>
       </ExperienceContextProvider>
       </>
    }
  } else {
    if (_protectedRoutes.length > 0 && unprotectedRoutes[5]!="/job-search") {
      return <Redirect to="/login" />
    } else if(unprotectedRoutes[5]!="/job-search"){

       return <Redirect to="/job-search"/>   
    }

     else {
       
      return <>
      <ExperienceContextProvider>
      <JobTypeContextProvider>
      <JobAlertContextProvider>
      <SearchContextProvider>
      <LayoutContainer />;
      </SearchContextProvider>
      </JobAlertContextProvider>
      </JobTypeContextProvider>
      </ExperienceContextProvider>
      </>
    }
  }*/
};

export default StartUp;
