export const environment = {
  production: false,
  baseUrl: "https://localhost:44361",
  tokenUrl: "https://localhost:44361/api",
  loginUrl: "/login"
};

export const AppUrls = {
  // registerMe: environment.baseUrl + "/api/account/register-me",
  GetAllJobCategory: environment.baseUrl + "/api/JobCategory/gaaa",
  GetAllJobApplied: environment.baseUrl + "/api/JobApplied/gaaa",
  GetMyProfileSelectBoxData:
    environment.baseUrl + "/api/Candidate/get-selectbox-data",
  GetVendorMyProfileSelectBoxData:
    environment.baseUrl + "/api/Vendor/get-selectbox-data",
  GetAllMatchedJob: environment.baseUrl + "/api/JobPost/sbca",
  GetAllSearchResultWithToken: environment.baseUrl + "/api/JobPost/sa",
  GetAllJobType: environment.baseUrl + "/api/JobType/gaaa",
  GetAllNewlyPostedJob: environment.baseUrl + "/api/jobsearch/gaaa",
  GetAllSearchResultWithOutToken: environment.baseUrl + "/api/jobsearch/sa",
  GetAllJobAlert: environment.baseUrl + "/api/JobPost/gaja",
  GetAllJobAlertWithCandidateId: environment.baseUrl + "/api/JobPostAlert/gaaa",
  GetAllExperience: environment.baseUrl + "/api/ExpereinceType/gaaa",
  GetAllDatePosted: environment.baseUrl + "/api/DatePosted/gaaa",
  GetMyProfileBasicDetails:
    environment.baseUrl + "/api/Account/get-basic-details",
  GetAllBookMarkedJobs: environment.baseUrl + "/api/jobBookmarked/grsj",
  GetAllCandidateBookMarkedJobs:
    environment.baseUrl + "/api/jobBookmarked/gaaa",
  GetDigiLockerDocumentList:
    environment.baseUrl + "/api/DigiDocumentDetails/gaa",
  GetDigiLockerDocumentTypeList:
    environment.baseUrl + "/api/DigiDocumentType/gaaa",
  GetDigiLockerExpiringDocumentList:
    environment.baseUrl + "/api/DigiDocumentDetails/geda",
  GetDigiLocker: environment.baseUrl + "/api/DigiDocumentDetails/ga",
  GetProfileImage: environment.baseUrl + "/api/CandidateProfileImage/gaaa",
  GetVendorProfileImage: environment.baseUrl + "/api/VendorProfileImage/gaaa",
  GetSuggestedJob: environment.baseUrl + "/api/JobPost/sgja",
  GetAllCandidateJobAppliedList:
    environment.baseUrl + "/api/JobAppliedDetails/graj",
  GetAllCount: environment.baseUrl + "/api/JobApplied/gaca",
  GetProfileProgressBar: environment.baseUrl + "/api/JobApplied/gpa",
  GetTitleWithCategory: environment.baseUrl + "/api/JobPostAlert/get-title",
  // ShareDigiLocker:environment.baseUrl+"/api/DigiDocumentDownload/ga",
  ShareDigiLocker: environment.baseUrl + "/api/DigiDocumentUpload/share-doc",
  SaveJobAlert: environment.baseUrl + "/api/jobPostAlert/coea",
  SaveJobAlertCategoryMap:
    environment.baseUrl + "/api/JobAlertCategoryMap/coea",
  SaveJobAlertExperienceMap:
    environment.baseUrl + "/api/jobAlertExperienceMap/coea",
  SaveJobAlertIndustryMap:
    environment.baseUrl + "/api/jobAlertIndustryMap/coea",
  SaveJobAlertRoleMapMap: environment.baseUrl + "/api/jobAlertRoleMap/coea",
  SaveJobAlertTypeMap: environment.baseUrl + "/api/jobAlertTypeMap/coea",
  SaveJobApplied: environment.baseUrl + "/api/jobAppliedDetails/coea",
  SaveBookMarkList: environment.baseUrl + "/api/jobBookmarked/coea",
  SaveDigiLockerList: environment.baseUrl + "/api/DigiDocumentDetails/coea",

  DeleteDigiLocker: environment.baseUrl + "/api/DigiDocumentDetails/da",
  DeleteBookMarked: environment.baseUrl + "/api/Jobbookmarked/nba",
  DeleteJobAlert: environment.baseUrl + "/api/JobPostAlert/da",

  ToMoveDocument: environment.baseUrl + "/api/DigiDocumentDetails/doc-move",

  // Vendor apis
  GetMyProfileVendorSelectBoxData:
    environment.baseUrl + "/api/Vendor/get-selectbox-data",
  GetProfileVendorImage: environment.baseUrl + "/api/VendorProfileImage/gaaa",
  GetProfileVendorProgressBar:
    environment.baseUrl + "/api/JobApplied_vendor/gpa"
};

export const socialLoginIds = {
  facebook: {
    appId: "376466233478368"
  }
};
